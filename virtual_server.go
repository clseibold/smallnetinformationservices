package smallnetinformationservices

import (
	"bytes"
	"crypto/sha256"
	"embed"
	"encoding/hex"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gabriel-vasile/mimetype"
	cmap "github.com/orcaman/concurrent-map/v2"
	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
)

type ServerType int

const ServerType_Gemini ServerType = 0  // Gemini + Titan
const ServerType_Nex ServerType = 1     // Nex + NPS
const ServerType_Gopher ServerType = 2  // Gopher
const ServerType_Misfin ServerType = 3  // Misfin(B) + Misfin(C) + Gemini + Titan
const ServerType_Admin ServerType = 4   // Gemini + Titan
const ServerType_Spartan ServerType = 5 // Spartan
const ServerType_Scroll ServerType = 6  // Scroll + Titan
const ServerType_Unknown ServerType = 7

type RateLimitListItem struct {
	prevTime     time.Time
	redirectPath string
}

// NOTE: Usually accessed through a ServerHandle. See server_handle.go
type VirtualServer struct {
	Name string
	Type ServerType

	Protocols   bitset.BitSet[ProtocolType, ProtocolType]
	KeyValStore cmap.ConcurrentMap[string, interface{}] // To store data that can be used in request handlers.

	MaxConcurrentConnections int    // -1 for unlimited
	Directory                string // Defaults to SIS Directory, unless configured to elsewhere via sis.conf file in code.
	FS                       ServerFS

	via_config      bool              // Determines if the Server was added via the API or via a config.
	ServerHandler   ServerHandlerFunc // For custom servers
	DefaultLanguage string

	Pubnix            bool
	Router            Router      // For Downloads
	UploadRouter      Router      // For Uploads (if applicable)
	msg               chan string // TODO
	RateLimitDuration time.Duration

	SIS             *SISContext
	ipRateLimitList cmap.ConcurrentMap[string, RateLimitListItem]
	running         bool // TODO
	handle          VirtualServerHandle

	admin bool
}

func (s *VirtualServer) GetHandle() VirtualServerHandle {
	return s.handle
}

type ServerHandlerFunc func(s *VirtualServer)

func (s *VirtualServer) HasTLSProtocol() bool {
	return s.Protocols.Test(ProtocolType_Gemini) || s.Protocols.Test(ProtocolType_Misfin_A) || s.Protocols.Test(ProtocolType_Misfin_B) || s.Protocols.Test(ProtocolType_Misfin_C) || s.Protocols.Test(ProtocolType_Scroll) || s.Protocols.Test(ProtocolType_Gopher_SSL) || s.Protocols.Test(ProtocolType_Titan)
}

func (s *VirtualServer) HasNonTLSProtocol() bool {
	return s.Protocols.Test(ProtocolType_Gopher) || s.Protocols.Test(ProtocolType_Spartan) || s.Protocols.Test(ProtocolType_Guppy) || s.Protocols.Test(ProtocolType_Nex) || s.Protocols.Test(ProtocolType_NPS)
}

func (s *VirtualServer) HasUploadProtocol() bool {
	return s.Protocols.Test(ProtocolType_Titan) || s.Protocols.Test(ProtocolType_Misfin_A) || s.Protocols.Test(ProtocolType_Misfin_B) || s.Protocols.Test(ProtocolType_Misfin_C) || s.Protocols.Test(ProtocolType_Spartan) || s.Protocols.Test(ProtocolType_NPS) || s.Protocols.Test(ProtocolType_Scorpion)
}

func (s *VirtualServer) AddHosts(hosts ...HostConfig) {
	context := s.SIS

	var domainsMap map[string]struct{} = make(map[string]struct{})
	var domains []string
	if s.admin {
		for _, host := range hosts {
			if _, exists := domainsMap[host.Hostname]; host.Hostname != "" && !exists {
				domainsMap[host.Hostname] = struct{}{}
				domains = append(domains, host.Hostname)
			}
		}
	}

	// Go through each host, add the certs, add the hosts to the server, and add the server to the port listener(s)
	for _, host := range hosts {
		if host.BindPort == "" {
			switch s.Type {
			case ServerType_Admin, ServerType_Gemini:
				if host.Upload {
					host.BindPort = defaultPorts[ProtocolType_Titan]
				} else {
					host.BindPort = defaultPorts[ProtocolType_Gemini]
				}
			case ServerType_Gopher:
				if host.Upload {
					continue
				} else {
					host.BindPort = defaultPorts[ProtocolType_Gopher]
				}
			case ServerType_Misfin:
				if host.Upload {
					host.BindPort = defaultPorts[ProtocolType_Misfin_B]
				} else {
					host.BindPort = defaultPorts[ProtocolType_Misfin_B]
				}
			case ServerType_Nex:
				if host.Upload {
					host.BindPort = defaultPorts[ProtocolType_NPS]
				} else {
					host.BindPort = defaultPorts[ProtocolType_Nex]
				}
			case ServerType_Scroll:
				if host.Upload {
					host.BindPort = defaultPorts[ProtocolType_Titan]
				} else {
					host.BindPort = defaultPorts[ProtocolType_Scroll]
				}
			case ServerType_Spartan:
				host.BindPort = defaultPorts[ProtocolType_Spartan]
			}
		}
		if host.Port == "" && !host.SCGI {
			switch s.Type {
			case ServerType_Admin, ServerType_Gemini:
				if host.Upload {
					host.Port = defaultPorts[ProtocolType_Titan]
				} else {
					host.Port = defaultPorts[ProtocolType_Gemini]
				}
			case ServerType_Gopher:
				if host.Upload {
					continue
				} else {
					host.Port = defaultPorts[ProtocolType_Gopher]
				}
			case ServerType_Misfin:
				if host.Upload {
					host.Port = defaultPorts[ProtocolType_Misfin_B]
				} else {
					host.Port = defaultPorts[ProtocolType_Misfin_B]
				}
			case ServerType_Nex:
				if host.Upload {
					host.Port = defaultPorts[ProtocolType_NPS]
				} else {
					host.Port = defaultPorts[ProtocolType_Nex]
				}
			case ServerType_Scroll:
				if host.Upload {
					host.Port = defaultPorts[ProtocolType_Titan]
				} else {
					host.Port = defaultPorts[ProtocolType_Scroll]
				}
			case ServerType_Spartan:
				host.Port = defaultPorts[ProtocolType_Spartan]
			}
		}

		if host.BindPort == "" {
			panic(fmt.Sprintf("Host's Bind Port is empty. %v", host))
		}

		// Create the port listener, if it doesn't already exist
		context.GetOrCreatePortListener(host.BindAddress, host.BindPort)

		// If host lines don't specify a certificate, and the protocol requires a certificate, then add a default
		if !host.SCGI && host.CertPath == "" && (s.Type == ServerType_Gemini || s.Type == ServerType_Admin || s.Type == ServerType_Misfin || s.Type == ServerType_Scroll) {
			if s.admin {
				host.CertPath = filepath.Join(context.directory, "admin.pem")
			} else {
				host.CertPath = filepath.Join(context.directory, s.Name+".pem")
			}
		}

		// For admin server, check that certificate exists and generate it if it doesn't
		if _, err := os.Stat(host.CertPath); s.admin && errors.Is(err, os.ErrNotExist) {
			// Auto-create the certificate and save it to CertFilepath
			pemBuffer := bytes.NewBuffer([]byte{})
			certificate, err := GenerateSelfSignedCertificateAndPrivateKey(KeyType_ECDSA, host.Hostname, domains, "", time.Now().UTC().Add(time.Hour*24*365*200), pemBuffer)
			if err != nil || certificate == nil {
				panic("Certificate for SIS Manager interface doesn't exist and failed to auto-generate.")
			}

			write_err := os.WriteFile(host.CertPath, pemBuffer.Bytes(), 0600)
			if write_err != nil {
				panic(write_err)
			}
		}

		if !host.SCGI && host.CertPath != "" {
			if !filepath.IsAbs(host.CertPath) {
				host.CertPath = filepath.Join(context.directory, host.CertPath)
			}

			context.GetOrAddCertificateConnection(host.CertPath, host.BindAddress, host.BindPort, host.Hostname)
		}

		context.AddServerHostMapping(host.BindAddress, host.BindPort, host.Hostname, host.Port, host.Upload, host.SCGI, s.GetHandle())
	}
}

// Located in server's directory (which defaults to the SIS directory)
func (s *VirtualServer) LoadRoutes() error {
	routesFilepath := filepath.Join(s.SIS.directory, s.Name+"_routes.list") // TODO: I changed this to use SIS directory rather than Server's directory
	if s.Name == "SIS" {
		routesFilepath = filepath.Join(s.SIS.directory, "admin_routes.list")
	}
	data, err := os.ReadFile(routesFilepath)
	if err != nil {
		return err
	}
	lines := strings.FieldsFunc(string(data), func(r rune) bool { return r == '\n' })

	for _, line := range lines {
		if strings.HasPrefix(line, "//") || strings.TrimSpace(line) == "" { // Skip comment lines and empty lines
			continue
		}
		parts := strings.SplitN(line, " ", 2)
		if len(parts) < 2 {
			continue
		}
		route := strings.TrimSpace(parts[0])
		path := strings.TrimSpace(parts[1])
		if strings.HasPrefix(path, "$") {
			s.AddProxyRoute(route, path, '0') // TODO: GopherItemType
		} else if strings.HasPrefix(path, "cgi:") {
			s.AddCGIRoute(route, path)
		} else if strings.HasPrefix(path, "scgi://") {
			address := strings.TrimSuffix(path, "/")
			s.AddSCGIRoute(route, address)
		} else {
			if !filepath.IsAbs(path) {
				// If not absolute, make relative to server directory
				path = filepath.Join(s.Directory, path)
			}

			// Check if directory or file
			info, err := os.Stat(path)
			if err != nil {
				continue
			}
			if info.IsDir() {
				s.AddDirectory(route, path)
			} else {
				s.AddFile(route, path)
			}
		}
	}
	return nil
}

func (s *VirtualServer) SaveRoutes() error {
	var builder strings.Builder
	// Save a comment header that details the different formats
	builder.WriteString("// Route Format Examples:\n")
	builder.WriteString("// /route/file /abs/path/to/file\n")
	builder.WriteString("// /route/dir/* /abs/path/to/dir\n")
	builder.WriteString("// /route/cgi_name/* cgi:/abs/path/to/cgi/script\n")
	builder.WriteString("// /route/scgi_name/* scgi://address:port\n")
	builder.WriteString("// /route/gopher_proxy/* $gopher_server_name/*\n")
	builder.WriteString("// /route/blog_proxy/* $persomal_server_name/blog/*\n")
	builder.WriteString("\n")

	for _, route := range s.Router.GetRoutesList() {
		if route.node.via_config && (route.node.servePath != "" || route.node.proxyRoute != "") {
			builder.WriteString(route.route)
			builder.WriteRune(' ')
			if route.node.proxyRoute != "" {
				if strings.HasPrefix(route.node.proxyRoute, "/") {
					builder.WriteString("$" + route.node.proxyRoute)
				} else {
					builder.WriteString(route.node.proxyRoute)
				}
			} else {
				builder.WriteString(route.node.servePath) // Should always be absolute
			}
			builder.WriteRune('\n')
		}
	}

	routesFilepath := filepath.Join(s.Directory, s.Name+"_routes.list")
	return os.WriteFile(routesFilepath, []byte(builder.String()), 0600)
}

func (s *VirtualServer) GetHostOfPortListener(protocol ProtocolType, upload bool, bind_addr string, bind_port string, hostname string, port string, scgi bool) ServerHost {
	if !s.Protocols.Test(protocol) && protocol != ProtocolType_Unknown {
		return ServerHost{}
	}
	if upload && !s.HasUploadProtocol() {
		return ServerHost{}
	}

	hosts := s.SIS.certmanager.GetServerHostsWithServePortOnPortListener(bind_addr, bind_port, port, s.handle)
	/*if len(hosts) <= 0 {
	panic(fmt.Sprintf("Server %s has no hosts.", s.Name))
	}*/

	for _, h := range hosts {
		if h.SCGI == scgi && h.Hostname == hostname && upload == h.Upload {
			return h
		}
	}

	return ServerHost{}
}

// TODO: This doesn't work correctly (because it uses maps). We need to store what the preferred host should be (and if it changes, it should change).
func (s *VirtualServer) GetPreferredHost(protocol ProtocolType, upload bool, scgi bool) ServerHost {
	if !s.Protocols.Test(protocol) && protocol != ProtocolType_Unknown {
		return ServerHost{}
	}
	if upload && !s.HasUploadProtocol() {
		return ServerHost{}
	}

	hosts := s.SIS.certmanager.GetServerHosts(s.handle)
	/*if len(hosts) <= 0 {
	panic(fmt.Sprintf("Server %s has no hosts.", s.Name))
	}*/

	for _, h := range hosts {
		if h.SCGI == scgi && upload == h.Upload {
			return h
		}
	}

	return ServerHost{}
}

// TODO: This doesn't work correctly (because it uses maps)
func (s *VirtualServer) GetPreferredHostOfPortListener(protocol ProtocolType, upload bool, bind_addr string, bind_port string, scgi bool) ServerHost {
	if !s.Protocols.Test(protocol) && protocol != ProtocolType_Unknown {
		return ServerHost{}
	}
	if upload && !s.HasUploadProtocol() {
		return ServerHost{}
	}

	hosts := s.SIS.certmanager.GetServerHostsOnPortListener(bind_addr, bind_port, s.handle)
	/*if len(hosts) <= 0 {
	panic(fmt.Sprintf("Server %s has no hosts.", s.Name))
	}*/

	for _, h := range hosts {
		if h.SCGI == scgi && upload == h.Upload {
			return h
		}
	}

	return ServerHost{}
}

// If not rate-limited, set ip to current time and return false. If last access time is within duration, set ip to current time and return true. Otherwise, set ip to current time and return false
func (s *VirtualServer) IsIPRateLimited(ip string) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))
	currentTime := time.Now().UTC()

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if currentTime.After(rl.prevTime.Add(s.RateLimitDuration)) {
			// Outside of duration, no rate limit
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return false
		} else {
			// Rate limited, require ip to wait 1 whole second before requesting again by setting the ip's "previous access time" to the future
			//delta := time.Second - s.RateLimitDuration
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return true
		}
	} else {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, ""})
		return false
	}
}

func (s *VirtualServer) IPRateLimit_IsExpectingRedirect(ip string) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if rl.redirectPath != "" {
			return true
		}
	}
	return false
}
func (s *VirtualServer) IpRateLimit_GetExpectedRedirectPath(ip string) string {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		return rl.redirectPath
	}
	return ""
}

// When a redirection occurs, let server know to expect another request immediately following the previous one so that the rate-limiting can be bypassed
func (s *VirtualServer) IPRateLimit_ExpectRedirectPath(ip string, redirectPath string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, redirectPath})
	}
}
func (s *VirtualServer) IPRateLimit_ClearRedirectPath(ip string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, ""})
	}
}

// Removes all IPs that are passed the rate-limit duration.
// There's no need to call this too often, but do not wait so long that the ip list becomes too big.
func (s *VirtualServer) CleanupRateLimiting() {
	fmt.Printf("[:%s] Cleaning up rate limiting map\n", s.Name)
	currentTime := time.Now().UTC()
	items := s.ipRateLimitList.IterBuffered()
	for item := range items {
		prevTime := item.Val.prevTime
		redirectPath := item.Val.redirectPath
		if currentTime.After(prevTime.Add(s.RateLimitDuration)) && redirectPath == "" {
			s.ipRateLimitList.Remove(item.Key)
		}
	}
}

// Starts a goroutine that cleans up the ipRateLimitList map every 6 hours.
func (s *VirtualServer) startRateLimitingCleanerLoop() {
	timer := time.NewTicker(time.Hour * 6)
	go func() {
		for {
			<-timer.C
			s.CleanupRateLimiting()
		}
	}()
}

// Send request off to protocol's request handler
func (s *VirtualServer) handleRequest(conn net.Conn, requestHeader RequestHeader, host ServerHost, ip string, remote_port string, _tcpConn *net.TCPConn) {
	switch s.Type {
	case ServerType_Gemini, ServerType_Admin:
		s.gemini_handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
	case ServerType_Gopher:
		s.gopher_handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
	case ServerType_Misfin:
		//s.misfin
	case ServerType_Nex:
		s.nex_handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
	case ServerType_Spartan:
		s.spartan_handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
	case ServerType_Scroll:
		s.scroll_handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
	}
}

func (s *VirtualServer) AddRoute(p string, handler RequestHandler) {
	// NOTE: Assume that all routes ending in "/" are intended to be directories/gophermaps for now.
	if strings.HasSuffix(p, "/") {
		// Present as directory listing
		s.Router.addRoute(p, handler, "", "", "", '1') // TODO
	} else {
		// TODO: Present as text file?
		s.Router.addRoute(p, handler, "", "", "", '0') // TODO
	}
}
func (s *VirtualServer) AddUploadRoute(p string, handler RequestHandler) {
	switch s.Type {
	case ServerType_Gopher, ServerType_Nex:
		panic("Nex servers don't support upload routes.")
	}
	s.UploadRouter.addRoute(p, handler, "", "", "", '0')
}

func (s *VirtualServer) AddEmbedFile(p string, content embed.FS, filePath string) {
}
func (s *VirtualServer) AddEmbedDirectory(p string, content embed.FS, directoryPath string) {
	//direntry, _ := content.ReadDir(".")

}

// AddDirectory adds a directory that's either absolute, or relative to the server's directory
// Remember that a server's directory, when not specified, defaults to a subdirectory named after the server's name under the SIS directory.
func (s *VirtualServer) AddDirectory(p string, directoryPath string) {
	// Security: Don't expose system's root directory.
	if directoryPath == "/" || directoryPath == "C:" || directoryPath == "C:/" || directoryPath == "C:\\" {
		panic("System's root directory is exposed.")
	}
	if !filepath.IsAbs(directoryPath) {
		directoryPath = filepath.Join(s.Directory, directoryPath)
	}

	// TODO: Handle {route}/index.gmi route

	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleDirectory, directoryPath, "", "", '1')
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleNexDirectory, directoryPath, "", "", '1')
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleDirectory, directoryPath, "", "", '1')
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleDirectory, directoryPath, "", "", '1')
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleDirectory, directoryPath, "", "", '1')
	}
}

func (s *VirtualServer) AddCGIRoute(p string, filePath string) {
	path := strings.TrimPrefix(filePath, "cgi:")
	if !filepath.IsAbs(path) {
		filePath = "cgi:" + filepath.Join(s.Directory, path)
	}

	fileItemType := '1'
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleCGI, filePath, "", "", fileItemType)
		s.UploadRouter.addRoute(p, gemini_handleCGI, filePath, "", "", fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleCGI, filePath, "", "", fileItemType)
		s.UploadRouter.addRoute(p, nex_handleCGI, filePath, "", "", fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleCGI, filePath, "", "", fileItemType)
		s.UploadRouter.addRoute(p, gopher_handleCGI, filePath, "", "", fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleCGI, filePath, "", "", fileItemType)
		s.UploadRouter.addRoute(p, spartan_handleCGI, filePath, "", "", fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleCGI, filePath, "", "", fileItemType)
		s.UploadRouter.addRoute(p, scroll_handleCGI, filePath, "", "", fileItemType)
	}
}

func (s *VirtualServer) AddSCGIRoute(p string, address string) {
	fileItemType := '1'
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleSCGI, address, "", "", fileItemType)
		s.UploadRouter.addRoute(p, gemini_handleSCGI, address, "", "", fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleSCGI, address, "", "", fileItemType)
		s.UploadRouter.addRoute(p, nex_handleSCGI, address, "", "", fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleSCGI, address, "", "", fileItemType)
		s.UploadRouter.addRoute(p, gopher_handleSCGI, address, "", "", fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleSCGI, address, "", "", fileItemType)
		s.UploadRouter.addRoute(p, spartan_handleSCGI, address, "", "", fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleSCGI, address, "", "", fileItemType)
		s.UploadRouter.addRoute(p, scroll_handleSCGI, address, "", "", fileItemType)
	}
}

// Proxy routes will proxy the handler from a different server to this server and will automatically set the request to convert mode.
func (s *VirtualServer) AddProxyRoute(p string, proxyRoute string, gopherItemType rune) {
	s.Router.addRoute(p, proxyHandler, "", proxyRoute, "", gopherItemType) // TODO
}

// Proxies an entire (Virtual)Server underneath the given route.
func (s *VirtualServer) AddServerProxyRoute(p string, server VirtualServerHandle) {
	s.Router.addRoute(p, proxyHandler, "", "$"+server.Name()+"/*", "", '1')
}

// Adds a file that's either absolute, or relative to the server's directory
func (s *VirtualServer) AddFile(p string, filePath string) {
	if !filepath.IsAbs(filePath) {
		filePath = filepath.Join(s.Directory, filePath)
	}

	// Detect file's itemtype
	extension := filepath.Ext(filePath)
	mt, err := mimetype.DetectFile(filePath)
	if err != nil {
		panic(err)
	}
	mt_string := mt.String()
	mt_parent_string := mt.Parent().String()
	if extension == ".gemini" || extension == ".gmi" {
		mt_string = "text/gemini"
		mt_parent_string = ""
	} else if extension == ".gophermap" {
		mt_string = "application/gopher-menu"
		mt_parent_string = ""
	} else if extension == ".nex" || (extension == "" && filepath.Base(filePath) == "index") {
		mt_string = "text/nex"
		mt_parent_string = ""
	} else if PrivacySensitiveFilename(filePath) || PrivacySensitiveMimetype(mt_string) {
		// Don't add privacy-sensitive files or certificate files to routing
		return
	}

	fileItemType := MimetypeToGopherItemtype(mt_string, mt_parent_string)
	if strings.HasSuffix(p, "/") {
		// Present file as a directory listing
		fileItemType = '1'
	}

	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleFile, filePath, "", "", fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleNexFile, filePath, "", "", fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleFile, filePath, "", "", fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleFile, filePath, "", "", fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleFile, filePath, "", "", fileItemType)
	}
}

// Group creates a prefix to be automatically prepended to all routes created on the RouteGroup.
// It will also ensure all absolute links in the responses of the attached routes will be
// prepended with the group prefix.
func (s *VirtualServer) Group(p string) RouteGroup {
	return RouteGroup{handle: s.GetHandle(), p: p}
}

/*
func (s *Server) PrefixLink(link string) string {
	return link
}
*/

// Gets the default/download protocol Scheme
func (s *VirtualServer) Scheme() string { // TODO: Replace this with a system that gets the default scheme for a server (based on TLS/non-TLS and upload/download)
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		return "gemini://"
	case ServerType_Nex:
		return "nex://"
	case ServerType_Gopher:
		return "gopher://"
	case ServerType_Misfin:
		return "misfin://"
	case ServerType_Spartan:
		return "spartan://"
	case ServerType_Scroll:
		return "scroll://"
	}

	return ""
}

func (s *VirtualServer) UploadScheme() string { // TODO: Replace this with a system that gets the default scheme for a server (based on TLS/non-TLS and upload/download)
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		return "titan://"
	case ServerType_Nex:
		return "nps://"
	case ServerType_Gopher: // TODO
		return "titan://"
	case ServerType_Misfin:
		return "misfin://"
	case ServerType_Spartan:
		return "spartan://"
	case ServerType_Scroll:
		return "titan://"
	}

	return ""
}
