// The main Smallnet Information Services Server
package main

import (
	"fmt"
	"os"

	sis "gitlab.com/sis-suite/smallnetinformationservices"
)

func main() {
	//startCmd := flag.NewFlagSet("start", flag.ExitOnError)
	if len(os.Args) < 2 {
		fmt.Printf("SIS Subcommands:\n")
		fmt.Printf("  %-10s %s\n", "start", "Starts the server.")
		fmt.Print("\n")
		os.Exit(1)
	}

	cmd := os.Args[1]
	args := os.Args[2:]
	switch cmd {
	case "start":
		startServer(args)
	case "help":
		fmt.Printf("SIS Subcommands:\n")
		fmt.Printf("  %-10s %s\n", "start", "Starts the server.")
		fmt.Print("\n")
		os.Exit(1)
	default:
		fmt.Printf("That command doesn't exist.\n")
		fmt.Printf("SIS Subcommands:\n")
		fmt.Printf("  %-10s %s\n", "start", "Starts the server.")
		fmt.Print("\n")
		os.Exit(1)
		os.Exit(1)
	}
}

func startServer(args []string) {
	if len(args) < 1 {
		fmt.Printf("You must pass in the root SIS directory to start the server from.\n")
		os.Exit(1)
	}
	context, err := sis.InitSIS(args[0])

	if err != nil {
		panic(err)
	}
	context.SaveConfiguration()
	context.Start()
}
