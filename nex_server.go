package smallnetinformationservices

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"time"

	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
)

func (s *VirtualServer) nex_handleRequest(conn net.Conn, requestHeader RequestHeader, host ServerHost, ip string, remote_port string, _tcpConn *net.TCPConn) {
	if requestHeader.Protocol == ProtocolType_NPS { // NPS - only one route is used for NPS uploads, since NPS doesn't support the use of selectors/paths/URLs.
		// Data sent via NPS, which ends in a "." on its own line.
		uploadData := strings.TrimSuffix(strings.TrimSuffix(requestHeader.Request, "\n."), "\r")
		p := "/"

		if s.IsIPRateLimited(ip) {
			// Rate limited, return a 44 slow down error
			conn.Write([]byte("Rate-limited. Slow down.\r\n"))
			return
		}

		fmt.Printf("[Nex:%s] Requested Path: %s\n", s.Name, p)
		node, globString, params := s.UploadRouter.Search(p)
		//fmt.Printf("Glob String: %s\n", globString)
		defer conn.Write([]byte("\n"))
		if node != nil && node.Handler != nil {
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			flags.Set(RequestFlag_Upload)
			flags.Set(RequestFlag_HeaderSent) // NPS doesn't send a response header
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			routePath := node.GetRoutePath()
			prefix := InsertParamsIntoRouteString(node.prefix, params, globString, true)
			addSlashSuffix := strings.HasSuffix(p, "/")
			requestPath := strings.TrimSuffix(strings.TrimSuffix(p, "/"), prefix)
			if addSlashSuffix && (len(requestPath) == 0 || requestPath[len(requestPath)-1] != '/') {
				requestPath = requestPath + "/"
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			node.Handler(&Request{ProtocolType_NPS, flags, host, routePath, prefix, requestPath, p, "", 0, globString, conn, _tcpConn, params, int64(len(uploadData)), []byte(uploadData), "text/plain", ClientCertificate{}, ip, remote_port, node.servePath, node.proxyRoute, proxiedUnder, "", node.gopherItemType, s.GetHandle(), []string{}, ScrollMetadata{}})
		} else {
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			flags.Set(RequestFlag_Upload)
			flags.Set(RequestFlag_HeaderSent) // NPS doesn't send a response header
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			s.Router.NotFoundHandler(&Request{ProtocolType_NPS, flags, host, "", "", "", p, "", 0, globString, conn, _tcpConn, params, int64(len(uploadData)), []byte(uploadData), "text/plain", ClientCertificate{}, ip, remote_port, "", "", proxiedUnder, "", '1', s.GetHandle(), []string{}, ScrollMetadata{}})
		}
		params = nil
	} else if requestHeader.Protocol == ProtocolType_Nex { // Nex
		request_without_query, query, _ := strings.Cut(requestHeader.Request, "?")
		p := path.Clean(request_without_query)

		// Handle routing, making sure path is clean for security (eliminate "/..")
		addTrailingSlash := false
		if p == "." || p == "" {
			p = "."
			addTrailingSlash = true
		} else if p == "/" {
			p = "."
		} else {
			p = strings.TrimPrefix(p, "/")
		}

		// Add the trailing slash back in
		if addTrailingSlash || strings.HasSuffix(requestHeader.Request, "/") {
			p = p + "/"
		}

		// Do rate-limiting now that we have read the header of the request to check against
		if s.IpRateLimit_GetExpectedRedirectPath(ip) == p {
			// Allow the redirect through and clear the expected redirect path
			s.IPRateLimit_ClearRedirectPath(ip)
		} else if s.IsIPRateLimited(ip) {
			// Rate limited, return a 44 slow down error
			conn.Write([]byte("Rate-limited. Slow down.\r\n"))
			return
		}

		fmt.Printf("[Nex:%s] Requested Path: %s\n", s.Name, p)
		s.SIS.Log(s.Type, s.Name, "Requested Path: %s", p)

		node, globString, params := s.Router.Search(p)
		//fmt.Printf("Glob String: %s\n", globString)
		defer conn.Write([]byte("\n"))
		if node != nil && node.Handler != nil {
			parsed_ip := net.ParseIP(ip)
			if parsed_ip != nil && !parsed_ip.IsPrivate() {
				node.visits.Add(1)
			}
			node.currentConnections.Add(1)
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			routePath := node.GetRoutePath()
			prefix := InsertParamsIntoRouteString(node.prefix, params, globString, true)
			addSlashSuffix := strings.HasSuffix(p, "/")
			requestPath := strings.TrimSuffix(strings.TrimSuffix(p, "/"), prefix)
			if addSlashSuffix && (len(requestPath) == 0 || requestPath[len(requestPath)-1] != '/') {
				requestPath = requestPath + "/"
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			node.Handler(&Request{ProtocolType_Nex, flags, host, routePath, prefix, requestPath, p, url.QueryEscape(query), 0, globString, conn, _tcpConn, params, 0, []byte{}, "text/plain", ClientCertificate{}, ip, remote_port, node.servePath, node.proxyRoute, proxiedUnder, "", node.gopherItemType, s.GetHandle(), []string{}, ScrollMetadata{}})
			node.currentConnections.Add(-1)
		} else {
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			s.Router.NotFoundHandler(&Request{ProtocolType_Nex, flags, host, "", "", "", p, url.QueryEscape(query), 0, globString, conn, _tcpConn, params, 0, []byte{}, "text/plain", ClientCertificate{}, ip, remote_port, "", "", proxiedUnder, "", '1', s.GetHandle(), []string{}, ScrollMetadata{}})
		}
		params = nil
	}
}

// ----- Route Request Handlers -----

func nex_notFound(request *Request) {
	request.NotFound("Path '/%s' not found.", request.requestPath)
}

func nex_directoryNotFile(request *Request) {
	request.TemporaryFailure("/%s/ is a directory, not a file.", request.requestPath)
}

func nex_handleNexFile(request *Request) {
	info, info_err := os.Stat(request.servePath)
	if info_err != nil {
		gemini_notFound(request)
		return // Return error
	} else if info.IsDir() {
		request.NotFound("Not found.")
		return
	} else if runtime.GOOS != "windows" && info.Mode().Perm()&(1<<2) == 0 { // Must be world-readable file or world-readable-executable directory on non-Windows systems.
		request.NotFound("Not found.")
		return
	}

	err := request.File(request.servePath)
	if err != nil {
		nex_notFound(request)
		return
	}
	/*file, err := os.Open(request.servePath)
	if err != nil {
		nex_notFound(request)
		return
	}
	defer file.Close()
	io.Copy(request.Writer, file)*/
}

// Handles serving a directory, and anything underneath if a glob was used
func nex_handleNexDirectory(request *Request) {
	// Combine servePath with request path
	p := filepath.Join(request.servePath, request.GlobString)
	fmt.Printf("Serving: %s\n", p)

	// Check if file, and serve the file instead
	info, err := os.Stat(p)
	if err != nil {
		nex_notFound(request)
		return // Return error
	} else if runtime.GOOS != "windows" && info.Mode().Perm()&(1<<2) == 0 { // Must be world-readable
		request.NotFound("Not found.")
		return
	} else if info.IsDir() && !strings.HasSuffix(request.requestPath, "/") { // TODO: request.TrailingSlash
		// If directory but request string doesn't end in "/", then redirect to error: nex_directoryNotFile
		nex_directoryNotFile(request)
		return
	} else if !info.IsDir() {
		// If not a directory, serve the file
		err := request.File(p)
		if err != nil {
			nex_notFound(request)
			return
		}
		return
	}

	indexFilepath := filepath.Join(p, "index")
	err = request.FileMimetype("text/nex", indexFilepath)
	//indexFile, err := os.Open(indexFilepath)
	if err == nil {
		/*io.Copy(request.Writer, indexFile)
		indexFile.Close()*/
		return
	}

	// Check for .modified and .desc files, used for sorting the directory listing.
	modified := true
	if _, err := os.Stat(filepath.Join(p, ".modified")); err != nil {
		modified = false
	}
	asc := false
	if _, err := os.Stat(filepath.Join(p, ".desc")); err != nil {
		asc = true
	}

	// Read directory and sort
	entries, err := os.ReadDir(p)
	if err != nil {
		nex_notFound(request)
		return
	}
	sort.Slice(entries, func(i, j int) bool {
		if modified {
			st1, err := entries[i].Info()
			if err != nil {
				return false // TODO
			}
			st2, err := entries[j].Info()
			if err != nil {
				return false
			}
			return st1.ModTime().After(st2.ModTime())
		} else if asc {
			return entries[i].Name() < entries[j].Name()
		} else {
			return entries[i].Name() > entries[j].Name()
		}
	})

	// Send the directory listing
	if request.requestPath != "." { // TODO: Print ".." if the parent route has a handler?
		request.NexListing("=> ../\n")
	}
	for _, entry := range entries {
		name := entry.Name()
		if CertFilename(name) && !request.flags.Test(RequestFlag_AllowServeCert) {
			continue
		} else if PrivacySensitiveFilename(name) {
			continue
		}
		if DotFilename(name) && !request.flags.Test(RequestFlag_AllowServeDotFiles) {
			continue
		}
		info, err := entry.Info()
		if err != nil {
			continue
		}
		if info.Mode()&(1<<2) == 0 {
			continue
		}
		if entry.IsDir() {
			name = name + "/"
		}
		request.NexListing(fmt.Sprintf("=> %s\n", name))
	}
}

// Hands request information off to a CGI script.
// If in convert mode, stdout should be handled by SIS's request functions.
func nex_handleCGI(request *Request) {
	// Set up environment variables
	env := os.Environ()
	env = append(env, "GATEWAY_INTERFACE=CGI/1.1")
	env = append(env, "SERVER_PROTOCOL=NEX")
	env = append(env, "SCHEME=nex")
	env = append(env, "REQUEST_METHOD=")
	env = append(env, "SERVER_SOFTWARE=SIS/"+Version)
	env = append(env, fmt.Sprintf("SERVER_NAME=%s", request.Hostname()))
	env = append(env, fmt.Sprintf("SERVER_PORT=%s", request.Host.Port))
	env = append(env, fmt.Sprintf("REAL_LISTEN_ADDR=%s", request.Host.BindAddress))
	env = append(env, fmt.Sprintf("REAL_LISTEN_PORT=%s", request.Host.BindPort))
	env = append(env, fmt.Sprintf("REMOTE_ADDR=%s", request.IP))
	env = append(env, fmt.Sprintf("REMOTE_PORT=%s", request.RemotePort))
	env = append(env, fmt.Sprintf("REMOTE_HOST=%s:%s", request.IP, request.RemotePort)) // TODO
	script_name := strings.TrimSuffix(strings.TrimSuffix(request.RawPath(), request.GlobString), "/")
	env = append(env, "SCRIPT_NAME="+script_name) // TODO

	// NOTE: This is kept because nex servers can be proxied to TLS protocols
	if (request.UserCert != ClientCertificate{}) {
		env = append(env, fmt.Sprintf("AUTH_TYPE=CERTIFICATE"))
		env = append(env, fmt.Sprintf("TLS_CLIENT_HASH=%s", request.UserCertHash())) // TODO: JetForce prepends with "SHA256:"
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_BEFORE=%s", request.UserCert.NotBefore.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_AFTER=%s", request.UserCert.NotAfter.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SERIAL_NUMBER=%s", request.UserCert.SerialNumber))
		env = append(env, fmt.Sprintf("TLS_CLIENT_ISSUER=%s", request.UserCert.Issuer))
		// // // // env = append(env, fmt.Sprintf("TLS_CLIENT_AUTHORIZED=%s", "1")) // TODO

		env = append(env, fmt.Sprintf("REMOTE_IDENT=%s", request.UserCertHash()))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SUBJECT=%s", request.UserCert.Subject))
		env = append(env, fmt.Sprintf("REMOTE_USER=%s", request.UserCert.Subject))
		env = append(env, fmt.Sprintf("REMOTE_USER_ID=%s", request.UserCert.SubjectUserID)) // TODO: Jetforce uses the CN, not the OID_USER_ID
	}

	env = append(env, fmt.Sprintf("REQUEST_URI=%s", request.requestString))
	env = append(env, fmt.Sprintf("NEX_URL=%s", request.requestString))
	env = append(env, fmt.Sprintf("URL=%s", request.requestString))
	env = append(env, fmt.Sprintf("REQUEST_DOMAIN=%s", request.Hostname()))
	env = append(env, fmt.Sprintf("NEX_URL_PATH=%s", request.RawPath()))
	env = append(env, fmt.Sprintf("PATH_INFO=%s", "/"+request.GlobString)) // TODO: Just use the path after the prefix for the route to the script
	env = append(env, fmt.Sprintf("REQUEST_PATH=%s", request.RawPath()))
	query, _ := request.Query()
	env = append(env, fmt.Sprintf("QUERY_STRING=%s", query))

	//if tlsConn, isTls := conn.(*tls.Conn); isTls {
	env = append(env, fmt.Sprintf("SSL_TLS_SNI=%s", request.Hostname()))
	// env = append(env, fmt.Sprintf("TLS_CIPHER=%s", tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)))
	// env = append(env, fmt.Sprintf("TLS_VERSION=%s", tls.VersionName(tlsConn.ConnectionState().Version)))
	//}

	// Check CGI script permissions
	realServePath := strings.TrimPrefix(request.servePath, "cgi:")
	if runtime.GOOS != "windows" {
		stat, stat_err := os.Stat(realServePath)
		if stat_err != nil {
			fmt.Println("Error executing CGI script. Could not stat script.")
			request.CGIFailure("CGI error. Incorrect permissions on script.")
			return
		}
		worldPermission := stat.Mode().Perm() & 0007
		if worldPermission != 5 { // Is not Read and Execute
			fmt.Println("Error executing CGI script. Incorrect permissions on script.")
			request.CGIFailure("CGI error. Incorrect permissions on script.")
			return
		}
	}

	// Execute the CGI script
	cmd := exec.Command(realServePath)
	cmd.Env = env
	cmd.Stdout = request.readwriter
	cmd.Stderr = os.Stderr
	cmd.Dir = filepath.Dir(realServePath)

	// Run the command
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error executing CGI script:", err)
		request.CGIFailure("CGI error.")
		return
	}
}

func nex_handleSCGI(request *Request) {
	query, _ := request.Query()

	// Prepare SCGI request
	script_name := strings.TrimSuffix(strings.TrimSuffix(request.RawPath(), request.GlobString), "/")
	headers := map[string]string{
		"GATEWAY_INTERFACE": "CGI/1.1",
		"CONTENT_LENGTH":    "0",
		"SCGI":              "1",
		"SERVER_PROTOCOL":   "NEX",
		"SCHEME":            "nex",
		"REQUEST_METHOD":    "",
		"SERVER_SOFTWARE":   "SIS/" + Version,
		"SERVER_NAME":       request.Hostname(),
		"SERVER_PORT":       request.Host.Port, // port being forwarded from router
		"REAL_LISTEN_ADDR":  request.Host.BindAddress,
		"REAL_LISTEN_PORT":  request.Host.BindPort, // listenPort
		"REMOTE_ADDR":       request.IP,
		"REMOTE_PORT":       request.RemotePort,
		"REMOTE_HOST":       fmt.Sprintf("%s:%s", request.IP, request.RemotePort),
		//"GEMINI_DOCUMENT_ROOT": filepath.Dir(request.servePath),

		"SCRIPT_NAME":    script_name,
		"REQUEST_URI":    request.requestString,
		"NEX_URL":        request.requestString,
		"URL":            request.requestString,
		"REQUEST_DOMAIN": request.Hostname(),
		"NEX_URL_PATH":   request.RawPath(),
		"PATH_INFO":      "/" + request.GlobString, // NOTE: Just use the path after the prefix for the route to the script
		"REQUEST_PATH":   request.RawPath(),
		"QUERY_STRING":   query,
	}

	if (request.UserCert != ClientCertificate{}) {
		headers["AUTH_TYPE"] = "CERTIFICATE"
		headers["TLS_CLIENT_HASH"] = request.UserCertHash()
		headers["TLS_CLIENT_NOT_BEFORE"] = request.UserCert.NotBefore.Format(time.RFC3339)
		headers["TLS_CLIENT_NOT_AFTER"] = request.UserCert.NotAfter.Format(time.RFC3339)
		headers["TLS_CLIENT_SERIAL_NUMBER"] = request.UserCert.SerialNumber
		headers["TLS_CLIENT_ISSUER"] = request.UserCert.Issuer
		headers["REMOTE_IDENT"] = request.UserCertHash()
		headers["TLS_CLIENT_SUBJECT"] = request.UserCert.Subject   // TODO: Jetforce uses Subject CN for this.
		headers["REMOTE_USER"] = request.UserCert.Subject          // TODO: Jetforce uses Subject CN for this.
		headers["REMOTE_USER_ID"] = request.UserCert.SubjectUserID // TODO: Jetforce uses Subject CN for this.
	}

	//if tlsConn, isTls := conn.(*tls.Conn); isTls {
	headers["SSL_TLS_SNI"] = request.Hostname()
	// headers["TLS_CIPHER"] = tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)
	// headers["TLS_VERSION"] = tls.VersionName(tlsConn.ConnectionState().Version)
	//}

	// Create SCGI request
	var buf bytes.Buffer
	for k, v := range headers {
		buf.WriteString(k)
		buf.WriteByte(0)
		buf.WriteString(v)
		buf.WriteByte(0)
	}
	headerLength := buf.Len()
	fullRequest := fmt.Sprintf("%d:%s,", headerLength, buf.String())

	// Connect to SCGI server
	address := strings.TrimSuffix(strings.TrimPrefix(request.servePath, "scgi://"), "/")
	scgiConn, err := net.Dial("tcp", address)
	if err != nil {
		request.CGIFailure("SCGI connection error.")
		return
	}
	defer scgiConn.Close()

	// Send SCGI request
	_, err = scgiConn.Write([]byte(fullRequest))
	if err != nil {
		fmt.Println("Error sending SCGI request:", err)
		request.CGIFailure("SCGI request error.")
		return
	}

	// Send Upload content if applicable
	if request.Upload() {
		uploadData, err := request.GetUploadData()
		if err != nil {
			fmt.Println("Error sending SCGI request:", err)
			request.CGIFailure("SCGI request error.")
			return
		}

		_, err = scgiConn.Write([]byte(uploadData))
		if err != nil {
			fmt.Println("Error sending SCGI request:", err)
			request.CGIFailure("SCGI request error.")
			return
		}
	}

	// Read response from SCGI server and write to client
	_, err = io.Copy(request.readwriter, scgiConn)
	if err != nil {
		fmt.Println("Error reading SCGI response:", err)
		request.CGIFailure("SCGI response error.")
		return
	}
}
