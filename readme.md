# Smallnet Information Services (SIS)

[![Static Badge](https://img.shields.io/badge/mdBook-Docs-green)](https://auragem.ddns.net/sis/)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](docs/code_of_conduct.md)
![GitLab License](https://img.shields.io/gitlab/license/clseibold%2Fsmallnetinformationservices)
![GitLab Tag](https://img.shields.io/gitlab/v/tag/clseibold%2Fsmallnetinformationservices)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/clseibold%2Fsmallnetinformationservices)

**Smallnet Information Services (SIS)** is a server software suite that plays a similar role to Microsoft's *Internet Information Services (IIS)*, but for protocols in the *small internet ecosystem*. SIS provides integrated server support for [Gemini](https://geminiprotocol.net/), [Gopher](https://en.wikipedia.org/wiki/Gopher_(protocol)), Nex/NPS, Spartan, Misfin, Scroll, and Titan protocols. Using the built-in **SIS Manager** interface (accessed via Gemini), administrators can configure and manage multiple virtual servers across different protocols, defining their hostnames, certificates, and protocol-specific settings from a single administrative console.

![SIS Manager gemini interface](docs/images/Snapshot_2025-02-11_06-07-04.png)

## Request Multiplexing

Incoming connections are multiplexed to each server based on the request's protocol, SNI and ALPN information, and any information provided within the protocol's request syntax. Multiplexing allows dynamic server configuration to take effect immediately without server restarts.

SIS uses *port listeners* to route incoming requests to the appropriate server. This allows for virtual hosting and multiple protocols on one port. However, some protocols have specific restrictions:
- Nex and Gopher don't support virtual hosting, so they need dedicated ports
- Misfin servers on shared ports must use unique hostnames and their own root CA certificates

Since most protocols either use different ports or support virtual hosting, the above restrictions are easily avoided.

## Security

SIS implements features that ensure the security of your system:
* All servers by default rate-limit incoming connections based on IP addresses.
* There is a global max concurrent connection limit.
* File permissions are taken into account when serving directories, files, and CGI scripts.

### Permissions Under Unix Systems (unimplemented)

SIS will only serve world-readable content; all files and directories must be world-readable. If they are not, they are not served and are hidden from directory listings.

CGI scripts must be world-executable and world-readable to function properly.

### Windows Systems (unimplemented)

Like Apache, when SIS is installed as a Windows service, it is ran under a particular user account created for the server. SIS will only serve content readable by this user. CGI scripts must also be executable by this user.

## Installing and Using

View installation and usage documentation [here](http://auragem.ddns.net/sis/book/installing.html).

## Client Library

SIS can be used as a Golang library to: extend an existing server, build a custom server from scratch, create CGI applications, or implement SCGI application servers. See the section on [Using SIS as a Library](http://auragem.ddns.net/sis/book/programming_api/using_sis_as_library.html) for more information.

### SCGI Application Server Example


```go
package main

import sis "gitlab.com/sis-suite/smallnetinformationservices"

func main() {
    // Start SCGI Application Server, in configless mode, meaning there will not be a SIS manager or configuration files.
    context, err := sis.InitConfiglessMode()
    if err != nil {
        panic(err)
    }

    // For SCGI application servers, the Hostname is the expected hostname given from the main gemini server (aka. REQUEST_DOMAIN
    // or SERVER_NAME), and the Port is the expected serve port given from the main gemini server (aka. SERVER_PORT).
    hosts := [...]sis.HostConfig{
        {BindAddress: "localhost", BindPort: "5001", Hostname: "localhost", Port: "1995", Upload: false, SCGI: true},
        {BindAddress: "localhost", BindPort: "5001", Hostname: "localhost", Port: "1995", Upload: true, SCGI: true},
    }
    scgi_gemini_server := context.AddServer(sis.Server{Name: "AuraMuse", Type: sis.ServerType_Gemini}, hosts[:]...)
    scgi_gemini_server.AddRoute("/", func(request sis.Request {
        request.Gemini("# SCGI Application Server Homepage\n")
        request.Link("/test", "Test Link") // This makes sure the link is prepended with the SERVER_NAME, SERVER_PORT, and SCRIPT_NAME given to the SCGI application server.
        request.Gemini("=> /test Another Test Link\n") // request.Gemini() also converts all links in the given gemtext to their correct version by prepending SCRIPT_NAME to absolute links.
    })
    scgi_gemini_server.AddRoute("/test", func(request sis.Request {
        request.Gemini("Hello World!\n")
    })

    context.Start()
}
```
