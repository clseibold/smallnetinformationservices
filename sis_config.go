package smallnetinformationservices

import (
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	cmap "github.com/orcaman/concurrent-map/v2"
)

type HostConfig struct {
	BindAddress string
	BindPort    string
	Hostname    string
	Port        string
	Upload      bool
	CertPath    string
	SCGI        bool // Whether the host should only use SCGI or not. SCGI hosts don't require a hostname, serve port, or certificate, since these are all managed by the SCGI "client".
}

func loadSISConfig(filename string, config *SISContext) error {
	data, err := os.ReadFile(filename)
	if errors.Is(err, os.ErrNotExist) {
		data = []byte("")
	} else if err != nil {
		return err
	}
	lines := strings.FieldsFunc(string(data), func(r rune) bool { return r == '\n' })

	config.servers = make([]VirtualServer, 0)
	var currentServer VirtualServer
	var currentHosts = make([]HostConfig, 0, 1)
	var hasSISSection bool = false
	for _, line := range lines {
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "[") && strings.HasSuffix(line, "]") {
			// Add previous Server to servers list
			addServerConfig(config, &currentServer, currentHosts)

			// Set next server's defaults
			currentHosts = make([]HostConfig, 0, 1)
			name := strings.TrimSuffix(strings.TrimPrefix(line, "["), "]")
			currentServer = VirtualServer{Name: name, via_config: true, MaxConcurrentConnections: 2000, SIS: config /*, Hosts: make([]Host, 0, 1)*/}
			currentServer.msg = make(chan string, 100) // TODO
			currentServer.KeyValStore = cmap.New[interface{}]()
			currentServer.RateLimitDuration = time.Millisecond * 225
			currentServer.ipRateLimitList = cmap.New[RateLimitListItem]()
			currentServer.Router.NotFoundHandler = gemini_notFound
			currentServer.UploadRouter.NotFoundHandler = gemini_notFound
			if name == "SIS" {
				hasSISSection = true
				currentServer.Type = ServerType_Admin
				currentServer.MaxConcurrentConnections = 100
				currentServer.admin = true
			}
		} else {
			parts := strings.SplitN(line, ":", 2)
			field := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])

			if currentServer.Name == "SIS" {
				// Handle the properties specific to the admin Gemini Interface
				if field == "directory" {
					config.directory = value
					if strings.HasPrefix(config.directory, "~") {
						homeDir, _ := os.UserHomeDir()
						config.directory = filepath.Join(homeDir, strings.TrimPrefix(config.directory, "~"))
					} else {
						config.directory, err = filepath.Abs(value)
						if err != nil {
							// TODO
						}
					}
					currentServer.Directory = filepath.Join(config.directory, "admin")
				} else if field == "download-host" || field == "upload-host" {
					parts := strings.Split(value, " ")
					if len(parts) < 2 {
						// ERROR: Must have 2 or 3 arguments (`bindaddr:bindport hostname[:server_port] [cert_path]`)
						panic("Error: host must have 2 or 3 arguments (`bindaddr[:bindport] hostname[:server_port] [cert_path]`)")
					}

					bind := parts[0]
					serve := parts[1]

					bind_parts := strings.SplitN(bind, ":", 2)
					ip := net.ParseIP(bind_parts[0])
					if ip == nil && bind_parts[0] != "localhost" {
						panic(fmt.Sprintf("%s is not a valid bind ip address.", bind))
					}
					ip_addr := ip.String()
					if ip == nil && bind_parts[0] == "localhost" {
						ip_addr = "localhost"
					}

					port := ""
					if len(bind_parts) > 1 {
						port = bind_parts[1]
					}
					// TODO: Verify that the port string is a number

					serve_parts := strings.SplitN(serve, ":", 2)
					if len(serve_parts) == 0 {
						panic("Host must provide a serve hostname and port.")
					}
					hostname := serve_parts[0]
					serve_port := ""
					if len(serve_parts) > 1 {
						serve_port = serve_parts[1]
					}

					certificate_path := ""
					if len(parts) > 2 {
						certificate_path = strings.Join(parts[2:], "")
					}

					currentHosts = append(currentHosts, HostConfig{ip_addr, port, hostname, serve_port, field == "upload-host", certificate_path, false})
				} else if field == "download-scgi-host" || field == "upload-scgi-host" {
					parts := strings.Split(value, " ")
					if len(parts) < 1 {
						// ERROR: Must have one argument (`bindaddr:bindport`)
						panic("Error: host must have one argument (`bindaddr[:bindport]`)")
					}

					bind := parts[0]

					bind_parts := strings.SplitN(bind, ":", 2)
					ip := net.ParseIP(bind_parts[0])
					if ip == nil && bind_parts[0] != "localhost" {
						panic(fmt.Sprintf("%s is not a valid bind ip address.", bind))
					}
					ip_addr := ip.String()
					if ip == nil && bind_parts[0] == "localhost" {
						ip_addr = "localhost"
					}

					port := ""
					if len(bind_parts) > 1 {
						port = bind_parts[1]
					}
					// TODO: Verify that the port string is a number

					currentHosts = append(currentHosts, HostConfig{ip_addr, port, "", port, field == "upload-scgi-host", "", true})
				} else if field == "admin-max-concurrent-connections" {
					integer, err := strconv.ParseInt(value, 10, strconv.IntSize)
					if err != nil {
						panic(err)
					}

					currentServer.MaxConcurrentConnections = int(integer)
				} else if field == "admin-rate-limit" {
					integer, err := strconv.ParseInt(value, 10, strconv.IntSize)
					if err != nil {
						panic(err)
					}

					currentServer.RateLimitDuration = time.Millisecond * time.Duration(integer)
				} else if field == "default-language" {
					currentServer.DefaultLanguage = value
				}
			} else if field == "type" {
				switch value {
				case "gemini":
					currentServer.Type = ServerType_Gemini
					currentServer.Router.NotFoundHandler = gemini_notFound
					currentServer.UploadRouter.NotFoundHandler = gemini_notFound
				case "misfin":
					currentServer.Type = ServerType_Misfin
				case "nex":
					currentServer.Type = ServerType_Nex
					currentServer.Router.NotFoundHandler = nex_notFound
					currentServer.UploadRouter.NotFoundHandler = nex_notFound
				case "gopher":
					currentServer.Type = ServerType_Gopher
					currentServer.Router.NotFoundHandler = gopher_notFound
					currentServer.UploadRouter.NotFoundHandler = gopher_notFound
				case "spartan":
					currentServer.Type = ServerType_Spartan
					currentServer.Router.NotFoundHandler = spartan_notFound
					currentServer.UploadRouter.NotFoundHandler = spartan_notFound
				case "scroll":
					currentServer.Type = ServerType_Scroll
					currentServer.Router.NotFoundHandler = scroll_notFound
					currentServer.UploadRouter.NotFoundHandler = scroll_notFound
				default:
					// TODO: Custom types
				}
			} else if field == "download-host" || field == "upload-host" {
				parts := strings.Split(value, " ")
				if len(parts) < 2 {
					// ERROR: Must have 2 or 3 arguments (`bindaddr:bindport hostname[:server_port] [cert_path]`)
					panic("Error: host must have 2 or 3 arguments (`bindaddr:bindport hostname[:server_port] [cert_path]`)")
				}

				bind := parts[0]
				serve := parts[1]

				bind_parts := strings.SplitN(bind, ":", 2)
				ip := net.ParseIP(bind_parts[0])
				if ip == nil && bind_parts[0] != "localhost" {
					panic(fmt.Sprintf("%s is not a valid bind ip address.", bind))
				}
				ip_addr := ip.String()
				if ip == nil && bind_parts[0] == "localhost" {
					ip_addr = "localhost"
				}

				port := ""
				if len(bind_parts) > 1 {
					port = bind_parts[1]
				}
				// TODO: Verify that the port string is a number

				serve_parts := strings.SplitN(serve, ":", 2)
				if len(serve_parts) == 0 {
					panic("Host must provide a serve hostname and port.")
				}
				hostname := serve_parts[0]
				serve_port := ""
				if len(serve_parts) > 1 {
					serve_port = serve_parts[1]
				}

				certificate_path := ""
				if len(parts) > 2 {
					certificate_path = strings.Join(parts[2:], "")
				}

				currentHosts = append(currentHosts, HostConfig{ip_addr, port, hostname, serve_port, field == "upload-host", certificate_path, false})
			} else if field == "download-scgi-host" || field == "upload-scgi-host" {
				parts := strings.Split(value, " ")
				if len(parts) < 1 {
					// ERROR: Must have one argument (`bindaddr:bindport`)
					panic("Error: host must have one argument (`bindaddr[:bindport]`)")
				}

				bind := parts[0]

				bind_parts := strings.SplitN(bind, ":", 2)
				ip := net.ParseIP(bind_parts[0])
				if ip == nil && bind_parts[0] != "localhost" {
					panic(fmt.Sprintf("%s is not a valid bind ip address.", bind))
				}
				ip_addr := ip.String()
				if ip == nil && bind_parts[0] == "localhost" {
					ip_addr = "localhost"
				}

				port := ""
				if len(bind_parts) > 1 {
					port = bind_parts[1]
				}
				// TODO: Verify that the port string is a number

				currentHosts = append(currentHosts, HostConfig{ip_addr, port, "", port, field == "upload-scgi-host", "", true})
			} else if field == "max-concurrent-connections" {
				integer, err := strconv.ParseInt(value, 10, strconv.IntSize)
				if err != nil {
					panic(err)
				}

				currentServer.MaxConcurrentConnections = int(integer)
			} else if field == "rate-limit" {
				integer, err := strconv.ParseInt(value, 10, strconv.IntSize)
				if err != nil {
					panic(err)
				}

				currentServer.RateLimitDuration = time.Millisecond * time.Duration(integer)
			} else if field == "directory" {
				currentServer.Directory = value
				if strings.HasPrefix(currentServer.Directory, "~") {
					homeDir, _ := os.UserHomeDir()
					currentServer.Directory = filepath.Join(homeDir, strings.TrimPrefix(currentServer.Directory, "~"))
				} else {
					currentServer.Directory, err = filepath.Abs(value)
					if err != nil {
						// TODO
					}
				}
			} else if field == "pubnix" {
				if value == "true" {
					currentServer.Pubnix = true
				} else if value == "false" {
					currentServer.Pubnix = false
				}
			} else if field == "default-language" {
				currentServer.DefaultLanguage = value
			}
		}
	}

	// Add last server to Servers list
	if currentServer.Name != "" {
		addServerConfig(config, &currentServer, currentHosts)
	}

	// Set defaults if SIS Section not provided (and make sure it is the first one in the array)
	if !hasSISSection {
		adminServer := VirtualServer{Name: "SIS", via_config: true, MaxConcurrentConnections: 2000, SIS: config /*, Hosts: make([]Host, 0, 1)*/}
		adminServer.Type = ServerType_Gemini
		adminServer.MaxConcurrentConnections = 100
		adminServer.Type = ServerType_Admin
		adminServer.Router.NotFoundHandler = gemini_notFound
		adminServer.UploadRouter.NotFoundHandler = gemini_notFound
		adminServer.msg = make(chan string, 100)
		adminServer.ipRateLimitList = cmap.New[RateLimitListItem]()
		adminServer.RateLimitDuration = time.Millisecond * 225
		adminServer.admin = true

		hosts := []HostConfig{
			HostConfig{"localhost", "1995", "localhost", "1995", false, filepath.Join(config.directory, "admin.pem"), false},
			HostConfig{"localhost", "1995", "localhost", "1995", true, filepath.Join(config.directory, "admin.pem"), false},
		}

		addServerConfig(config, &adminServer, hosts)
	}

	if config.directory == "" {
		panic("SIS directory not set.")
	}

	// Set default directories for all servers if directory not configured. Also set the default ServerTLSCertFilename and the FS
	for i := 0; i < len(config.servers); i++ {
		s := &config.servers[i]
		if s.Directory == "" {
			s.Directory = config.directory //filepath.Join(config.directory, s.Name)
		}

		// For security, panic when root directory is exposed.
		if s.Directory == "/" || s.Directory == "C:" || s.Directory == "C:/" || s.Directory == "C:\\" {
			panic("System's root directory is exposed.")
		}

		s.FS = DirFS(s.Directory).(ServerDirFS)
	}

	return nil
}

func addServerConfig(config *SISContext, currentServer *VirtualServer, currentHosts []HostConfig) VirtualServerHandle {
	if currentServer.Name != "" {
		// Add default hosts to admin server if none are provided
		if currentServer.admin && len(currentHosts) == 0 {
			currentHosts = []HostConfig{
				HostConfig{"localhost", "1995", "localhost", "1995", false, filepath.Join(config.directory, "admin.pem"), false},
				HostConfig{"localhost", "1995", "localhost", "1995", true, filepath.Join(config.directory, "admin.pem"), false},
			}
		}

		var domainsMap map[string]struct{} = make(map[string]struct{})
		var domains []string
		for _, host := range currentHosts {
			if _, exists := domainsMap[host.Hostname]; host.Hostname != "" && !exists {
				domainsMap[host.Hostname] = struct{}{}
				domains = append(domains, host.Hostname)
			}
		}

		// Add the server to the servers list
		config.servers = append(config.servers, *currentServer)
		currentServerHandle := VirtualServerHandle{len(config.servers) - 1, config}
		currentServerHandle.GetServer().handle = currentServerHandle
		currentServerHandle.AddHosts(currentHosts...)

		if currentServer.admin {
			config.adminServer = currentServerHandle
		}

		return currentServerHandle
	}

	return VirtualServerHandle{}
}

func saveSISConfig(filename string, config *SISContext) error {
	var builder strings.Builder

	// Handle Admin Server first
	adminServer := config.adminServer.GetServer()
	writeSectionName(&builder, "SIS")
	writeStringField(&builder, "directory", config.directory)
	writeIntField(&builder, "admin-max-concurrent-connections", adminServer.MaxConcurrentConnections)
	writeInt64Field(&builder, "admin-rate-limit", adminServer.RateLimitDuration.Milliseconds())
	writeStringField(&builder, "default-language", adminServer.DefaultLanguage)

	hosts := config.certmanager.GetServerHosts(adminServer.handle)
	for _, host := range hosts {
		cert, has_cert := config.certmanager.GetCertificateFromServerHost(host)
		writeHostField(&builder, host, has_cert, string(cert.CertFilepath)) // TODO: Write cert for a hostname only once
	}

	// Handle the rest of the servers
	for i := range config.servers {
		handle := config.servers[i].GetHandle()
		if !handle.via_config() || i == config.adminServer.index {
			continue
		}

		writeSectionName(&builder, handle.Name())
		writeTypeField(&builder, handle.Type())
		if handle.Directory() != "" {
			writeStringField(&builder, "directory", handle.Directory())
		}
		writeIntField(&builder, "max-concurrent-connections", handle.MaxConcurrentConnections())
		writeInt64Field(&builder, "rate-limit", handle.RateLimitDuration().Milliseconds())
		writeStringField(&builder, "default-language", handle.DefaultLanguage())

		if handle.Pubnix() {
			writeBoolField(&builder, "pubnix", handle.Pubnix())
		}

		hosts := config.certmanager.GetServerHosts(handle)
		//fmt.Printf("Hosts: %#v\n", hosts)
		for _, host := range hosts {
			cert, has_cert := config.certmanager.GetCertificateFromServerHost(host)
			writeHostField(&builder, host, has_cert, string(cert.CertFilepath)) // TODO: Write cert for a hostname only once
		}
	}

	builder.WriteString("\n")
	return os.WriteFile(filename, []byte(builder.String()), 0600)
}

func writeSectionName(builder *strings.Builder, name string) {
	builder.WriteString("[")
	builder.WriteString(name)
	builder.WriteString("]\n")
}

func writeStringField(builder *strings.Builder, field, value string) {
	builder.WriteString(field)
	builder.WriteString(": ")
	builder.WriteString(value)
	builder.WriteString("\n")
}

func writeTypeField(builder *strings.Builder, value ServerType) {
	switch value {
	case ServerType_Admin:
		// Note: Shouldn't be writing this to config file
	case ServerType_Gemini:
		writeStringField(builder, "type", "gemini")
	case ServerType_Misfin:
		writeStringField(builder, "type", "misfin")
	case ServerType_Gopher:
		writeStringField(builder, "type", "gopher")
	case ServerType_Nex:
		writeStringField(builder, "type", "nex")
	case ServerType_Spartan:
		writeStringField(builder, "type", "spartan")
	case ServerType_Scroll:
		writeStringField(builder, "type", "scroll")
	default:
		// TODO
	}
}

func writeIntField(builder *strings.Builder, field string, value int) {
	writeStringField(builder, field, strconv.FormatInt(int64(value), 10))
}

func writeInt64Field(builder *strings.Builder, field string, value int64) {
	writeStringField(builder, field, strconv.FormatInt(value, 10))
}

func writeBoolField(builder *strings.Builder, field string, value bool) {
	if value {
		writeStringField(builder, field, "true")
	} else {
		writeStringField(builder, field, "false")
	}
}

func writeHostField(builder *strings.Builder, host ServerHost, write_cert bool, certFilepath string) {
	if !host.Upload {
		if host.SCGI {
			builder.WriteString("download-scgi-host: ")
		} else {
			builder.WriteString("download-host: ")
		}
		fmt.Fprintf(builder, "%s:%s %s:%s", host.BindAddress, host.BindPort, host.Hostname, host.Port)
		if write_cert {
			builder.WriteRune(' ')
			builder.WriteString(certFilepath)
		}
		builder.WriteRune('\n')
	} else {
		if host.SCGI {
			builder.WriteString("upload-scgi-host: ")
		} else {
			builder.WriteString("upload-host: ")
		}
		fmt.Fprintf(builder, "%s:%s %s:%s", host.BindAddress, host.BindPort, host.Hostname, host.Port)
		if write_cert {
			builder.WriteRune(' ')
			builder.WriteString(certFilepath)
		}
		builder.WriteRune('\n')
	}
}
