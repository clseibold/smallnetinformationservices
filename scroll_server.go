package smallnetinformationservices

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"time"

	"github.com/djherbis/times"
	"github.com/gabriel-vasile/mimetype"
	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
)

func (s *VirtualServer) scroll_handleRequest(conn net.Conn, requestHeader RequestHeader, host ServerHost, ip string, remote_port string, _tcpConn *net.TCPConn) {
	// Get the URL
	request_without_query, _, _ := strings.Cut(requestHeader.Request, "?")
	URL, err := url.Parse(requestHeader.Request)
	if err != nil {
		conn.Write([]byte("59 Invalid url in request.\r\n"))
		return
	}

	fmt.Printf("[Scroll:%s] Requested Path: %s\n", s.Name, URL.EscapedPath())
	if s.Type != ServerType_Admin {
		s.SIS.Log(s.Type, s.Name, "Requested Path: %s", URL.EscapedPath())
	}

	// Do rate-limiting now that we have read the header of the request to check against
	if s.IpRateLimit_GetExpectedRedirectPath(ip) == URL.EscapedPath() {
		// Allow the redirect through and clear the expected redirect path
		s.IPRateLimit_ClearRedirectPath(ip)
	} else if s.IsIPRateLimited(ip) {
		// Rate limited, return a 44 slow down error
		conn.Write([]byte("44 1\r\n"))
		return
	}

	if requestHeader.Protocol == ProtocolType_Titan {
		node, globString, params := s.UploadRouter.Search(URL.EscapedPath())

		// Call handlers. The data should be read from within these handlers
		if node != nil && node.Handler != nil {
			// Set token as param
			params["token"] = requestHeader.Token
			parsed_ip := net.ParseIP(ip)
			if parsed_ip != nil && !parsed_ip.IsPrivate() {
				node.visits.Add(1)
			}
			node.currentConnections.Add(1)
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			flags.Set(RequestFlag_Upload)
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			routePath := node.GetRoutePath()
			prefix := InsertParamsIntoRouteString(node.prefix, params, globString, true)
			addSlashSuffix := strings.HasSuffix(URL.EscapedPath(), "/")
			requestPath := strings.TrimSuffix(strings.TrimSuffix(URL.EscapedPath(), "/"), prefix) //URL.EscapedPath()
			if addSlashSuffix && (len(requestPath) == 0 || requestPath[len(requestPath)-1] != '/') {
				requestPath = requestPath + "/"
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			node.Handler(&Request{ProtocolType_Titan, flags, host, routePath, prefix, requestPath, request_without_query, URL.RawQuery, 0, globString, conn, _tcpConn, params, requestHeader.ContentLength, []byte{}, requestHeader.Mime, requestHeader.UserCert, ip, remote_port, node.servePath, node.proxyRoute, proxiedUnder, "", node.gopherItemType, s.GetHandle(), []string{}, ScrollMetadata{}})
			node.currentConnections.Add(-1)
		} else {
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			flags.Set(RequestFlag_Upload)
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			s.Router.NotFoundHandler(&Request{ProtocolType_Titan, flags, host, "", "", "", request_without_query, URL.RawQuery, 0, globString, conn, _tcpConn, params, requestHeader.ContentLength, []byte{}, requestHeader.Mime, requestHeader.UserCert, ip, remote_port, "", node.proxyRoute, proxiedUnder, "", node.gopherItemType, s.GetHandle(), []string{}, ScrollMetadata{}})
		}
		params = nil
	} else if requestHeader.Protocol == ProtocolType_Scroll {
		node, globString, params := s.Router.Search(URL.EscapedPath())
		if node != nil && node.Handler != nil {
			parsed_ip := net.ParseIP(ip)
			if parsed_ip != nil && !parsed_ip.IsPrivate() {
				node.visits.Add(1)
			}
			node.currentConnections.Add(1)
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			if requestHeader.MetadataRequest {
				flags.Set(RequestFlag_ScrollMetadataRequested)
			}
			routePath := node.GetRoutePath()
			prefix := InsertParamsIntoRouteString(node.prefix, params, globString, true)
			addSlashSuffix := strings.HasSuffix(URL.EscapedPath(), "/")
			requestPath := strings.TrimSuffix(strings.TrimSuffix(URL.EscapedPath(), "/"), prefix) //URL.EscapedPath()
			if addSlashSuffix && (len(requestPath) == 0 || requestPath[len(requestPath)-1] != '/') {
				requestPath = requestPath + "/"
			}
			proxiedUnder := path.Join(requestHeader.SCGI_SCRIPT_NAME, prefix)
			node.Handler(&Request{ProtocolType_Scroll, flags, host, routePath, prefix, requestPath, request_without_query, URL.RawQuery, 0, globString, conn, _tcpConn, params, 0, []byte{}, "", requestHeader.UserCert, ip, remote_port, node.servePath, node.proxyRoute, proxiedUnder, "", node.gopherItemType, s.GetHandle(), requestHeader.Languages, ScrollMetadata{}})
			node.currentConnections.Add(-1)
		} else {
			flags := bitset.BitSet[RequestFlag, RequestFlag]{}
			if requestHeader.SCGI {
				flags.Set(RequestFlag_Proxied)
			}
			if requestHeader.MetadataRequest {
				flags.Set(RequestFlag_ScrollMetadataRequested)
			}
			proxiedUnder := requestHeader.SCGI_SCRIPT_NAME
			s.Router.NotFoundHandler(&Request{ProtocolType_Scroll, flags, host, "", "", "", request_without_query, URL.RawQuery, 0, globString, conn, _tcpConn, params, 0, []byte{}, "", requestHeader.UserCert, ip, remote_port, "", "", proxiedUnder, "", '1', s.GetHandle(), requestHeader.Languages, ScrollMetadata{}})
		}
		params = nil
	}
}

// ----- Route Request Handlers -----

func scroll_notFound(request *Request) {
	request.NotFound("Not Found.")
}

func scroll_directoryNotFile(request *Request) {
	request.TemporaryFailure("/%s/ is a directory, not a file.", request.requestString)
}

// TODO: Cache file's mimetype!!! // store in request?
func scroll_handleFile(request *Request) {
	info, info_err := os.Stat(request.servePath)
	if info_err != nil {
		gemini_notFound(request)
		return // Return error
	} else if info.IsDir() {
		request.NotFound("Not found.")
		return
	} else if runtime.GOOS != "windows" && info.Mode().Perm()&(1<<2) == 0 { // Must be world-readable file or world-readable-executable directory on non-Windows systems.
		request.NotFound("Not found.")
		return
	}

	// Disable convert mode for most files (directory index files will be handled by the handleDirectory method)
	if !(filepath.Ext(request.servePath) == ".scroll" && (request.Type == ProtocolType_Gemini || request.Type == ProtocolType_Spartan)) {
		request.DisableConvertMode()
	}

	err := request.File(request.servePath)
	if err != nil {
		scroll_notFound(request)
		return
	}
}

// TODO: NotFoundHandler vs. request.NotFound?????
func scroll_handleDirectory(request *Request) {
	// Combine servePath with request path
	p := filepath.Join(request.servePath, request.GlobString)
	directoryName := filepath.Base(p)
	//fmt.Printf("Serving: %s\n", p)

	// Check if file, and serve the file instead
	info, err := os.Stat(p)
	if err != nil {
		scroll_notFound(request)
		return // Return error
	} else if runtime.GOOS != "windows" && info.Mode().Perm()&(1<<2) == 0 { // Must be world-readable
		request.NotFound("Not found.")
		return
	} else if info.IsDir() && !strings.HasSuffix(request.requestPath, "/") { // TODO: request.TrailingSlash
		// If directory but request string doesn't end in "/", then redirect to proper route
		//gemini_directoryNotFile(request)
		request.Redirect("%s", request.requestPath+"/")
		return
	} else if !info.IsDir() {
		// Disable convert mode for most files within directories (directory listing/index files will still be converted further below)
		if !(filepath.Ext(p) == ".scroll" && (request.Type == ProtocolType_Gemini || request.Type == ProtocolType_Spartan)) {
			request.DisableConvertMode()
		}

		// If not a directory, serve the file
		err := request.File(p)
		if err != nil {
			scroll_notFound(request)
			return
		}
		return
	}

	// Get the Abstract file for the directory (.abstract), if available
	if request.flags.Test(RequestFlag_ScrollMetadataRequested) {
		abstractFilepath := filepath.Join(p, ".abstract")
		abstractData, err := os.ReadFile(abstractFilepath)
		if err == nil {
			request.scrollMetadata.Abstract = string(abstractData)
		}
	}

	// Try index.scroll. If doesn't exist, then try index.gmi, and if that doesn't exist, then serve directory listing.
	indexFilepath := filepath.Join(p, "index.scroll")
	err = request.FileMimetype("text/scroll", indexFilepath)
	//indexFile, err := os.Open(indexFilepath)
	if err == nil {
		return
	} else if errors.Is(err, os.ErrNotExist) || errors.Is(err, fs.ErrNotExist) {
		indexFilepath := filepath.Join(p, "index.gmi")
		err = request.FileMimetype("text/gemini", indexFilepath)
		//indexFile, err := os.Open(indexFilepath)
		if err == nil {
			return
		}
	}

	// Set directory publishdate and modificationdate stuff
	stat_times, _ := times.Stat(p)
	if stat_times.HasBirthTime() {
		request.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	request.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	if request.flags.Test(RequestFlag_ScrollMetadataRequested) {
		// If we get here, then the scroll/gemini files were not found. Send the abstract
		if request.scrollMetadata.Abstract != "" {
			request.Abstract("text/scroll", request.scrollMetadata.Abstract)
		} else {
			request.Abstract("text/scroll", "# "+directoryName+"\n")
		}
		return
	}

	// Check for .modified and .desc files, used for sorting the directory listing.
	modified := true
	if _, err := os.Stat(filepath.Join(p, ".modified")); err != nil {
		modified = false
	}
	asc := false
	if _, err := os.Stat(filepath.Join(p, ".desc")); err != nil {
		asc = true
	}

	entries, err := os.ReadDir(p)
	if err != nil {
		scroll_notFound(request)
		return
	}
	sort.Slice(entries, func(i, j int) bool {
		if modified {
			st1, err := entries[i].Info()
			if err != nil {
				return false // TODO
			}
			st2, err := entries[j].Info()
			if err != nil {
				return false
			}
			return st1.ModTime().After(st2.ModTime())
		} else if asc {
			return entries[i].Name() < entries[j].Name()
		} else {
			return entries[i].Name() > entries[j].Name()
		}
	})

	// Send the directory listing
	request.Scroll(fmt.Sprintf("# %s\n", directoryName))
	if request.requestPath != "." { // TODO: Print ".." if the parent route has a handler?
		request.Scroll("=> ../ ../\n")
	}
	for _, entry := range entries {
		name := entry.Name()
		if filepath.Ext(name) == ".abstract" { // Hide abstract files
			continue
		}
		if CertFilename(name) && !request.flags.Test(RequestFlag_AllowServeCert) {
			continue
		} else if PrivacySensitiveFilename(name) {
			continue
		}
		if DotFilename(name) && !request.flags.Test(RequestFlag_AllowServeDotFiles) {
			continue
		}
		// TODO: Check permissions of file?
		info, err := entry.Info()
		if err != nil {
			continue
		}
		if info.Mode()&(1<<2) == 0 {
			continue
		}
		if entry.IsDir() {
			name = name + "/"
		}
		if request.Type == ProtocolType_Gopher && request.flags.Test(RequestFlag_ConvertMode) && !entry.IsDir() { // For proxying
			mt := ""
			extension := filepath.Ext(name)
			mt = "text/plain"
			if strings.HasSuffix(name, ".gmi") || strings.HasSuffix(name, ".gemini") {
				mt = "text/gemini"
			} else if strings.HasSuffix(name, ".scroll") {
				mt = "text/scroll"
			} else if strings.HasSuffix(name, "index") || strings.HasSuffix(name, ".nex") {
				mt = "text/nex" // TODO: Assume nex for this for now, but come up with a better way later.
			} else if strings.HasSuffix(name, "gophermap") {
				mt = "application/gopher-menu"
			} else if extension == ".pem" {
				mt = "application/x-pem-file"
			} else {
				path := filepath.Join(p, name)
				file, err := os.Open(path)
				if err != nil {
					continue
				}
				defer file.Close()

				mime, err := mimetype.DetectReader(file)
				if err != nil {
					continue
				}
				mt = mime.String()
			}

			itemtype := MimetypeToGopherItemtype(mt, "")
			request.GophermapLine(string(itemtype), name, path.Join(request.ProxiedUnder, request.prefix, request.Path(), name), "", "")
		} else {
			request.Scroll(fmt.Sprintf("=> %s\n", name))
		}
	}
}

// Hands request information off to a CGI script.
// If in convert mode, stdout should be handled by SIS's request functions.
func scroll_handleCGI(request *Request) {
	// Set up environment variables
	env := os.Environ()
	env = append(env, "GATEWAY_INTERFACE=CGI/1.1")
	env = append(env, "SERVER_PROTOCOL=SCROLL")
	env = append(env, "SCHEME=scroll")
	env = append(env, "REQUEST_METHOD=")
	env = append(env, "SERVER_SOFTWARE=SIS/"+Version)
	env = append(env, fmt.Sprintf("SERVER_NAME=%s", request.Hostname()))
	env = append(env, fmt.Sprintf("SERVER_PORT=%s", request.Host.Port))
	env = append(env, fmt.Sprintf("REAL_LISTEN_ADDR=%s", request.Host.BindAddress))
	env = append(env, fmt.Sprintf("REAL_LISTEN_PORT=%s", request.Host.BindPort))
	env = append(env, fmt.Sprintf("REMOTE_ADDR=%s", request.IP))
	env = append(env, fmt.Sprintf("REMOTE_PORT=%s", request.RemotePort))
	env = append(env, fmt.Sprintf("REMOTE_HOST=%s:%s", request.IP, request.RemotePort)) // TODO
	script_name := strings.TrimSuffix(strings.TrimSuffix(request.RawPath(), request.GlobString), "/")
	env = append(env, "SCRIPT_NAME="+script_name) // TODO

	if (request.UserCert != ClientCertificate{}) {
		env = append(env, fmt.Sprintf("AUTH_TYPE=CERTIFICATE"))
		env = append(env, fmt.Sprintf("TLS_CLIENT_HASH=%s", request.UserCertHash())) // TODO: JetForce prepends with "SHA256:"
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_BEFORE=%s", request.UserCert.NotBefore.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_AFTER=%s", request.UserCert.NotAfter.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SERIAL_NUMBER=%s", request.UserCert.SerialNumber))
		env = append(env, fmt.Sprintf("TLS_CLIENT_ISSUER=%s", request.UserCert.Issuer))
		// // // // env = append(env, fmt.Sprintf("TLS_CLIENT_AUTHORIZED=%s", "1")) // TODO

		env = append(env, fmt.Sprintf("REMOTE_IDENT=%s", request.UserCertHash()))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SUBJECT=%s", request.UserCert.Subject))
		env = append(env, fmt.Sprintf("REMOTE_USER=%s", request.UserCert.Subject))
		env = append(env, fmt.Sprintf("REMOTE_USER_ID=%s", request.UserCert.SubjectUserID)) // TODO: Jetforce uses the CN, not the OID_USER_ID
	}

	env = append(env, fmt.Sprintf("REQUEST_URI=%s", request.requestString))
	env = append(env, fmt.Sprintf("SCROLL_URL=%s", request.requestString))
	env = append(env, fmt.Sprintf("URL=%s", request.requestString))
	env = append(env, fmt.Sprintf("REQUEST_DOMAIN=%s", request.Hostname()))
	env = append(env, fmt.Sprintf("SCROLL_URL_PATH=%s", request.RawPath()))
	env = append(env, fmt.Sprintf("PATH_INFO=%s", "/"+request.GlobString)) // TODO: Just use the path after the prefix for the route to the script
	env = append(env, fmt.Sprintf("REQUEST_PATH=%s", request.RawPath()))
	query, _ := request.Query()
	env = append(env, fmt.Sprintf("QUERY_STRING=%s", query))

	//if tlsConn, isTls := conn.(*tls.Conn); isTls {
	env = append(env, fmt.Sprintf("SSL_TLS_SNI=%s", request.Hostname()))
	// env = append(env, fmt.Sprintf("TLS_CIPHER=%s", tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)))
	// env = append(env, fmt.Sprintf("TLS_VERSION=%s", tls.VersionName(tlsConn.ConnectionState().Version)))
	//}

	// Check CGI script permissions
	realServePath := strings.TrimPrefix(request.servePath, "cgi:")
	if runtime.GOOS != "windows" {
		stat, stat_err := os.Stat(realServePath)
		if stat_err != nil {
			fmt.Println("Error executing CGI script. Could not stat script.")
			request.CGIFailure("CGI error. Incorrect permissions on script.")
			return
		}
		worldPermission := stat.Mode().Perm() & 0007
		if worldPermission != 5 { // Is not Read and Execute
			fmt.Println("Error executing CGI script. Incorrect permissions on script.")
			request.CGIFailure("CGI error. Incorrect permissions on script.")
			return
		}
	}

	// Execute the CGI script
	cmd := exec.Command(realServePath)
	cmd.Env = env
	cmd.Stdout = request.readwriter
	cmd.Stderr = os.Stderr
	cmd.Dir = filepath.Dir(realServePath)

	// Run the command
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error executing CGI script:", err)
		request.CGIFailure("CGI error.")
		return
	}
}

func scroll_handleSCGI(request *Request) {
	query, _ := request.Query()

	// Prepare SCGI request
	script_name := strings.TrimSuffix(strings.TrimSuffix(request.RawPath(), request.GlobString), "/")
	headers := map[string]string{
		"GATEWAY_INTERFACE": "CGI/1.1",
		"CONTENT_LENGTH":    "0",
		"SCGI":              "1",
		"SERVER_PROTOCOL":   "SCROLL",
		"SCHEME":            "scroll",
		"REQUEST_METHOD":    "",
		"SERVER_SOFTWARE":   "SIS/" + Version,
		"SERVER_NAME":       request.Hostname(),
		"SERVER_PORT":       request.Host.Port, // port being forwarded from router
		"REAL_LISTEN_ADDR":  request.Host.BindAddress,
		"REAL_LISTEN_PORT":  request.Host.BindPort, // listenPort
		"REMOTE_ADDR":       request.IP,
		"REMOTE_PORT":       request.RemotePort,
		"REMOTE_HOST":       fmt.Sprintf("%s:%s", request.IP, request.RemotePort),
		//"GEMINI_DOCUMENT_ROOT": filepath.Dir(request.servePath),

		"SCRIPT_NAME":     script_name,
		"REQUEST_URI":     request.requestString,
		"SCROLL_URL":      request.requestString,
		"URL":             request.requestString,
		"REQUEST_DOMAIN":  request.Hostname(),
		"SCROLL_URL_PATH": request.RawPath(),
		"PATH_INFO":       "/" + request.GlobString, // NOTE: Just use the path after the prefix for the route to the script
		"REQUEST_PATH":    request.RawPath(),
		"QUERY_STRING":    query,
	}

	if (request.UserCert != ClientCertificate{}) {
		headers["AUTH_TYPE"] = "CERTIFICATE"
		headers["TLS_CLIENT_HASH"] = request.UserCertHash()
		headers["TLS_CLIENT_NOT_BEFORE"] = request.UserCert.NotBefore.Format(time.RFC3339)
		headers["TLS_CLIENT_NOT_AFTER"] = request.UserCert.NotAfter.Format(time.RFC3339)
		headers["TLS_CLIENT_SERIAL_NUMBER"] = request.UserCert.SerialNumber
		headers["TLS_CLIENT_ISSUER"] = request.UserCert.Issuer
		headers["REMOTE_IDENT"] = request.UserCertHash()
		headers["TLS_CLIENT_SUBJECT"] = request.UserCert.Subject
		headers["REMOTE_USER"] = request.UserCert.Subject
		headers["REMOTE_USER_ID"] = request.UserCert.SubjectUserID
	}

	//if tlsConn, isTls := conn.(*tls.Conn); isTls {
	headers["SSL_TLS_SNI"] = request.Hostname()
	// headers["TLS_CIPHER"] = tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)
	// headers["TLS_VERSION"] = tls.VersionName(tlsConn.ConnectionState().Version)
	//}

	// Create SCGI request
	var buf bytes.Buffer
	for k, v := range headers {
		buf.WriteString(k)
		buf.WriteByte(0)
		buf.WriteString(v)
		buf.WriteByte(0)
	}
	headerLength := buf.Len()
	fullRequest := fmt.Sprintf("%d:%s,", headerLength, buf.String())

	// Connect to SCGI server
	address := strings.TrimSuffix(strings.TrimPrefix(request.servePath, "scgi://"), "/")
	scgiConn, err := net.Dial("tcp", address)
	if err != nil {
		request.CGIFailure("SCGI connection error.")
		return
	}
	defer scgiConn.Close()

	// Send SCGI request
	_, err = scgiConn.Write([]byte(fullRequest))
	if err != nil {
		fmt.Println("Error sending SCGI request:", err)
		request.CGIFailure("SCGI request error.")
		return
	}

	// Get response code by reading the first byte of response
	var responseCode []byte
	switch request.Type {
	case ProtocolType_Scroll, ProtocolType_Gemini, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		responseCode = make([]byte, 2)
		io.ReadFull(scgiConn, responseCode)
		if strings.HasPrefix(string(responseCode), "3") {
			// Read rest of response, and deal with the redirect for rate-limiting reasons.
			response, _ := io.ReadAll(scgiConn)
			r_string := strings.TrimSpace(string(response))
			request.Redirect("%s", r_string)
			return
		}
	case ProtocolType_Spartan:
		responseCode = make([]byte, 1)
		io.ReadFull(scgiConn, responseCode)
		if string(responseCode) == "3" {
			// Read rest of response, and deal with the redirect for rate-limiting reasons.
			response, _ := io.ReadAll(scgiConn)
			r_string := strings.TrimSpace(string(response))
			request.Redirect("%s", r_string)
			return
		}
	}

	// Send over response code
	_, err = io.Copy(request.readwriter, strings.NewReader(string(responseCode)))
	if err != nil {
		fmt.Println("Error reading SCGI response:", err)
		request.CGIFailure("SCGI response error.")
		return
	}

	// Send Upload content if applicable
	if request.Upload() {
		uploadData, err := request.GetUploadData()
		if err != nil {
			fmt.Println("Error sending SCGI request:", err)
			request.CGIFailure("SCGI request error.")
			return
		}

		_, err = scgiConn.Write([]byte(uploadData))
		if err != nil {
			fmt.Println("Error sending SCGI request:", err)
			request.CGIFailure("SCGI request error.")
			return
		}
	}

	// Read response from SCGI server and write to client
	_, err = io.Copy(request.readwriter, scgiConn)
	if err != nil {
		fmt.Println("Error reading SCGI response:", err)
		request.CGIFailure("SCGI response error.")
		return
	}
}
