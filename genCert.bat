openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout scrollprotocol.key -out scrollprotocol.crt -subj "/C=US/ST=Iowa/L=Sioux City/O=Scroll Protocol/OU=IT/CN=scrollprotocol.us.to" -addext "subjectAltName=DNS:scrollprotocol.us.to"

openssl req -x509 -nodes -days 36500 -newkey rsa:2048 -keyout auragem.key -out auragem.crt -subj "/C=US/ST=Iowa/L=Sioux City/O=AuraGem/OU=IT/CN=auragem.letz.dev" -addext "subjectAltName=DNS:auragem.letz.dev,DNS:auragemhkzsr5rowsaxauti6yhinsaa43wjtcqxhh7fw5tijdoqbreyd.onion"
