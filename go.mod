module gitlab.com/sis-suite/smallnetinformationservices

go 1.23.0

require (
	github.com/clseibold/go-gemini v0.0.0-20240226215632-c39755b92b21
	github.com/cretz/bine v0.2.0
	github.com/dhowden/tag v0.0.0-20240417053706-3d75831295e8
	github.com/djherbis/times v1.6.0
	github.com/eidolon/wordwrap v0.0.0-20161011182207-e0f54129b8bb
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/gammazero/deque v1.0.0
	github.com/orcaman/concurrent-map/v2 v2.0.1
	github.com/warpfork/go-fsx v0.4.0
	golang.org/x/sync v0.5.0
	golang.org/x/text v0.14.0
)

require (
	golang.org/x/crypto v0.19.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
