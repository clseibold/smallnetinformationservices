package smallnetinformationservices

import (
	"crypto/tls"
	"net"
)

type MaybeTLSListener struct {
	net.Listener
	tlsConfig *tls.Config
}

func (l MaybeTLSListener) Accept() (net.Conn, error) {
	conn, err := l.Listener.Accept()
	if err != nil {
		return nil, err
	}

	// Peek at the first byte to determine if it's a TLS handshake
	buf := make([]byte, 1)
	_, err = conn.Read(buf)
	if err != nil {
		return nil, err
	}

	// Put the byte back into the connection
	conn = &peekedConn{Conn: conn, peeked: buf}

	if buf[0] == 0x16 { // TLS handshake starts with 0x16
		return tls.Server(conn, l.tlsConfig), nil
	}

	return conn, nil
}
