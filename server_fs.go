package smallnetinformationservices

import (
	"io/fs"
	"os"
	"path/filepath"

	"github.com/warpfork/go-fsx"
)

// Extends go-fsx/osfs to add ReadDir and ReadFile methods
type ServerFS interface {
	fs.FS
	fs.ReadFileFS
	fs.ReadDirFS
	fsx.FS
	fsx.FSSupportingWrite
	fsx.FSSupportingMkSymlink
	fsx.FSSupportingReadlink
	fsx.FSSupportingStat
	WriteFile(name string, data []byte, perm fs.FileMode) error
}

func DirFS(name string) fs.FS {
	return ServerDirFS(name)
}

type ServerDirFS string

func (dir ServerDirFS) Open(name string) (fs.File, error) {
	return os.DirFS(string(dir)).Open(name)
}

func (dir ServerDirFS) ReadFile(name string) ([]byte, error) {
	return os.DirFS(string(dir)).(fs.ReadFileFS).ReadFile(name)
}

func (dir ServerDirFS) ReadDir(name string) ([]fs.DirEntry, error) {
	return os.DirFS(string(dir)).(fs.ReadDirFS).ReadDir(name)
}

func (dir ServerDirFS) Stat(name string) (fs.FileInfo, error) {
	return os.DirFS(string(dir)).(fsx.FSSupportingStat).Stat(name)
}

func (dir ServerDirFS) OpenFile(name string, flag int, perm fs.FileMode) (fs.File, error) {
	name = filepath.Clean(name)
	return os.OpenFile(filepath.Join(string(dir), name), flag, perm)
}

func (dir ServerDirFS) WriteFile(name string, data []byte, perm fs.FileMode) error {
	name = filepath.Clean(name)
	return os.WriteFile(filepath.Join(string(dir), name), data, perm)
}

func (dir ServerDirFS) Mkdir(name string, perm fs.FileMode) error {
	name = filepath.Clean(name)
	return os.Mkdir(filepath.Join(string(dir), name), perm)
}

func (dir ServerDirFS) Readlink(name string) (string, error) {
	name = filepath.Clean(name)
	return os.Readlink(filepath.Join(string(dir), name))
}

func (dir ServerDirFS) Lstat(name string) (fs.FileInfo, error) {
	name = filepath.Clean(name)
	return os.Lstat(filepath.Join(string(dir), name))
}

// NOTE: The target is *not* relative to the dirFS
func (dir ServerDirFS) MkSymlink(name, target string) error {
	name = filepath.Clean(name)
	return os.Symlink(target, filepath.Join(string(dir), name))
}
