package smallnetinformationservices

import (
	"container/ring"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/clseibold/go-gemini"
	"github.com/cretz/bine/tor"
	cmap "github.com/orcaman/concurrent-map/v2"
)

// Maps address to fingerprint. Shared between all servers
var TOFUMap map[string]string
var Version string = "0.1"

func printBanner() {
	fmt.Printf("----- Smallnet Information Services -----\n")
	fmt.Printf("Created by Chrsitian Lee Seibold, 2023-2025.\n")
	fmt.Printf("Version %s\n", Version)
	fmt.Printf("\n")
}

type SISContext struct {
	directory   string // Guaranteed to be absolute
	adminServer VirtualServerHandle
	configless  bool // No configurations or admin server.

	servers       []VirtualServer // Backing storage for Servers
	certmanager   *CertificateManager
	portListeners map[PortListenerID]*PortListener
	tor           *tor.Tor

	wg          *sync.WaitGroup
	ipBlocklist cmap.ConcurrentMap[string, struct{}]
	msg         chan string

	updateLink string // Link to know where to download an update
	logBuffer  *ring.Ring
	logChan    chan Log
}

type Log struct {
	serverType ServerType
	label      string // Usually the server name
	msg        string
}

// Initialize SIS in configless mode, without an admin server. This is primarily for using SIS as a library to create one-off servers, especially for SCGI servers.
func InitConfiglessMode() (*SISContext, error) {
	context := &SISContext{certmanager: NewCertManager()}
	context.logBuffer = ring.New(100)
	context.logChan = make(chan Log, 50)
	context.portListeners = map[PortListenerID]*PortListener{}
	context.wg = &sync.WaitGroup{}
	context.ipBlocklist = cmap.New[struct{}]()
	context.msg = make(chan string, len(context.servers)*5)
	context.configless = true

	return context, nil
}

func InitSIS(directory string) (*SISContext, error) {
	var absErr error
	directory, absErr = filepath.Abs(directory)
	if absErr != nil {
		// TODO
	}

	// Make directory if it doesn't exist
	err := os.MkdirAll(directory, 0700)
	if err != nil {
		return &SISContext{}, err
	}

	// Load configuration
	context := &SISContext{directory: directory, certmanager: NewCertManager()}
	context.logBuffer = ring.New(100)
	context.logChan = make(chan Log, 50)
	context.portListeners = map[PortListenerID]*PortListener{}
	err = loadSISConfig(filepath.Join(directory, "sis.conf"), context)
	if err != nil {
		return context, err
	}

	// Make server directories and load routing for all servers
	// TODO: Move this to AddServer instead?
	for i := 0; i < len(context.servers); i++ {
		s := &context.servers[i]
		err := os.MkdirAll(s.Directory, 0777) // Server serve directories should be world-readable and world-executable
		if err != nil {
			// TODO: Print out error
			continue
		}
		err = s.LoadRoutes()
		//fmt.Printf("%s Routes:\n", s.Name)
		//s.Router.PrintRouteTree()
		//fmt.Printf("\n\n")
		if err != nil {
			// TODO: Print out error
			continue
		}
	}

	// Add default routing for admin server
	admin_Init(context.adminServer.GetServer())

	context.wg = &sync.WaitGroup{}
	context.ipBlocklist = cmap.New[struct{}]()
	context.msg = make(chan string, len(context.servers)*5) // TODO
	return context, nil
}

// Concurrent-safe
func (context *SISContext) Log(serverType ServerType, label string, msgFormat string, args ...any) {
	context.logChan <- Log{serverType, label, fmt.Sprintf(msgFormat, args...)}
}

func (context *SISContext) addToLog(log Log) {
	typeString := ""
	switch log.serverType {
	case ServerType_Admin:
		context.logBuffer.Value = "[Gemini:SIS Manager] " + log.msg
		context.logBuffer = context.logBuffer.Next()
		return
	case ServerType_Gemini:
		typeString = "Gemini:"
	case ServerType_Gopher:
		typeString = "Gopher:"
	case ServerType_Misfin:
		typeString = "Misfin:"
	case ServerType_Nex:
		typeString = "Nex:"
	case ServerType_Spartan:
		typeString = "Spartan:"
	case ServerType_Scroll:
		typeString = "Scroll:"
	default:
		typeString = ""
	}
	context.logBuffer.Value = "[" + typeString + log.label + "] " + log.msg
	context.logBuffer = context.logBuffer.Next()
}

func (context *SISContext) GetTor() *tor.Tor {
	if context.tor == nil {
		var err error
		context.tor, err = tor.Start(nil, &tor.StartConf{DataDir: filepath.Join(context.directory, "TorData"), EnableNetwork: true})
		if err != nil {
			fmt.Printf("Failed to start tor: %v", err)
		}
	}

	return context.tor
}

// Set a gemini link to update the server
func (context *SISContext) SetUpdateLink(link string) {
	context.updateLink = link
}

// You must have the new subprocess delete the renamed filename (which is returned by the function). You can do this by redirecting to a route that deletes the old file. By the time the redirect happens, the server would have been updated.
// You can pass in the data of the executable instead of downloading the data from the update link.
func (context *SISContext) UpdateAndRestart(data []byte) string {
	// If there's no update link, then just do a full restart of the server
	if context.updateLink == "" && data == nil {
		context.FullRestart()
		return ""
	}

	// Try to download the update from the update link
	var updateData []byte = nil
	if data == nil {
		// Download new server executable into the old exepath
		client := gemini.DefaultClient
		adminServer := context.AdminServer() // TODO: This stuff is a bit meh.
		host := adminServer.GetPreferredHost(ProtocolType_Gemini, false, false)
		adminCert, _ := context.certmanager.GetCertificateFromServerHost(host)
		resp, err := client.FetchWithCert(context.updateLink, adminCert.Certificate.Raw, adminCert.PrivateKeyPemBytes)
		if err != nil {
			// Error, do a regular full restart
			context.FullRestart()
			return ""
		}
		if resp.Status != 20 {
			// Error, do a regular full restart
			context.FullRestart()
			return ""
		}
		updateData, err = io.ReadAll(resp.Body)
		if err != nil {
			context.FullRestart()
			return ""
		}
		resp.Body.Close()
	}

	// Get current executable path
	exepath, err := os.Executable()
	fmt.Printf("Executable Path: %s\n", exepath)
	if err != nil {
		panic("Failed to get executable path.")
	}
	exepath, err = filepath.EvalSymlinks(exepath)
	if err != nil {
		panic("Failed to evaluate symlink.")
	}

	// Rename the executable path to a different name
	newname := filepath.Base(exepath) + "_old" + strconv.FormatInt(time.Now().Unix(), 10)
	newpath := filepath.Join(filepath.Dir(exepath), newname)
	err = os.Rename(exepath, newpath)
	if err != nil {
		panic("Failed to rename executable.")
	}

	// Make all existing portListeners go into maintenance mode
	context.EnterMaintenanceMode_All()

	if data != nil {
		fmt.Printf("[SIS] Updating server with uploaded data.\n")
		os.WriteFile(exepath, data, 0600)
	} else {
		fmt.Printf("[SIS] Updating server via the update link.\n")
		os.WriteFile(exepath, updateData, 0600)
	}

	// Shut down all of the currently running portListeners
	context.ShutdownPortListeners()

	// Run the new executable (forks the process into one thread)
	cmd := exec.Command(exepath, os.Args[1:]...)
	cmd.Start()

	context.ShutdownSIS()

	return newpath
}

func (context *SISContext) FullRestart() {
	// Get current executable path
	exepath, err := os.Executable()
	fmt.Printf("Executable Path: %s\n", exepath)
	if err != nil {
		panic("Failed to get executable path.")
	}
	exepath, err = filepath.EvalSymlinks(exepath)
	if err != nil {
		panic("Failed to evaluate symlink.")
	}

	// Shut down all of the currently running portListeners
	context.ShutdownPortListeners()

	// Run the executable
	cmd := exec.Command(exepath, os.Args[1:]...)
	cmd.Start()

	// Shut down the current process
	context.ShutdownSIS()
}

func (context *SISContext) GetOrCreatePortListener(BindAddress string, BindPort string) *PortListener {
	if _, has := context.portListeners[PortListenerID{BindAddress, BindPort}]; has {
		return context.portListeners[PortListenerID{BindAddress, BindPort}]
	} else {
		newPortListener := NewPortListener(context, BindAddress, BindPort)
		context.portListeners[PortListenerID{BindAddress, BindPort}] = newPortListener
		return newPortListener
	}
}

func (context *SISContext) GetPortListener(BindAddress string, Port string) *PortListener {
	if _, has := context.portListeners[PortListenerID{BindAddress, Port}]; has {
		return context.portListeners[PortListenerID{BindAddress, Port}]
	} else {
		return nil
	}
}

func (context *SISContext) AddServerHostMapping(BindAddress string, BindPort string, Hostname string, ServePort string, Upload bool, scgi bool, server VirtualServerHandle) {
	context.certmanager.AddServerHostMapping(BindAddress, BindPort, Hostname, ServePort, Upload, scgi, server)
}

func (context *SISContext) GetOrAddCertificateConnection(certFilepath string, bindaddress string, bindport string, hostname string) CertID {
	return context.certmanager.AddCertificateConnection(certFilepath, bindaddress, bindport, hostname)
}

func (context *SISContext) BlockIP(ip string) {
	context.ipBlocklist.Set(ip, struct{}{})
}
func (context *SISContext) AllowIP(ip string) {
	context.ipBlocklist.Remove(ip)
}
func (context *SISContext) IsIPBlocked(ip string) bool {
	return context.ipBlocklist.Has(ip)
}

func (context *SISContext) FindServerByName(name string) VirtualServerHandle {
	for i := 0; i < len(context.servers); i++ {
		s := &context.servers[i]
		if s.Name == name {
			return VirtualServerHandle{i, context}
		}
	}
	return VirtualServerHandle{}
}

// Saves SIS Config File
// TODO: Mutex?
func (context *SISContext) SaveConfiguration() error {
	if context.configless {
		return nil
	}
	return saveSISConfig(filepath.Join(context.directory, "sis.conf"), context)
	// TODO: Save configuration of all the servers?
}

// Get the Admin server, which will let you customize it (like by adding routes and handlers).
func (context *SISContext) AdminServer() *VirtualServer { // TODO: Check that admin server is always the first one
	if context.configless {
		return nil
	}
	return context.adminServer.GetServer()
}

// TODO: Other settings one might want to specify include Directory, FS, Pubnix, and MaxConcurrentConnections
func (context *SISContext) CreateServer(t ServerType, name string, defaultLanguage string, hosts ...HostConfig) (VirtualServerHandle, error) {
	return context.AddServer(VirtualServer{Name: name, Type: t, DefaultLanguage: defaultLanguage})
}

// When directory is empty, creates subdirectory under SIS directory named with the server's name.
// TODO: Take in a ServerConfig, rather than a Server that gets copied
// TODO: Make loadSISConfig use this function?
func (context *SISContext) AddServer(server VirtualServer, hosts ...HostConfig) (VirtualServerHandle, error) {
	// Server Names should not have spaces in them
	if strings.Contains(server.Name, " ") {
		panic("Server names cannot have spaces.")
	} else if strings.TrimSpace(server.Name) == "" {
		panic("Server names must not be blank.")
	}

	// Make sure server of same name doesn't already exist, or that server of same port doesn't already exist
	if (context.FindServerByName(server.Name) != VirtualServerHandle{}) {
		return VirtualServerHandle{}, errors.New("Server name already exists.")
	}

	// Set directory
	if server.Directory == "" {
		server.Directory = filepath.Join(context.directory, server.Name)
	} else if strings.HasPrefix(server.Directory, "~") {
		homeDir, _ := os.UserHomeDir()
		server.Directory = filepath.Join(homeDir, strings.TrimPrefix(server.Directory, "~"))
	}

	// For security, panic when system's root directory is exposed.
	if server.Directory == "/" || server.Directory == "C:" || server.Directory == "C:/" || server.Directory == "C:\\" {
		panic("System's root directory is exposed.")
	}

	// Make server directory
	if !context.configless { // TODO
		os.MkdirAll(server.Directory, 0600)

		// Setup the FS from the server Directory
		server.FS = DirFS(server.Directory).(ServerFS)
	}

	// Other Server settings
	if server.MaxConcurrentConnections == 0 {
		server.MaxConcurrentConnections = 2000
	}
	server.msg = make(chan string, 100) // TODO
	server.KeyValStore = cmap.New[interface{}]()
	//server.via_config = false // TODO

	// Add Default NotFoundHandler and supported Protocols for each server type.
	switch server.Type {
	case ServerType_Admin:
		server.Router.NotFoundHandler = gemini_notFound
		server.UploadRouter.NotFoundHandler = gemini_notFound
		server.Protocols.Set(ProtocolType_Gemini)
		server.Protocols.Set(ProtocolType_Titan)
		server.admin = true
	case ServerType_Gemini:
		server.Router.NotFoundHandler = gemini_notFound
		server.UploadRouter.NotFoundHandler = gemini_notFound
		server.Protocols.Set(ProtocolType_Gemini)
		server.Protocols.Set(ProtocolType_Titan)
	case ServerType_Gopher:
		server.Router.NotFoundHandler = gopher_notFound
		server.UploadRouter.NotFoundHandler = gopher_notFound
		server.Protocols.Set(ProtocolType_Gopher)
		server.Protocols.Set(ProtocolType_Titan)
	case ServerType_Nex:
		server.Router.NotFoundHandler = nex_notFound
		server.UploadRouter.NotFoundHandler = nex_notFound
		server.Protocols.Set(ProtocolType_Nex)
		server.Protocols.Set(ProtocolType_NPS)
	case ServerType_Spartan:
		server.Router.NotFoundHandler = spartan_notFound
		server.UploadRouter.NotFoundHandler = spartan_notFound
		server.Protocols.Set(ProtocolType_Spartan)
	case ServerType_Scroll:
		server.Router.NotFoundHandler = scroll_notFound
		server.UploadRouter.NotFoundHandler = scroll_notFound
		server.Protocols.Set(ProtocolType_Scroll)
		server.Protocols.Set(ProtocolType_Titan)
	case ServerType_Misfin:
		// TODO
		server.Protocols.Set(ProtocolType_Misfin_B)
		server.Protocols.Set(ProtocolType_Misfin_C)
		server.Protocols.Set(ProtocolType_Gemini)
		server.Protocols.Set(ProtocolType_Titan)
	}
	server.SIS = context
	server.ipRateLimitList = cmap.New[RateLimitListItem]()
	server.RateLimitDuration = time.Millisecond * 225

	// Add to servers list
	context.servers = append(context.servers, server)
	serverHandle := VirtualServerHandle{len(context.servers) - 1, context}
	serverHandle.GetServer().handle = serverHandle

	if len(hosts) > 0 {
		serverHandle.AddHosts(hosts...)
	}

	return serverHandle, nil
}

// Starts SIS. Must call InitSIS first.
func (context *SISContext) Start() {
	printBanner()

	TOFUMap = make(map[string]string)

	// Run each portListener
	for _, l := range context.portListeners {
		fmt.Printf("Starting Port Listener: %s:%s\n", l.BindAddress, l.BindPort)
		context.wg.Add(1)
		go l.Start(context.wg)
	}

	time.Sleep(time.Millisecond * 200)
	if context.configless {
		fmt.Printf("SIS now running.\n\n")
	} else {
		adminPreferredHost := context.AdminServer().GetPreferredHost(ProtocolType_Gemini, false, false)
		fmt.Printf("SIS now running. Visit SIS Manager admin interface: gemini://%s:%s\n\n", adminPreferredHost.Hostname, adminPreferredHost.Port)
	}

msgloop:
	for {
		var log Log
		var message string = ""
		ok := true
		received := 0
		select {
		case message, ok = <-context.msg:
			received++
		default:
		}
		select {
		case log = <-context.logChan:
			context.addToLog(log)
			received++
		default:
		}

		// Haven't received anything, block the loop until one is received
		if received == 0 {
			select {
			case message, ok = <-context.msg:
			case log = <-context.logChan:
				context.addToLog(log)
			}
		}

		// If need to shut down
		if !ok {
			fmt.Printf("Shutting down SIS.\n")
			for _, portListener := range context.portListeners {
				if portListener.running {
					close(portListener.msg)
				}
			}

			break msgloop
		}

		switch message {
		case "shutdown": // TODO: Shut down every portListener
			fmt.Printf("Shutting down SIS.\n")
			for _, portListener := range context.portListeners {
				if portListener.running {
					close(portListener.msg)
				}
			}

			break msgloop
		}
	}

	context.wg.Wait()
}

// Shuts down all of SIS
func (context *SISContext) ShutdownSIS() {
	context.msg <- "shutdown"
}

// Shuts down all Port Listeners, but does not shut down SIS
func (context *SISContext) ShutdownPortListeners() {
	// Make all existing portListeners go into maintenance mode
	for _, portListener := range context.portListeners {
		if portListener.running {
			close(portListener.msg)
		}
	}
}

// Enters maintenance mode for all portListeners
func (context *SISContext) EnterMaintenanceMode_All() {
	// Make all existing portListeners go into maintenance mode
	for _, portListener := range context.portListeners {
		portListener.msg <- "enter_maintenance"
	}
}
