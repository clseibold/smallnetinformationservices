package smallnetinformationservices

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"net/url"
	"path"
	"strings"

	"github.com/eidolon/wordwrap"
)

// Contains functions that convert between different formats, or proxies links
// TODO: Get (and return) any error received from a WriteString() call

func nexProxyLinks(reader io.Reader, proxiedToGopher bool, proxiedUnder string, writer io.Writer) {
	geminiProxyLinks(reader, proxiedToGopher, proxiedUnder, writer)
}

// This function works with nex, gemtext, and spartan
func geminiProxyLinks(reader io.Reader, proxiedToGopher bool, proxiedUnder string, writer io.Writer) {
	scanner := bufio.NewScanner(reader)

	inPreformat := false
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\r\n")
		if inPreformat {
			if strings.HasPrefix(line, "```") {
				io.WriteString(writer, line)
				inPreformat = !inPreformat
				io.WriteString(writer, "\n")
				continue
			} else {
				io.WriteString(writer, line)
				io.WriteString(writer, "\n")
				continue
			}
		}

		if strings.HasPrefix(line, "```") {
			io.WriteString(writer, line)
			io.WriteString(writer, "\n")
			inPreformat = !inPreformat
		} else if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, hasTitle := CutAny(line, " \t")
			link = strings.TrimSpace(link)

			link_without_query_and_fragment, _, _ := strings.Cut(link, "#")
			link_without_query_and_fragment, _, _ = strings.Cut(link_without_query_and_fragment, "?")
			URL, _ := url.Parse(link)

			if strings.HasPrefix(link_without_query_and_fragment, "/") && proxiedUnder != "" { // If Absolute link, then add the proxyPath
				itemtype := "0"
				// TODO: For now, just assume all paths ending in a slash are directory listings.
				if proxiedToGopher {
					if strings.HasSuffix(link_without_query_and_fragment, "/") {
						itemtype = "1"
					} else {
						// Itemtype detection based on extension
						ext := path.Ext(link_without_query_and_fragment)
						if ext == ".gemini" || ext == ".gmi" {
							itemtype = "0"
						} else if ext == ".gophermap" {
							itemtype = "1"
						} else if ext == ".nex" || path.Base(link) == "index" {
							itemtype = "1"
						} else if ext == ".txt" {
							itemtype = "0"
						} else if ext == ".mp3" || ext == ".flac" || ext == ".mid" || ext == ".midi" || ext == ".mpa" || ext == ".ogg" || ext == ".wav" || ext == ".wma" || ext == ".aif" {
							itemtype = "9" // Or s or <
						} else if ext == ".exe" {
							itemtype = "9"
						} else if ext == ".srv3" {
							itemtype = "0"
						} else if ext == ".mp4" || ext == ".mkv" || ext == ".webm" || ext == ".flv" || ext == ".avi" || ext == ".m4p" || ext == ".m4v" || ext == ".svi" || ext == ".3gp" || ext == ".3g2" || ext == ".f4v" || ext == ".f4p" || ext == ".f4a" || ext == ".f4b" {
							itemtype = "9" // Or ;
						} else {
							itemtype = "0"
						}
					}
				}

				io.WriteString(writer, "=> ")
				if proxiedToGopher {
					io.WriteString(writer, path.Join("/"+url.PathEscape(itemtype), proxiedUnder, URL.EscapedPath()))
				} else {
					io.WriteString(writer, path.Join(proxiedUnder, URL.EscapedPath()))
				}
				if strings.HasSuffix(link_without_query_and_fragment, "/") { // NOTE: Since path.Join removes trailing slashes, add them back
					io.WriteString(writer, "/")
				}
				if URL.RawQuery != "" {
					io.WriteString(writer, "?"+URL.RawQuery) // Add the query back in
				}
				if URL.EscapedFragment() != "" {
					io.WriteString(writer, "#"+URL.EscapedFragment()) // Add the fragment back in
				}
				if hasTitle {
					io.WriteString(writer, " ")
					io.WriteString(writer, title)
				}
				io.WriteString(writer, "\n")
			} else {
				io.WriteString(writer, "=> ")
				io.WriteString(writer, line)
				io.WriteString(writer, "\n")
			}
		} else {
			io.WriteString(writer, line)
			io.WriteString(writer, "\n")
		}
	}
}

// Will just convert all links to work with proxying.
func geminiToSpartan(reader io.Reader, proxiedUnder string, writer io.Writer) {
	geminiProxyLinks(reader, false, proxiedUnder, writer)
}

// Takes proxiedUnder in order to convert any absolute links when a proxy is used
func geminiToNex(reader io.Reader, width int, proxiedUnder string, writer io.Writer) {
	wordWrapper := wordwrap.Wrapper(width, false)

	scanner := bufio.NewScanner(reader)
	inPreformat := false
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\r\n")
		if inPreformat {
			if strings.HasPrefix(line, "```") {
				io.WriteString(writer, line)
				inPreformat = !inPreformat
				io.WriteString(writer, "\n")
				continue
			} else {
				io.WriteString(writer, line)
				io.WriteString(writer, "\n")
				continue
			}
		}
		if strings.HasPrefix(line, "```") {
			io.WriteString(writer, line)
			inPreformat = !inPreformat
		} else if strings.HasPrefix(line, "###") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "###"))
			io.WriteString(writer, line)
		} else if strings.HasPrefix(line, "##") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "##"))
			io.WriteString(writer, line)
			io.WriteString(writer, "\n")
			for i := 0; i < len(line); i++ {
				io.WriteString(writer, "-")
			}
		} else if strings.HasPrefix(line, "#") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "#"))
			space := width - len(line)
			leftSpace := int(math.Floor(float64(space) / 2))
			rightSpace := int(math.Ceil(float64(space) / 2))

			for i := 0; i < leftSpace; i++ {
				io.WriteString(writer, " ")
			}
			io.WriteString(writer, line)
			for i := 0; i < rightSpace; i++ {
				io.WriteString(writer, " ")
			}
		} else if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, hasTitle := CutAny(line, " \t")
			link = strings.TrimSpace(link)

			link_without_query_and_fragment, _, _ := strings.Cut(link, "#")
			link_without_query_and_fragment, _, _ = strings.Cut(link_without_query_and_fragment, "?")
			URL, _ := url.Parse(link)

			if strings.HasPrefix(link, "/") && proxiedUnder != "" { // If Absolute link, then add the proxyPath
				io.WriteString(writer, "=> ")
				io.WriteString(writer, path.Join(proxiedUnder, URL.EscapedPath()))
				if strings.HasSuffix(link_without_query_and_fragment, "/") { // NOTE: Since path.Join removes trailing slashes, add them back
					io.WriteString(writer, "/")
				}
				if URL.RawQuery != "" {
					io.WriteString(writer, "?"+URL.RawQuery) // Add the query back in
				}
				if URL.EscapedFragment() != "" {
					io.WriteString(writer, "#"+URL.EscapedFragment()) // Add the fragment back in
				}
				if hasTitle {
					io.WriteString(writer, " ")
					io.WriteString(writer, title)
				}
			} else {
				io.WriteString(writer, "=> ")
				io.WriteString(writer, line)
			}
		} else if strings.HasPrefix(line, ">") {
			newtext := wordWrapper(strings.TrimPrefix(line, ">"))
			newtext_lines := strings.FieldsFunc(newtext, func(r rune) bool { return r == '\n' || r == '\r' })
			for _, new_line := range newtext_lines {
				io.WriteString(writer, "> "+strings.TrimRight(new_line, "\r\n"))
			}
		} else {
			io.WriteString(writer, wordWrapper(line))
		}

		// TODO: Link lines - folder vs. file

		io.WriteString(writer, "\n")
	}
}

// TODO: Handle proxiedUnder stuff also
func markdownToGemini(text string) string {
	return text
}

// TODO: Handle proxiedUnder stuff also
func markdownToNex(text string) string {
	return text
}

func geminiToGophermap(reader io.Reader, width int, proxiedUnder string, currentPath string, proxyRoute string, hostname string, port string, writer io.Writer) {
	wordWrapper := wordwrap.Wrapper(width, false)

	scanner := bufio.NewScanner(reader)
	inPreformat := false
	preceedingEmptyLines := 0
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\r\n")
		if inPreformat {
			if strings.HasPrefix(line, "```") {
				inPreformat = !inPreformat
				preceedingEmptyLines = 0
				io.WriteString(writer, gophermapLine("i", strings.TrimSpace(line), "/", hostname, port))
				continue
			} else {
				if strings.TrimSpace(line) == "" {
					preceedingEmptyLines += 1
				} else {
					preceedingEmptyLines = 0
				}
				io.WriteString(writer, gophermapLine("i", strings.TrimRight(line, "\r\n"), "/", hostname, port))
				continue
			}
		}

		if strings.HasPrefix(line, "```") {
			//builder.WriteString(line)
			io.WriteString(writer, gophermapLine("i", strings.TrimSpace(line), "/", hostname, port))
			inPreformat = !inPreformat
		} else if strings.HasPrefix(line, "###") {
			if preceedingEmptyLines < 1 {
				for i := 0; i < 2-preceedingEmptyLines; i++ {
					io.WriteString(writer, gophermapLine("i", "", "/", hostname, port))
				}
			}

			line = strings.TrimSpace(strings.TrimPrefix(line, "###"))
			io.WriteString(writer, gophermapLine("i", line, "/", hostname, port))
		} else if strings.HasPrefix(line, "##") {
			if preceedingEmptyLines < 2 {
				for i := 0; i < 2-preceedingEmptyLines; i++ {
					io.WriteString(writer, gophermapLine("i", "", "/", hostname, port))
				}
			}

			line = strings.TrimSpace(strings.TrimPrefix(line, "##"))
			io.WriteString(writer, gophermapLine("i", line, "/", hostname, port))

			io.WriteString(writer, "i") // itemtype
			for i := 0; i < len(line); i++ {
				io.WriteString(writer, "-")
			}
			io.WriteString(writer, "\t/\t") // selector
			io.WriteString(writer, hostname)
			io.WriteString(writer, "\t")
			io.WriteString(writer, port)
			io.WriteString(writer, "\r\n")
		} else if strings.HasPrefix(line, "#") {
			if preceedingEmptyLines < 2 {
				for i := 0; i < 2-preceedingEmptyLines; i++ {
					io.WriteString(writer, gophermapLine("i", "", "/", hostname, port))
				}
			}

			line = strings.TrimSpace(strings.TrimPrefix(line, "#"))
			space := width - len(line)
			leftSpace := int(math.Floor(float64(space) / 2))
			rightSpace := int(math.Ceil(float64(space) / 2))

			io.WriteString(writer, "i") // itemtype
			for i := 0; i < leftSpace; i++ {
				io.WriteString(writer, " ")
			}
			io.WriteString(writer, line)
			for i := 0; i < rightSpace; i++ {
				io.WriteString(writer, " ")
			}
			io.WriteString(writer, "\t/\t") // selector
			io.WriteString(writer, hostname)
			io.WriteString(writer, "\t")
			io.WriteString(writer, port)
			io.WriteString(writer, "\r\n")
		} else if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, _ := CutAny(line, " \t")

			link_without_query_and_fragment, _, _ := strings.Cut(link, "#")
			link_without_query_and_fragment, _, _ = strings.Cut(link_without_query_and_fragment, "?")
			URL, parseErr := url.Parse(link)
			if parseErr != nil {
				preceedingEmptyLines = 0
				continue
			}

			itemtype := "0"
			// TODO: For now, just assume all paths ending in a slash are directory listings.
			if strings.HasSuffix(link_without_query_and_fragment, "/") {
				itemtype = "1"
			} else {
				// Itemtype detection based on extension
				ext := path.Ext(link)
				if ext == ".gemini" || ext == ".gmi" {
					itemtype = "0"
				} else if ext == ".gophermap" {
					itemtype = "1"
				} else if ext == ".nex" || path.Base(link) == "index" {
					itemtype = "1"
				} else if ext == ".txt" {
					itemtype = "0"
				} else if ext == ".mp3" || ext == ".flac" || ext == ".mid" || ext == ".midi" || ext == ".mpa" || ext == ".ogg" || ext == ".wav" || ext == ".wma" || ext == ".aif" {
					itemtype = "9" // OR s or <
				} else if ext == ".exe" {
					itemtype = "9"
				} else if ext == ".srv3" {
					itemtype = "0"
				} else if ext == ".mp4" || ext == ".mkv" || ext == ".webm" || ext == ".flv" || ext == ".avi" || ext == ".m4p" || ext == ".m4v" || ext == ".svi" || ext == ".3gp" || ext == ".3g2" || ext == ".f4v" || ext == ".f4p" || ext == ".f4a" || ext == ".f4b" {
					itemtype = "9" // Or ;
				} else {
					itemtype = "0"
				}
			}

			if URL.Scheme != "" {
				itemtype = "h"
				if title == "" {
					title = URL.String()
				}
				io.WriteString(writer, gophermapLine(itemtype, title, "URL:"+URL.String(), hostname, port))
			} else if strings.HasPrefix(link, "/") { // Absolute link
				/*beforeGlob, _, _ := strings.Cut(proxyRoute, "*")
				newLink := strings.Replace(url.EscapedPath(), beforeGlob, proxiedUnder, 1)*/
				newLink := path.Join(proxiedUnder, URL.EscapedPath())
				if strings.HasSuffix(link_without_query_and_fragment, "/") { // NOTE: Since path.Join removes trailing slashes, add them back
					newLink += "/"
				}
				if title == "" {
					title = newLink
				}
				if URL.RawQuery != "" {
					newLink += "?" + URL.RawQuery // Add the query back in
				}
				if URL.EscapedFragment() != "" {
					newLink += "#" + URL.EscapedFragment() // Add the fragment back in
				}
				io.WriteString(writer, gophermapLine(itemtype, title, newLink, hostname, port))
			} else { // Relative link
				newLink := path.Join(proxiedUnder, currentPath, URL.EscapedPath())
				if strings.HasSuffix(link_without_query_and_fragment, "/") { // Add back in the trailing slash
					newLink += "/"
				}
				if title == "" {
					title = newLink
				}
				if URL.RawQuery != "" {
					newLink += "?" + URL.RawQuery // Add the query back in
				}
				if URL.EscapedFragment() != "" {
					newLink += "#" + URL.EscapedFragment() // Add the fragment back in
				}
				io.WriteString(writer, gophermapLine(itemtype, title, newLink, hostname, port))
			}
			// TODO: How to get itemtype?
			// TODO: Links that end in slash should be assumed to be gemtext directory listing.
		} else if strings.HasPrefix(line, ">") {
			newtext := wordWrapper(strings.TrimPrefix(line, ">"))
			newtext_lines := strings.FieldsFunc(newtext, func(r rune) bool { return r == '\n' || r == '\r' })
			for _, new_line := range newtext_lines {
				io.WriteString(writer, gophermapLine("i", strings.TrimRight("> "+new_line, "\r\n"), "/", hostname, port))
			}
		} else {
			lines := wordWrapper(line)
			for _, line := range strings.Split(lines, "\n") {
				io.WriteString(writer, gophermapLine("i", strings.TrimRight(line, "\r"), "/", hostname, port))
			}
		}

		if strings.TrimSpace(line) == "" {
			preceedingEmptyLines += 1
		} else {
			preceedingEmptyLines = 0
		}

		// TODO: Link lines - folder vs. file
	}
}

// TODO: Handle the proxying stuff
func nexToGophermap(reader io.Reader, currentPath string, hostname string, port string, width int, writer io.Writer) {
	wordWrapper := wordwrap.Wrapper(width, false)

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\r\n")
		if strings.HasPrefix(line, "=>") {
			line = strings.TrimSpace(strings.TrimPrefix(line, "=>"))
			link, title, _ := CutAny(line, " \t")

			URL, parseErr := url.Parse(link)
			if parseErr != nil {
				continue
			}

			itemtype := "0"
			// TODO: For now, just assume all paths ending in a slash are directory listings.
			if strings.HasSuffix(link, "/") {
				itemtype = "1"
			}

			if URL.Scheme != "" {
				itemtype = "h"
				io.WriteString(writer, gophermapLine(itemtype, title, URL.EscapedPath(), "URL:"+URL.Scheme+"://"+URL.Hostname(), URL.Port()))
			} else {
				fmt.Printf("Current Path: %s\n", currentPath)
				io.WriteString(writer, gophermapLine(itemtype, title, path.Join(currentPath, URL.EscapedPath()), hostname, port))
			}
			// TODO: How to get itemtype?
			// TODO: Links that end in slash should be assumed to be gemtext directory listing.
		} else if strings.HasPrefix(line, ">") {
			newtext := wordWrapper(strings.TrimPrefix(line, ">"))
			newtext_lines := strings.FieldsFunc(newtext, func(r rune) bool { return r == '\n' || r == '\r' })
			for _, new_line := range newtext_lines {
				io.WriteString(writer, gophermapLine("i", strings.TrimRight("> "+new_line, "\r\n"), "/", hostname, port))
			}
		} else {
			lines := wordWrapper(line)
			for _, line := range strings.Split(lines, "\n") {
				io.WriteString(writer, gophermapLine("i", line, "/", hostname, port))
			}
		}
	}
}

// Detect paragraphs and merge back onto one line
func gophermapToGemini(text string, workingDirectory string) string { // TODO
	var builder strings.Builder
	scanner := bufio.NewScanner(strings.NewReader(text))
	for scanner.Scan() {
		line := strings.TrimRight(scanner.Text(), "\r\n")
		parts := strings.Split(line, "\t")

		itemType := parts[0][0]
		text := parts[0][1:]

		selector := "" // TODO: Can have "URL:" prefix for full URLs
		hostname := ""
		port := ""

		if len(parts) > 1 {
			selector = parts[1]
		}
		if len(parts) > 2 {
			hostname = parts[2]
		}
		if len(parts) > 3 {
			port = parts[3]
		}

		if itemType == '!' {
			// Gophernicus menu titles
		} else if itemType == 'i' {
			// Information line
			builder.WriteString(text)
			builder.WriteString("\n")
		} else if itemType == '0' || itemType == '5' || itemType == '9' || itemType == 'g' || itemType == 'h' || itemType == 'I' || itemType == 'd' || itemType == 's' || itemType == ';' || itemType == 'c' || itemType == 'M' {
			// Document (Text, Archive, Binary, etc.)
			builder.WriteString("=> ")
			if hostname != "" || port != "" {
				builder.WriteString(hostname)
				if port != "" {
					builder.WriteRune(':')
					builder.WriteString(port)
				}
			}
			builder.WriteString(selector)
			builder.WriteString(" ")
			builder.WriteString(text)
			builder.WriteString("\n")
		} else if itemType == '7' {
			// TODO: Search Query
		} else if itemType == '8' {
			// Telnet Session
		} else if itemType == '1' {
			// Directory
			builder.WriteString("=> ")
			if hostname != "" || port != "" {
				builder.WriteString(hostname)
				if port != "" {
					builder.WriteRune(':')
					builder.WriteString(port)
				}
			}
			builder.WriteString(selector)
			if !strings.HasSuffix(selector, "/") {
				builder.WriteRune('/')
			}
			builder.WriteString(" ")
			builder.WriteString(text)
			builder.WriteString("\n")
		} else if itemType == '3' {
			// TODO: Return Error message
		} else if itemType == '#' {
			// Comment, ignore.
		} else if itemType == '*' {
			// TODO: Listing of current directory
		} else if itemType == '=' {
			// Include other gophermap or execute script/exe and include output as gophermap
			// mapfile = text
		} else if itemType == '~' {
			// Pubnix Users List
		} else if itemType == '%' {
			// Virtual Hosts list
		} else if itemType == '-' {
			// Exclude file from '*' directory listing
		} else if itemType == ':' {
			// ':ext=type' will change filetype of files with that extension for the current directory. Affects the '*' directory listing
		} else if itemType == '.' {
			// Stop processing
			break
		}
	}

	return builder.String()
}

func gophermapToNex() {

}

func gophermapLine(itemtype string, name string, selector string, hostname string, port string) string {
	return fmt.Sprintf("%s%s\t%s\t%s\t%s\r\n", itemtype, name, selector, hostname, port)
}

func geminiLinkLine(builder *strings.Builder, link string, text string) {
	builder.WriteString("=> ")
	builder.WriteString(link)
	builder.WriteRune(' ')
	builder.WriteString(text)
	builder.WriteRune('\n')
}
