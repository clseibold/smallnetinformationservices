package smallnetinformationservices

import (
	"path"
	"path/filepath"
	"strings"

	"github.com/gabriel-vasile/mimetype"
)

// Provides methods to add routes to a VirtualServer or RouteGroup
type ServeMux interface {
	AddRoute(p string, handler RequestHandler)
	AddUploadRoute(p string, handler RequestHandler)
	AddDirectory(p string, directoryPath string)
	AddCGIRoute(p string, filePath string)
	AddSCGIRoute(p string, address string)
	AddProxyRoute(p string, proxyRoute string, gopherItemType rune)
	AddFile(p string, filePath string)

	// Group creates a prefix to be automatically prepended to all routes created on the RouteGroup.
	Group(p string) RouteGroup

	// Prefixes the absolute link passed in, if applicable. Does nothing with relative links.
	// ServerHandle's have no profix. RoutePrefixes will prepend their prefix.
	// PrefixLink(link string) string
}

// Attach a prefix to routes.
type RouteGroup struct {
	handle VirtualServerHandle
	p      string
}

func (prefix RouteGroup) AddRoute(p string, handler RequestHandler) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()

	// NOTE: Assume that all routes ending in "/" are intended to be directories/gophermaps for now.
	if strings.HasSuffix(p, "/") {
		// Present as directory listing
		s.Router.addRoute(p, handler, "", "", prefix.p, '1') // TODO
	} else {
		// TODO: Present as text file?
		s.Router.addRoute(p, handler, "", "", prefix.p, '0') // TODO
	}
}
func (prefix RouteGroup) AddUploadRoute(p string, handler RequestHandler) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	switch s.Type {
	case ServerType_Gopher, ServerType_Nex:
		panic("Nex servers don't support upload routes.")
	}
	s.UploadRouter.addRoute(p, handler, "", "", prefix.p, '0')
}
func (prefix RouteGroup) AddDirectory(p string, directoryPath string) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	// Security: Don't expose system's root directory.
	if directoryPath == "/" || directoryPath == "C:" || directoryPath == "C:/" || directoryPath == "C:\\" {
		panic("System's root directory is exposed.")
	}
	if !filepath.IsAbs(directoryPath) {
		directoryPath = filepath.Join(s.Directory, directoryPath)
	}

	// TODO: Handle {route}/index.gmi route

	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleDirectory, directoryPath, "", prefix.p, '1')
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleNexDirectory, directoryPath, "", prefix.p, '1')
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleDirectory, directoryPath, "", prefix.p, '1')
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleDirectory, directoryPath, "", prefix.p, '1')
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleDirectory, directoryPath, "", prefix.p, '1')
	}
}
func (prefix RouteGroup) AddCGIRoute(p string, filePath string) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	path := strings.TrimPrefix(filePath, "cgi:")
	if !filepath.IsAbs(path) {
		filePath = "cgi:" + filepath.Join(s.Directory, path)
	}

	fileItemType := '1'
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleCGI, filePath, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, gemini_handleCGI, filePath, "", prefix.p, fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleCGI, filePath, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, nex_handleCGI, filePath, "", prefix.p, fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleCGI, filePath, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, gopher_handleCGI, filePath, "", prefix.p, fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleCGI, filePath, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, spartan_handleCGI, filePath, "", prefix.p, fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleCGI, filePath, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, scroll_handleCGI, filePath, "", prefix.p, fileItemType)
	}
}
func (prefix RouteGroup) AddSCGIRoute(p string, address string) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	fileItemType := '1'
	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleSCGI, address, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, gemini_handleSCGI, address, "", prefix.p, fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleSCGI, address, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, nex_handleSCGI, address, "", prefix.p, fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleSCGI, address, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, gopher_handleSCGI, address, "", prefix.p, fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleSCGI, address, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, spartan_handleSCGI, address, "", prefix.p, fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleSCGI, address, "", prefix.p, fileItemType)
		s.UploadRouter.addRoute(p, scroll_handleSCGI, address, "", prefix.p, fileItemType)
	}
}
func (prefix RouteGroup) AddProxyRoute(p string, proxyRoute string, gopherItemType rune) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	s.Router.addRoute(p, proxyHandler, "", proxyRoute, p, gopherItemType) // TODO
}
func (prefix RouteGroup) AddFile(p string, filePath string) {
	addSlashSuffix := strings.HasSuffix(p, "/")
	p = path.Join(prefix.p, p)
	if addSlashSuffix {
		p = p + "/"
	}

	s := prefix.handle.GetServer()
	if !filepath.IsAbs(filePath) {
		filePath = filepath.Join(s.Directory, filePath)
	}

	// Detect file's itemtype
	extension := filepath.Ext(filePath)
	mt, err := mimetype.DetectFile(filePath)
	if err != nil {
		panic(err)
	}
	mt_string := mt.String()
	mt_parent_string := mt.Parent().String()
	if extension == ".gemini" || extension == ".gmi" {
		mt_string = "text/gemini"
		mt_parent_string = ""
	} else if extension == ".gophermap" {
		mt_string = "application/gopher-menu"
		mt_parent_string = ""
	} else if extension == ".nex" || (extension == "" && filepath.Base(filePath) == "index") {
		mt_string = "text/nex"
		mt_parent_string = ""
	} else if PrivacySensitiveFilename(filePath) || PrivacySensitiveMimetype(mt_string) {
		// Don't add privacy-sensitive files or certificate files to routing
		return
	}

	fileItemType := MimetypeToGopherItemtype(mt_string, mt_parent_string)
	if strings.HasSuffix(p, "/") {
		// Present file as a directory listing
		fileItemType = '1'
	}

	switch s.Type {
	case ServerType_Admin, ServerType_Gemini:
		s.Router.addRoute(p, gemini_handleFile, filePath, "", prefix.p, fileItemType)
	case ServerType_Nex:
		s.Router.addRoute(p, nex_handleNexFile, filePath, "", prefix.p, fileItemType)
	case ServerType_Gopher:
		s.Router.addRoute(p, gopher_handleFile, filePath, "", prefix.p, fileItemType)
	case ServerType_Spartan:
		s.Router.addRoute(p, spartan_handleFile, filePath, "", prefix.p, fileItemType)
	case ServerType_Scroll:
		s.Router.addRoute(p, scroll_handleFile, filePath, "", prefix.p, fileItemType)
	}
}

// Group creates a prefix to be automatically prepended to all routes created on the RouteGroup.
// It will also ensure all absolute links in the responses of the attached routes will be
// prepended with the group prefix.
func (prefix RouteGroup) Group(p string) RouteGroup {
	return RouteGroup{handle: prefix.handle, p: path.Join(prefix.p, p)}
}
