package smallnetinformationservices

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"mime"
	"net"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/dhowden/tag"
	"github.com/djherbis/times"
	"github.com/gabriel-vasile/mimetype"
	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
	"golang.org/x/text/language"
)

type ScrollMetadata struct {
	Author         string
	PublishDate    time.Time // Should be in UTC
	UpdateDate     time.Time // Should be in UTC
	Language       string    // Should use BCP47
	Abstract       string
	Classification ScrollResponseUDC
}

type ScrollResponseUDC uint8

const (
	ScrollResponseUDC_Class4 ScrollResponseUDC = iota
	ScrollResponseUDC_Class0                   // Knowledge, General Science, Docs, RFCs, Software, Data
	ScrollResponseUDC_Class1
	ScrollResponseUDC_Class2
	ScrollResponseUDC_Class3
	ScrollResponseUDC_Class5
	ScrollResponseUDC_Class6
	ScrollResponseUDC_Class7
	ScrollResponseUDC_Class8
	ScrollResponseUDC_Class9

	ScrollResponseUDC_GeneralKnowledge   = ScrollResponseUDC_Class0
	ScrollResponseUDC_Docs               = ScrollResponseUDC_Class0
	ScrollResponseUDC_Data               = ScrollResponseUDC_Class0
	ScrollResponseUDC_GeneralScience     = ScrollResponseUDC_Class0
	ScrollResponseUDC_Reference          = ScrollResponseUDC_Class0
	ScrollResponseUDC_CompSci            = ScrollResponseUDC_Class0
	ScrollResponseUDC_News               = ScrollResponseUDC_Class0
	ScrollResponseUDC_ComputerTechnology = ScrollResponseUDC_Class0

	ScrollResponseUDC_Philosophy = ScrollResponseUDC_Class1
	ScrollResponseUDC_Psychology = ScrollResponseUDC_Class1

	ScrollResponseUDC_Religion  = ScrollResponseUDC_Class2
	ScrollResponseUDC_Theology  = ScrollResponseUDC_Class2
	ScrollResponseUDC_Scripture = ScrollResponseUDC_Class2

	ScrollResponseUDC_SocialScience = ScrollResponseUDC_Class3
	ScrollResponseUDC_Politics      = ScrollResponseUDC_Class3

	ScrollResponseUDC_Unclassed  = ScrollResponseUDC_Class4
	ScrollResponseUDC_Aggregator = ScrollResponseUDC_Class4
	ScrollResponseUDC_Directory  = ScrollResponseUDC_Class4
	ScrollResponseUDC_Index_Menu = ScrollResponseUDC_Class4
	ScrollResponseUDC_Misc       = ScrollResponseUDC_Class4

	ScrollResponseUDC_Mathematics    = ScrollResponseUDC_Class5
	ScrollResponseUDC_NaturalScience = ScrollResponseUDC_Class5

	ScrollResponseUDC_AppliedScience    = ScrollResponseUDC_Class6
	ScrollResponseUDC_Medicine          = ScrollResponseUDC_Class6
	ScrollResponseUDC_Health            = ScrollResponseUDC_Class6
	ScrollResponseUDC_GeneralTechnology = ScrollResponseUDC_Class6
	ScrollResponseUDC_Engineering       = ScrollResponseUDC_Class6
	ScrollResponseUDC_Business          = ScrollResponseUDC_Class6
	ScrollResponseUDC_Accountancy       = ScrollResponseUDC_Class6

	ScrollResponseUDC_Art                = ScrollResponseUDC_Class7
	ScrollResponseUDC_Fashion            = ScrollResponseUDC_Class7
	ScrollResponseUDC_Entertainment      = ScrollResponseUDC_Class7
	ScrollResponseUDC_Music              = ScrollResponseUDC_Class7
	ScrollResponseUDC_GamingVideos       = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalMovies    = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalAnimation = ScrollResponseUDC_Class7
	ScrollResponseUDC_FictionalTVShows   = ScrollResponseUDC_Class7
	ScrollResponseUDC_Sport              = ScrollResponseUDC_Class7
	ScrollResponseUDC_Fitness            = ScrollResponseUDC_Class7
	ScrollResponseUDC_Recreation         = ScrollResponseUDC_Class7

	ScrollResponseUDC_Linguistics       = ScrollResponseUDC_Class8
	ScrollResponseUDC_Literature        = ScrollResponseUDC_Class8
	ScrollResponseUDC_LiteraryCriticism = ScrollResponseUDC_Class8
	ScrollResponseUDC_Memoir            = ScrollResponseUDC_Class8
	ScrollResponseUDC_PersonalLog       = ScrollResponseUDC_Class8
	ScrollResponseUDC_BookReview        = ScrollResponseUDC_Class8
	ScrollResponseUDC_MovieReview       = ScrollResponseUDC_Class8
	ScrollResponseUDC_VideoReview       = ScrollResponseUDC_Class8
	ScrollResponseUDC_MusicReview       = ScrollResponseUDC_Class8

	ScrollResponseUDC_History   = ScrollResponseUDC_Class9
	ScrollResponseUDC_Geography = ScrollResponseUDC_Class9
	ScrollResponseUDC_Biography = ScrollResponseUDC_Class9
)

func UDCToSuccessResponse(value ScrollResponseUDC) int {
	switch value {
	case ScrollResponseUDC_Class4:
		return 24
	case ScrollResponseUDC_Class0:
		return 20
	case ScrollResponseUDC_Class1:
		return 21
	case ScrollResponseUDC_Class2:
		return 22
	case ScrollResponseUDC_Class3:
		return 23
	case ScrollResponseUDC_Class5:
		return 25
	case ScrollResponseUDC_Class6:
		return 26
	case ScrollResponseUDC_Class7:
		return 27
	case ScrollResponseUDC_Class8:
		return 28
	case ScrollResponseUDC_Class9:
		return 29
	default:
		return 24
	}
}

func ClassificationStringToUDC(value string) ScrollResponseUDC {
	switch strings.ToLower(value) {
	case "knowledge", "docs", "documentation", "data", "general science", "general knowledge", "reference", "computer science", "news", "software repository", "software repo", "computer technology", "howto", "tutorial", "devlog":
		return ScrollResponseUDC_Class0
	case "philosophy", "psychology":
		return ScrollResponseUDC_Class1
	case "religion", "theology", "scripture":
		return ScrollResponseUDC_Class2
	case "social science", "social sciences", "law", "politics", "gender studies", "sociology", "LGBTQ+", "military", "military affairs", "education":
		return ScrollResponseUDC_Class3
	case "mathematics", "math", "maths", "natural science":
		return ScrollResponseUDC_Class5
	case "applied science", "applied sciences", "technology", "tech", "medicine", "health", "engineering", "business", "accountancy", "accounting":
		return ScrollResponseUDC_Class6
	case "art", "arts", "entertainment", "music", "gaming", "gaming video", "gaming videos", "sport", "sports", "fictional movies", "fictional movie", "fictional animation", "fictional animations", "fictional anime", "fictional animes", "fashion", "beauty", "culinary art", "culinary arts", "recreation", "fitness", "drama", "performance", "performance art", "performance arts":
		return ScrollResponseUDC_Class7
	case "linguistics", "language", "langauges", "literature", "literary criticism", "biblical criticism", "memoir", "personal log", "personal blog", "book review", "movie review", "video review", "music review":
		return ScrollResponseUDC_Class8
	case "history", "geography", "biography", "autobiography":
		return ScrollResponseUDC_Class9
	case "unclassed", "unclassed software", "general software", "general aggregator", "general directory", "general index", "menu":
		return ScrollResponseUDC_Class4
	default:
		return ScrollResponseUDC_Class4
	}
}

// NOTE: If more are added, RequestFlag needs to be expanded to uin16 or above
type RequestFlag byte

const (
	RequestFlag_Upload                  RequestFlag = iota // Set if request supports upload.
	RequestFlag_HeaderSent                                 // Set if the response header has already been sent (for protocols that use response headers).
	RequestFlag_ConvertMode                                // Set to convert from given format to default format of protocol.
	RequestFlag_Proxied                                    // Set if request is proxied, or if an SCGI application server.
	RequestFlag_AllowServeCert                             // Set to allow serving cert/key files. Will be set to the route's security setting.
	RequestFlag_AllowServeDotFiles                         // Set to allow serving dot (and hidden) files. Will be set to the route's security setting.
	RequestFlag_ScrollMetadataRequested                    // Set if request is asking for Metadata from the Scroll protocol.
)

type Request struct {
	// -- Request Data --

	Type  ProtocolType // The expected protocol. When proxied, this will be the protocol being proxied to. Use proxyFromType for the protocol being proxied from.
	flags bitset.BitSet[RequestFlag, RequestFlag]

	Host        ServerHost // The Host information of the current request
	fromRoute   string     // What route the request came from
	prefix      string     // The prefix of the path for routes that are in a RouteGroup. This should *not* be part of requestPath.
	requestPath string     // The requested path (usually in raw/escaped form), excluding proxied under. Equivalent to fromRoute with params inserted in.

	requestString string            // Path for Nex, URL for Gopher, Gemini, and Misfin. Always excludes the query
	_rawQuery     string            // The raw query (always escaped, without '?', or '\t' for gopher). Spartan does not use this, but uses the Data field instead.
	_queryLimit   int64             // Used by Spartan servers to limit data for queries. If 0, use the default of 1024.
	GlobString    string            // Set to the path components of the glob part of the route
	readwriter    io.ReadWriter     // Reads from and Writes to the connection (net or tls)
	_tcpConn      *net.TCPConn      // TCP Connection. NOTE: Used internally *only* for TCP-related functions, like setting the KeepAliveConfig
	params        map[string]string // Stores route params and their values

	// -- Upload Data --

	DataSize int64  // Expected Size for upload (-1 for any size until connection close)
	_data    []byte // Used to cache raw uploaded data. For NPS, the data is already read before calling the handler. If proxying from Spartan servers, the data is set to a query string, if used.
	DataMime string // Used for Titan and other protocols that support upload with mimetype. For Spartan, it is usually empty or "text/plain" if proxying to Spartan servers from servers that use queries.

	// -- More Request Data --

	UserCert   ClientCertificate // Used for protocols that support TLS Client Certs (Misfin and Gemini)
	IP         string
	RemotePort string // Remote Port of client host

	// -- Response Data --

	servePath string // Used internally to serve static files/directories. Also used for host:port of SCGI servers.
	// prefix    string // Used internally for prefixed routes (part of a RouteGroup). All absolute links will be auto-converted to include this.

	// -- Proxying Data --

	proxyRoute       string // Used to proxy routes from one server to another, or within the same server.
	ProxiedUnder     string // The path being proxied under. This is necessary to proxy gemini and nex over to gopher, because gopher requires links be absolute to originating server. It is also used for SCGI application servers, where it is set to the SCRIPT_NAME environment variable.
	proxiedFromRoute string // The route being proxied from
	//origRequest  string // Gives you the original request string. Used for proxying.

	// -- More Response Data --

	// TODO: Add childrenRoutes and gopherItemType fields

	gopherItemType rune // Set to whatever the RouteNode was set to

	Server VirtualServerHandle

	// -- Scroll Protocol --

	ScrollRequestedLanguages []string
	scrollMetadata           ScrollMetadata

	// -- Misfin -- // TODO
}

var protocolSchemes = map[ProtocolType]string{
	ProtocolType_Gemini:     "gemini://",
	ProtocolType_Nex:        "nex://",
	ProtocolType_NPS:        "nps://", // TODO
	ProtocolType_Gopher:     "gopher://",
	ProtocolType_Gopher_SSL: "gophers://",
	ProtocolType_Misfin_A:   "misfina://",
	ProtocolType_Misfin_B:   "misfin://",
	ProtocolType_Misfin_C:   "misfin://",
	ProtocolType_Titan:      "titan://",
	ProtocolType_Spartan:    "spartan://",
	ProtocolType_Scroll:     "scroll://",
	ProtocolType_Guppy:      "guppy://",
	ProtocolType_Scorpion:   "scorpion://",
}

// Upload returns true if the request supports reading uploaded data
func (r *Request) Upload() bool {
	return r.flags.Test(RequestFlag_Upload)
}

// Proxied returns true if request is being proxied, or if the request is going through an SCGI application server.
func (r *Request) Proxied() bool {
	return r.flags.Test(RequestFlag_Proxied)
}

func (r *Request) ScrollMetadataRequested() bool {
	return r.flags.Test(RequestFlag_ScrollMetadataRequested)
}

func (r *Request) Scheme() string {
	return protocolSchemes[r.Type]
}

// Used for all protocols that send a mimetype in the response
func (r *Request) SetLanguage(lang string) {
	r.scrollMetadata.Language = lang
}

// Forces the response to have no lang parameter
func (r *Request) SetNoLanguage() {
	r.scrollMetadata.Language = "none"
}

func (r *Request) SetScrollMetadataResponse(metadata ScrollMetadata) {
	r.scrollMetadata = metadata
}
func (r *Request) ClearScrollMetadataResponse() {
	r.scrollMetadata = ScrollMetadata{}
}
func (r *Request) SetClassification(classification ScrollResponseUDC) {
	r.scrollMetadata.Classification = classification
}
func (r *Request) ClearClassification(classification ScrollResponseUDC) {
	r.scrollMetadata.Classification = ScrollResponseUDC_Unclassed
}

// Get Titan or Spartan raw data.
func (r *Request) GetUploadData() ([]byte, error) { // TODO: Return error when there's no data received after a timeout.
	if !r.flags.Test(RequestFlag_Upload) {
		return []byte{}, fmt.Errorf("current page does not allow upload")
	}

	return r._getUploadData()
}

func (r *Request) SetKeepAliveConfig(config net.KeepAliveConfig) error {
	if r._tcpConn == nil {
		return nil
	}
	return r._tcpConn.SetKeepAliveConfig(config)
}

func (r *Request) _getUploadData() ([]byte, error) {
	// If data has already been read, then return that. This is useful for multiple calls of this method, and for proxying to Spartan servers (where query strings get set to the data).
	if len(r._data) > 0 {
		return r._data, nil
	}

	if r.DataSize == 0 {
		// A delete request in Titan. No data upload in Spartan.
		return []byte{}, nil
	}

	uploadData := make([]byte, r.DataSize)
	buf := make([]byte, 1)
	for i := 0; i < int(r.DataSize); i++ {
		n, err := r.readwriter.Read(buf)
		if err == io.EOF && n <= 0 {
			switch r.Type {
			case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			case ProtocolType_Spartan: // Spartan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "4 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		} else if err != nil && err != io.EOF {
			switch r.Type {
			case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			case ProtocolType_Spartan: // Spartan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "4 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		}

		uploadData[i] = buf[0]
	}
	//_, err := r.readwriter.Read(titanData)
	//titanData, err := io.ReadAll(io.LimitReader(r.readwriter, r.DataSize))

	r._data = uploadData
	return uploadData, nil
}

// Takes checksum hash of IP and returns it in a hex string
func (r *Request) IPHash() string {
	hasher := sha256.New()
	hasher.Write([]byte(r.IP))
	return hex.EncodeToString(hasher.Sum(nil))
}

func (r *Request) HasUserCert() bool {
	return (r.UserCert != ClientCertificate{})
}
func (r *Request) UserCertHash() string {
	return r.UserCert.Hash
}
func (r *Request) UserCertOldGeminiHash() string {
	return r.UserCert.MD5Hash
}

// Misfin, Gopher, Gemini, Titan, and Nex

// TODO: Reconsider how this works...
// Gets the requested hostname for protocols that use those in requests
// If not used in request, then gets the hostname of the server.
func (r *Request) Hostname() string {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Scroll, ProtocolType_Titan:
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Hostname()
	default:
		return r.Host.Hostname
	}
}

var defaultPorts = map[ProtocolType]string{
	ProtocolType_Gemini:     "1965",
	ProtocolType_Nex:        "1900",
	ProtocolType_NPS:        "1915", // TODO
	ProtocolType_Gopher:     "70",
	ProtocolType_Gopher_SSL: "70",
	ProtocolType_Misfin_A:   "1958",
	ProtocolType_Misfin_B:   "1958",
	ProtocolType_Misfin_C:   "1958",
	ProtocolType_Titan:      "1965",
	ProtocolType_Spartan:    "300",
	ProtocolType_Scroll:     "5699",
	ProtocolType_Guppy:      "",
	ProtocolType_Scorpion:   "",
}

// Hostname + Port. Port is excluded if it's the default of the protocol.
func (r *Request) HostnamePort() string {
	if r.Host.Port != defaultPorts[r.Type] {
		return r.Hostname() + ":" + r.Host.Port
	} else {
		return r.Hostname()
	}
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests.
// Path is unescaped if part of URL (for protocols that use URL requests). Unescaping is unnecessary for Nex and Gopher.
func (r *Request) Path() string { // TODO: Escaped vs. Unescaped
	if r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL {
		path, _, _ := strings.Cut(r.requestPath, "\t")
		path, _, _ = strings.Cut(path, "?")
		return path
	}

	result, _ := url.PathUnescape(r.requestPath)
	return result
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests
// Path is escaped if part of URL (for protocols that use URL requests). NOTE: Nex and Gopher requests are not escaped.
func (r *Request) RawPath() string {
	if r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL {
		path, _, _ := strings.Cut(r.requestString, "\t")
		path, _, _ = strings.Cut(path, "?")
		return path
	}

	return r.requestPath
}

// Limits the number of bytes of uploaded data to read when you call request.Query() or request.RawQuery().
// The default is 1024 to (somewhat) match Gemini. This only applies to .RawQuery() and .Query() calls. It
// does not apply to .GetUploadData() calls.
func (r *Request) SetSpartanQueryLimit(bytesize int64) {
	r._queryLimit = bytesize
}

// Gets the escaped query for protocols that support queries in requests.
// If Spartan, it gets the uploaded data (if there is any) as text and escapes it.
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) RawQuery() (string, error) {
	if r.Type == ProtocolType_Spartan {
		// If there's already stored data, then use that.
		// Note that the stored _data is not escaped.
		if len(r._data) > 0 {
			return url.QueryEscape(string(r._data)), nil
		}

		if r._queryLimit == 0 {
			r._queryLimit = 1024
		}
		if r.DataSize == 0 {
			return "", nil
		} else if r.DataSize > r._queryLimit {
			return "", fmt.Errorf("upload data too long, limited to %d bytes", r._queryLimit)
		} else {
			data, err := r._getUploadData() // Call this version because r.UploadSupported is not required when getting qureies of limited length in Spartan
			return url.QueryEscape(string(data)), err
		}
	} else {
		return r._rawQuery, nil
	}
}

// Gets the query for protocols that support queries in requests. The returned query is unescaped.
// If Spartan, it gets the *raw* uploaded data (if there is any) as text. To get it query escaped, use request.RawQuery()
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) Query() (string, error) {
	if r.Type == ProtocolType_Spartan {
		// If there's already stored data, then use that.
		// Note that the stored _data is not escaped.
		if len(r._data) > 0 {
			return string(r._data), nil
		}

		if r._queryLimit == 0 {
			r._queryLimit = 1024
		}
		if r.DataSize == 0 {
			return "", nil
		} else if r.DataSize > r._queryLimit {
			return "", fmt.Errorf("upload data too long, limited to %d bytes", r._queryLimit)
		} else {
			data, err := r._getUploadData() // Call this version because r.UploadSupported is not required when getting queries of limited length in Spartan
			return string(data), err
		}
	} else {
		rawQuery, _ := r.RawQuery()
		q, err := url.QueryUnescape(rawQuery)
		if err != nil {
			return "", err
		}
		return q, nil
	}
}

// NOTE: Should almost never be used
func (r *Request) Fragment() string {
	if r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Scroll {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Fragment
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			f, err := url.QueryUnescape(parts[1])
			if err != nil {
				return ""
			} else {
				return f
			}
		}
	}
}

// NOTE: Should almost never be used
func (r *Request) RawFragment() string {
	if r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Scroll {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.EscapedFragment()
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			return parts[1]
		}
	}
}

// Returns empty string if param not in route
func (r *Request) GetParam(name string) string {
	p, ok := r.params[name]
	if ok {
		return p
	}
	return ""
}

// --- Response functions ---

func (r *Request) EnableConvertMode() {
	r.flags.Set(RequestFlag_ConvertMode)
}

func (r *Request) DisableConvertMode() {
	r.flags.Clear(RequestFlag_ConvertMode)
}

func (r *Request) Redirect(format string, elem ...any) { // TODO: Add query back on, and handle proxying for non-gopher protocols
	redirect := fmt.Sprintf(format, elem...)
	redirect_without_query_and_fragment, _, _ := strings.Cut(redirect, "#")
	redirect_without_query_and_fragment, _, _ = strings.Cut(redirect_without_query_and_fragment, "?")
	addSlashSuffix := strings.HasSuffix(redirect, "/")

	newRedirect := redirect
	newRedirectWithoutProxiedUnder := redirect
	URL, _ := url.Parse(redirect)

	// Convert absolute links for non-Gopher protocols.
	if URL.Scheme == "" && path.IsAbs(redirect) && r.Type != ProtocolType_Gopher && r.Type != ProtocolType_Gopher_SSL {
		newRedirect = URL.EscapedPath()
		newRedirectWithoutProxiedUnder = newRedirect

		newRedirect = path.Join("/", r.ProxiedUnder, r.prefix, newRedirect)
		newRedirectWithoutProxiedUnder = path.Join("/", r.prefix, newRedirectWithoutProxiedUnder)

		if addSlashSuffix {
			newRedirect = newRedirect + "/"
			newRedirectWithoutProxiedUnder = newRedirectWithoutProxiedUnder + "/"
		}

		// Add the query back in
		if URL.RawQuery != "" {
			newRedirect += "?" + URL.RawQuery
		}

		// Add the fragment back in
		if URL.EscapedFragment() != "" {
			newRedirect += "#" + URL.EscapedFragment()
		}
	}

	// If same hostname or no scheme provided, handle redirection stuff.
	if URL.Hostname() == r.Hostname() || !URL.IsAbs() {
		expectedRedirectPath := newRedirectWithoutProxiedUnder // TODO: Should this be escaped or unescaped (especially for nex and gopher)?
		r.Server.GetServer().IPRateLimit_ExpectRedirectPath(r.IP, expectedRedirectPath)
	}

	fmt.Printf("New Redirect: %s\n", newRedirect)

	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		r.sendHeader("30 %s\r\n", newRedirect)
	case ProtocolType_Spartan:
		r.sendHeader("3 %s\r\n", newRedirect)
	case ProtocolType_Nex:
		r.NexListing(fmt.Sprintf("=> %s Redirect\n", newRedirect))
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL: // TODO: Assumes gophermap link
		if URL.Scheme != "" {
			r.GophermapLine("h", "Redirect", "URL:"+URL.String(), r.Host.Hostname, r.Host.Port)
		} else if path.IsAbs(redirect) { // Absolute link
			link := path.Join("/", r.ProxiedUnder, r.prefix, URL.String())
			if strings.HasSuffix(redirect, "/") { // Add back in trailing slash, since path.Join removes it
				link += "/"
			}
			r.GophermapLine(string(r.gopherItemType), "Redirect", strings.TrimSpace(link), r.Host.Hostname, r.Host.Port)
		} else { // Relative link
			link := path.Join("/", r.ProxiedUnder, r.prefix, r.Path(), URL.String())
			if strings.HasSuffix(redirect, "/") { // Add back in trailing slash, since path.Join removes it
				link += "/"
			}
			r.GophermapLine(string(r.gopherItemType), "Redirect", strings.TrimSpace(link), r.Host.Hostname, r.Host.Port)
		}
	}
}

// Sends a link to a particular route on the given server.
// Route links can reference other servers by prefixing the route path with "$ServerName".
// You can reference the current request's params by prefixing them with "$" in the route path.
func (r *Request) _routeLink(route string, name string) error {
	preceedingSlash := strings.HasPrefix(route, "/")
	trailingSlash := strings.HasSuffix(route, "/")
	parts := strings.Split(route, "/")

	crossServer := false
	scheme := ""
	hostname := r.Hostname()
	port := r.Host.Port
	resultPath := ""

	// Find desired server and replace with host:port of server
	if strings.HasPrefix(parts[0], "$") {
		handle := r.Server.SIS().FindServerByName(strings.TrimPrefix(parts[0], "$"))
		server := handle.GetServer()

		scheme = server.Scheme()
		preferredHost := server.GetPreferredHost(ProtocolType_Unknown, r.flags.Test(RequestFlag_Upload), false) // TODO: Unknown
		hostname = preferredHost.Hostname
		port = preferredHost.Port
		parts = parts[1:]
		resultPath = "/"
		crossServer = true
	} else if preceedingSlash {
		resultPath = "/"
	}

	// Find each param reference and replace with the param's value
	for _, component := range parts {
		if strings.HasPrefix(component, "$") {
			param := r.GetParam(strings.TrimPrefix(component, "$"))
			resultPath = path.Join(resultPath, param)
		} else {
			resultPath = path.Join(resultPath, component)
		}
	}

	if trailingSlash {
		resultPath = resultPath + "/"
	}

	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Spartan, ProtocolType_Scroll, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Titan:
		if crossServer {
			return r.Gemini(fmt.Sprintf("=> %s%s:%s%s %s\n", scheme, hostname, port, resultPath, name))
		} else {
			return r.Gemini(fmt.Sprintf("=> %s %s\n", resultPath, name))
		}
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		if crossServer {
			// TODO: Make sure this is correct
			return r.GophermapLine("h", name, resultPath, "URL:"+scheme+hostname, port)
		} else {
			// TODO: Detect what the route's linetype is!
			return r.GophermapLine("0", name, resultPath, hostname, port)
		}
	}

	return nil
}

// Sends a link without any conversion, RouteGroup prefix, or proxy correction.
func (r *Request) RawLink(link string, text string) error {
	return r._handleLink(link, text, false, true)
}

// Sends a link via the current expected protocol, prepending the proxied path if applicable.
// The link must be manually handled; it will not take into account routes that were created
// with a RoutePrefix. If you want to send over an absolute link, use LinkAbs to make relative
// links absolute, which *will* automatically handle routes created with a RoutePrefix.
func (r *Request) Link(link string, text string) error {
	return r._handleLink(link, text, false, false)
}

// LinkAbs turns relative links absolute by joining them with the current path of request,
// also prepending the proxied path if applicable. This can be used to take into account
// routes that were created with a RoutePrefix.
func (r *Request) LinkAbs(link string, text string) error {
	// If link is relative, join with current path of route.
	if !path.IsAbs(link) {
		addFinalSlash := strings.HasSuffix(link, "/")
		link = path.Join(r.Path(), link)
		if addFinalSlash {
			link = link + "/"
		}
	}

	return r._handleLink(link, text, false, false)
}

// Uses a regular link linetype for Gemini and Nex, and a prompt/search linetype for Spartan and Gopher.
// The link should be in standard url format, even for gopher servers. The proxied path is auto-prepended
// if applicable.
// The link must be manually handled; it will not take into account routes that were created
// with a RoutePrefix. If you want to send over an absolute link, use PromptLineAbs to make relative
// links absolute, which *will* automatically handle routes created with a RoutePrefix.
func (r *Request) PromptLine(link string, text string) error {
	return r._handleLink(link, text, true, false)
}

// Uses a regular link linetype for Gemini and Nex, and a prompt/search linetype for Spartan and Gopher.
// The link should be in standard url format, even for gopher servers. The proxied path is auto-prepended
// if applicable.
// PromptLineAbs turns relative links absolute by joining them with the current path of request,
// also prepending the proxied path if applicable. This can be used to take into account
// routes that were created with a RoutePrefix.
func (r *Request) PromptLineAbs(link string, text string) error {
	// If link is relative, join with current path of route.
	if !path.IsAbs(link) {
		addFinalSlash := strings.HasSuffix(link, "/")
		link = path.Join(r.Path(), link)
		if addFinalSlash {
			link = link + "/"
		}
	}

	return r._handleLink(link, text, true, false)
}

func (r *Request) _handleLink(link string, text string, prompt bool, raw bool) error {
	link_without_query_and_fragment, _, _ := strings.Cut(link, "#")
	link_without_query_and_fragment, _, _ = strings.Cut(link_without_query_and_fragment, "?")
	addSlashSuffix := strings.HasSuffix(link, "/")

	URL, _ := url.Parse(link)
	newLink := link

	// Convert absolute links for non-Gopher protocols.
	if !raw && URL.Scheme == "" && path.IsAbs(link) && r.Type != ProtocolType_Gopher && r.Type != ProtocolType_Gopher_SSL {
		newLink = URL.EscapedPath()

		newLink = path.Join("/", r.ProxiedUnder, r.prefix, newLink)

		if addSlashSuffix {
			newLink = newLink + "/"
		}

		// Add the query back in
		if URL.RawQuery != "" {
			newLink += "?" + URL.RawQuery
		}

		// Add the fragment back in
		if URL.EscapedFragment() != "" {
			newLink += "#" + URL.EscapedFragment()
		}
	}

	if text == "" {
		text = newLink
	}

	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.success([]byte(fmt.Sprintf("=> %s %s\n", newLink, text)))
	case ProtocolType_Spartan, ProtocolType_Scroll:
		if prompt {
			return r.success([]byte(fmt.Sprintf("=: %s %s\n", newLink, text)))
		} else {
			return r.success([]byte(fmt.Sprintf("=> %s %s\n", newLink, text)))
		}
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		if URL.Scheme != "" {
			if prompt {
				return r.success([]byte(gophermapLine("7", text, "URL:"+URL.String(), r.Hostname(), r.Host.Port)))
			} else {
				return r.success([]byte(gophermapLine("h", text, "URL:"+URL.String(), r.Hostname(), r.Host.Port)))
			}
		} else if path.IsAbs(link) { // Absolute
			if !raw {
				newLink = path.Join(r.ProxiedUnder, r.prefix, URL.EscapedPath())
			}
		} else { // Relative
			if !raw {
				newLink = path.Join(r.ProxiedUnder, r.prefix, r.RawPath(), URL.EscapedPath())
			} else {
				newLink = path.Join(r.RawPath(), URL.EscapedPath())
			}
		}

		// NOTE: Since path.Join removes trailing slashes, add them back
		if strings.HasSuffix(link_without_query_and_fragment, "/") {
			newLink += "/"
		}
		if URL.RawQuery != "" {
			newLink += "?" + URL.RawQuery // Add the query back in
		}
		if URL.EscapedFragment() != "" {
			newLink += "#" + URL.EscapedFragment() // Add the fragment back in
		}
		if text == "" {
			text = newLink
		}

		if prompt {
			return r.success([]byte(gophermapLine("7", text, newLink, r.Hostname(), r.Host.Port)))
		} else {
			return r.success([]byte(gophermapLine("1", text, newLink, r.Hostname(), r.Host.Port)))
		}
	}

	return nil
}

func (r *Request) Spartan(text string) error {
	// TODO
	return nil
}

// TODO: Add a way to configure nex width in Request (or Nex Server)
func (r *Request) Gemini(text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			return r.success([]byte(text))
		case ProtocolType_Scroll:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiProxyLinks(strings.NewReader(text), r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
		case ProtocolType_Spartan:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToSpartan(strings.NewReader(text), path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
			//return r.success([]byte(geminiToSpartan(text, r.proxiedUnder)))
		case ProtocolType_Nex:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToNex(strings.NewReader(text), 70, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
			//return r.success([]byte(geminiToNex(text, 70, r.proxiedUnder)))
		case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
			/*currentPath := r.Path()
			if r.Proxied {
				// Add on the proxiedUnder path
				currentPath = r.proxiedUnder + r.Path()
			}*/
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToGophermap(strings.NewReader(text), 70, path.Join(r.ProxiedUnder, r.prefix), r.Path(), r.proxyRoute, r.Hostname(), r.Host.Port, r.readwriter)
			//return r.success([]byte(geminiToGophermap(text, 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.Port()))) // TODO
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// Convert all links in the file
		err := r.successWithMimetype("text/gemini", []byte{})
		if err != nil {
			return err
		}
		geminiProxyLinks(strings.NewReader(text), r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
	} else {
		return r.successWithMimetype("text/gemini", []byte(text))
	}

	return nil
}

// TODO: Add a way to configure nex width in Request (or Nex Server)
func (r *Request) Scroll(text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiProxyLinks(strings.NewReader(text), r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
		case ProtocolType_Scroll:
			return r.success([]byte(text))
		case ProtocolType_Spartan:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToSpartan(strings.NewReader(text), path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
			//return r.success([]byte(geminiToSpartan(text, r.proxiedUnder)))
		case ProtocolType_Nex:
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToNex(strings.NewReader(text), 70, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
			//return r.success([]byte(geminiToNex(text, 70, r.proxiedUnder)))
		case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
			/*currentPath := r.Path()
			if r.Proxied {
				// Add on the proxiedUnder path
				currentPath = r.proxiedUnder + r.Path()
			}*/
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			geminiToGophermap(strings.NewReader(text), 70, path.Join(r.ProxiedUnder, r.prefix), r.Path(), r.proxyRoute, r.Hostname(), r.Host.Port, r.readwriter)
			//return r.success([]byte(geminiToGophermap(text, 70, r.proxiedUnder, r.Path(), r.proxyRoute, r.Hostname(), r.Server.Port()))) // TODO
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// Convert all links in the file
		err := r.successWithMimetype("text/scroll", []byte{})
		if err != nil {
			return err
		}
		geminiProxyLinks(strings.NewReader(text), r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
	} else {
		return r.successWithMimetype("text/scroll", []byte(text))
	}

	return nil
}

func (r *Request) Markdown(text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Spartan, ProtocolType_Scroll, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Titan:
			return r.success([]byte(markdownToGemini(text)))
		case ProtocolType_Nex:
			return r.success([]byte(markdownToNex(text)))
		case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
			// TODO: markdownToGophermap()
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: add markdownProxyLinks() function
	}

	return r.successWithMimetype("text/markdown", []byte(text))
}

// Send Next text.
// TODO: This is unfinished.
func (r *Request) NexListing(text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Spartan, ProtocolType_Scroll, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Titan:
			return r.success([]byte(text)) // TODO: Nex to gemini is required to convert absolute link lines.
		case ProtocolType_Nex:
			return r.success([]byte(text))
		case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
			currentPath := r.Path()
			if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
				// Add on the proxiedUnder path
				currentPath = path.Join(r.ProxiedUnder, r.prefix, r.RawPath())
			}
			err := r.success([]byte{})
			if err != nil {
				return err
			}
			nexToGophermap(strings.NewReader(text), currentPath, r.Hostname(), r.Host.Port, 70, r.readwriter)
			//return r.success([]byte(nexToGophermap(text, currentPath, r.Hostname(), r.Server.Port(), 70)))
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		err := r.successWithMimetype("text/plain", []byte{})
		if err != nil {
			return err
		}
		nexProxyLinks(strings.NewReader(text), r.Type == ProtocolType_Gopher || r.Type == ProtocolType_Gopher_SSL, path.Join(r.ProxiedUnder, r.prefix), r.readwriter)
	} else {
		return r.successWithMimetype("text/plain", []byte(text))
	}

	return nil
}

// TODO: make sure all lines end in CRLF
func (r *Request) Gophermap(text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Spartan, ProtocolType_Scroll, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Titan:
		case ProtocolType_Nex:
		case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
			return r.successWithMimetype("application/gopher-menu", []byte(text))
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: gopherProxyLinks()
	}

	return r.successWithMimetype("application/gopher-menu", []byte(text))
}

// Enter empty strings for hostname and port to use the server's hostname and port.
// TODO: Handle URL: links and cross-server links
func (r *Request) GophermapLine(linetype string, name string, selector string, hostname string, port string) error {
	isAbs := false
	if strings.HasPrefix(selector, "/") {
		isAbs = true
	}
	if hostname == "" {
		hostname = r.Hostname()
	}
	if port == "" {
		// Use request server's port
		port = r.Host.Port
	}
	if r.flags.Test(RequestFlag_ConvertMode) {
		switch r.Type {
		case ProtocolType_Gemini, ProtocolType_Nex, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
			if isAbs {
				return r.success([]byte(fmt.Sprintf("=> %s:%s%s %s\n", hostname, port, selector, name)))
			} else {
				// Assume relative selector is not going cross-server.
				return r.success([]byte(fmt.Sprintf("=> %s %s\n", selector, name)))
			}
		case ProtocolType_Spartan:
			if linetype == "7" {
				if isAbs {
					return r.PromptLine(fmt.Sprintf("%s:%s%s", hostname, port, selector), name)
				} else {
					return r.PromptLine(fmt.Sprintf("%s:%s%s", hostname, port, selector), name)
				}
			} else {
				if isAbs {
					return r.success([]byte(fmt.Sprintf("=> %s:%s%s %s\n", hostname, port, selector, name)))
				} else {
					// Assume relative selector is not going cross-server.
					return r.success([]byte(fmt.Sprintf("=> %s %s\n", selector, name)))
				}
			}
		}
	} else if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: gophermapProxyLinks()
	}
	result := fmt.Sprintf("%s%s\t%s\t%s\t%s\r\n", linetype, name, selector, hostname, port)
	return r.successWithMimetype("application/gopher-menu", []byte(result))
}

func (r *Request) PlainText(format string, elem ...any) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		// TODO
	}
	return r.successWithMimetype("text/plain", []byte(fmt.Sprintf(format, elem...)))
}

// TODO
func (r *Request) TextWithMimetype(mimetype string, text string) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		// TODO
	}
	return r.successWithMimetype(mimetype, []byte(text))
}
func (r *Request) Bytes(mimetype string, data []byte) error {
	if r.flags.Test(RequestFlag_ConvertMode) {
		// TODO
	}
	return r.successWithMimetype(mimetype, data)
}

// Will send the abstract that is already stored in the scroll metadata
func (r *Request) SendAbstract(mimetype string) error {
	return r.Abstract(mimetype, r.scrollMetadata.Abstract)
}

// For sending an Abstract file. Will parse data to get Author metadata.
func (r *Request) Abstract(mimetype string, data string) error {
	abstractData := r.GetAbstractWithInlineMetadata(data)
	r.successWithMimetype(mimetype, nil) // Header and Metadata
	return r.PlainText("%s", abstractData)
}

func (r *Request) GetAbstractWithInlineMetadata(data string) (abstractData string) {
	abstractData = data

	// Scan first line of abstract to get metadata
	if !strings.HasPrefix(data, "#") {
		buf_reader := bufio.NewReader(strings.NewReader(data))
		for {
			line, err := buf_reader.ReadString('\n')
			if err != nil {
				break
			} else if strings.HasPrefix(line, "#") {
				rest, _ := io.ReadAll(buf_reader)
				abstractData = line + string(rest)
				break
			} else {
				field, value, hasValue := strings.Cut(line, ":")
				field = strings.ToLower(strings.TrimSpace(field))
				value = strings.TrimSpace(value)
				if hasValue {
					if field == "author" {
						r.scrollMetadata.Author = value
					} else if field == "publish-date" {
						publishdate, err := time.Parse(time.RFC3339, string(value))
						if err == nil {
							r.scrollMetadata.PublishDate = publishdate
						}
					} else if field == "modification-date" || field == "modified-date" || field == "update-date" || field == "updated-date" {
						modificationdate, err := time.Parse(time.RFC3339, string(value))
						if err == nil {
							r.scrollMetadata.UpdateDate = modificationdate
						}
					} else if field == "language" {
						r.SetLanguage(value)
					} else if field == "udc-class" {
						// Check if integer first, if not, then parse as string
						classification, err := strconv.Atoi(value)
						r.scrollMetadata.Classification = ScrollResponseUDC(classification)
						if err != nil {
							r.scrollMetadata.Classification = ClassificationStringToUDC(value)
						}
					}
				}
			}
		}
	}

	return abstractData
}

// If filename is empty, the mimetype will still be detected from the content of the data.
func (r *Request) DataStream(filename string, reader io.ReadSeeker) (err error) {
	// NOTE: We can skip the mimetype detection for protocols that don't require it (protocols that don't send the mimetype in a response header), but only if we are not in convert mode
	mt := ""
	extension := filepath.Ext(filename)
	if r.flags.Test(RequestFlag_ConvertMode) || r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" || r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Spartan || r.Type == ProtocolType_Scroll || r.Type == ProtocolType_Titan || r.Type == ProtocolType_Misfin_A || r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		mt = "text/plain"
		if strings.HasSuffix(filename, ".gmi") || strings.HasSuffix(filename, ".gemini") {
			mt = "text/gemini"
		} else if strings.HasSuffix(filename, ".scroll") {
			mt = "text/scroll"
		} else if strings.HasSuffix(filename, "index") || strings.HasSuffix(filename, ".nex") {
			mt = "text/nex" // TODO: Assume nex for this for now, but come up with a better way later.
		} else if strings.HasSuffix(filename, "gophermap") {
			mt = "application/gopher-menu"
		} else if extension == ".pem" {
			mt = "application/x-pem-file"
		} else {
			mime, err := mimetype.DetectReader(reader)
			if err != nil {
				return err
			}
			mt = mime.String()
		}

		_, err = reader.Seek(0, io.SeekStart)
		if err != nil {
			return err
		}
	}

	// Security warnings
	if CertFilename(filename) {
		if !r.flags.Test(RequestFlag_AllowServeCert) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] An SSL cert/key file ('%s') is being served.\n", filename)
	} else if PrivacySensitiveFilename(filename) {
		return r.TemporaryFailure("File not allowed.")
	}
	if DotFilename(filename) {
		if !r.flags.Test(RequestFlag_AllowServeDotFiles) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] Serving a dot file ('%s')\n", filename)
	}

	// Get Audio File tag metadata
	switch mt {
	case "audio/mpeg", "audio/mp3", "audio/x-mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wave", "audio/mp4", "video/mp4":
		tagInfo, err := tag.ReadFrom(reader)
		if err == nil {
			r.scrollMetadata.Author = tagInfo.AlbumArtist()
			if r.scrollMetadata.Author == "" {
				r.scrollMetadata.Author = tagInfo.Artist()
			}

			track, totalTracks := tagInfo.Track()
			r.scrollMetadata.PublishDate = time.Date(tagInfo.Year(), 0, 0, 0, 0, 0, 0, time.UTC)
			disc, totalDiscs := tagInfo.Disc()
			composer := tagInfo.Composer()
			r.scrollMetadata.Abstract = fmt.Sprintf("# %s\n, Track: %d/%d\nArtist: %s\nAlbum: %s\nAlbum Artist: %s\nComposer: %s\nDisc: %d/%d\n", tagInfo.Title(), track, totalTracks, tagInfo.Artist(), tagInfo.Album(), tagInfo.AlbumArtist(), composer, disc, totalDiscs)
		}
		reader.Seek(0, io.SeekStart)
	}

	// If only Metadata requested, via Scroll Protocol, then don't serve the actual file but just its metadata
	if r.flags.Test(RequestFlag_ScrollMetadataRequested) {
		if r.scrollMetadata.Abstract != "" {
			return r.Abstract(mt, r.scrollMetadata.Abstract)
		}
	} else {
		// Get Abstract info
		if mt == "text/gemini" || mt == "text/scroll" || mt == "text/markdown" {
			// Get the info from the gemtext/scrolltext/markdown file itself
			headerData := ScanUpToTitle(reader)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(headerData)
		}
	}

	// Check if Convert Mode or if proxied. If proxied or prefixed, the called functions will change the links in the document to the proxy links.
	// If in convert mode, the document will fully convert to the default documenttype of the server. This is usually used for directory listings/menus.
	if r.flags.Test(RequestFlag_ConvertMode) || r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: Change this to convert as it streams
		data, err := io.ReadAll(reader)
		if err != nil {
			return err
		}
		if mt == "text/gemini" {
			err = r.Gemini(string(data))
		} else if mt == "text/scroll" {
			err = r.Scroll(string(data))
		} else if mt == "text/nex" {
			err = r.NexListing(string(data))
		} else if mt == "text/markdown" {
			err = r.Markdown(string(data))
		} else if mt == "application/gopher-menu" {
			err = r.Gophermap(string(data))
		} else {
			r.successWithMimetype(mt, nil)
			reader.Seek(0, io.SeekStart)
			_, err = io.Copy(r.readwriter, reader)
		}
		return err
	}

	r.successWithMimetype(mt, nil)
	_, err = io.Copy(r.readwriter, reader)
	return err
}

// Reads whole file into memory, automatically determins the correct mimetype, and sends it over (converting if in convert mode).
// If file not found or error on opening file, returns with error without sending anything back to the connection.
// Prefer FileMimetype() if you know the mimetype beforehand.
func (r *Request) File(filePath string) error {
	// Open file, get file mimetype, convert if convertMode is set, and send file
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	stat_times, _ := times.Stat(filePath)
	if stat_times.HasBirthTime() {
		r.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	r.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	// NOTE: We can skip the mimetype detection for protocols that don't require it if we are not in convert mode
	mt := ""
	extension := filepath.Ext(filePath)
	if r.flags.Test(RequestFlag_ConvertMode) || r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" || r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Spartan || r.Type == ProtocolType_Scroll || r.Type == ProtocolType_Titan || r.Type == ProtocolType_Misfin_A || r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		mt = "text/plain"
		if strings.HasSuffix(filePath, ".gmi") || strings.HasSuffix(filePath, ".gemini") {
			mt = "text/gemini"
		} else if strings.HasSuffix(filePath, ".scroll") {
			mt = "text/scroll"
		} else if strings.HasSuffix(filePath, "index") || strings.HasSuffix(filePath, ".nex") {
			mt = "text/nex" // TODO: Assume nex for this for now, but come up with a better way later.
		} else if strings.HasSuffix(filePath, "gophermap") {
			mt = "application/gopher-menu"
		} else if extension == ".pem" {
			mt = "application/x-pem-file"
		} else {
			mime, err := mimetype.DetectReader(file)
			if err != nil {
				return err
			}
			mt = mime.String()
		}

		_, err = file.Seek(0, io.SeekStart)
		if err != nil {
			return err
		}
	}

	// Security warnings
	if CertFilename(filePath) {
		if !r.flags.Test(RequestFlag_AllowServeCert) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] An SSL cert/key file ('%s') is being served.\n", filePath)
	} else if PrivacySensitiveFilename(filePath) {
		return r.TemporaryFailure("File not allowed.")
	}
	if DotFilename(filePath) {
		if !r.flags.Test(RequestFlag_AllowServeDotFiles) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] Serving a dot file ('%s')\n", filePath)
	}

	// Get Audio File tag metadata
	switch mt {
	case "audio/mpeg", "audio/mp3", "audio/x-mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wave", "audio/mp4", "video/mp4":
		tagInfo, err := tag.ReadFrom(file)
		if err == nil {
			r.scrollMetadata.Author = tagInfo.AlbumArtist()
			if r.scrollMetadata.Author == "" {
				r.scrollMetadata.Author = tagInfo.Artist()
			}

			track, totalTracks := tagInfo.Track()
			r.scrollMetadata.PublishDate = time.Date(tagInfo.Year(), 0, 0, 0, 0, 0, 0, time.UTC)
			disc, totalDiscs := tagInfo.Disc()
			composer := tagInfo.Composer()
			r.scrollMetadata.Abstract = fmt.Sprintf("# %s\n, Track: %d/%d\nArtist: %s\nAlbum: %s\nAlbum Artist: %s\nComposer: %s\nDisc: %d/%d\n", tagInfo.Title(), track, totalTracks, tagInfo.Artist(), tagInfo.Album(), tagInfo.AlbumArtist(), composer, disc, totalDiscs)
		}
		file.Seek(0, io.SeekStart)
	}

	// If only Metadata requested, via Scroll Protocol, then don't serve the actual file but just its metadata
	if r.flags.Test(RequestFlag_ScrollMetadataRequested) {
		if r.scrollMetadata.Abstract != "" {
			return r.Abstract(mt, r.scrollMetadata.Abstract)
		} else {
			// Get abstract file
			abstract_file, err := os.Open(filePath + ".abstract")
			if err != nil {
				// Abstract file not found
				title := ""

				// If a gemtext/scrolltext/markdown file, get the title from the file
				if mt == "text/gemini" || mt == "text/markdown" || mt == "text/scroll" {
					title_, author, publishdate, updatedate, language, classification := GetInlineMetadataFromGemtext(file)
					title = title_
					if author != "" {
						r.scrollMetadata.Author = author
					}
					if (publishdate != time.Time{}) {
						r.scrollMetadata.PublishDate = publishdate
					}
					if (updatedate != time.Time{}) {
						r.scrollMetadata.UpdateDate = updatedate
					}
					if language != "" {
						r.scrollMetadata.Language = language
					}
					if classification != ScrollResponseUDC_Unclassed {
						r.scrollMetadata.Classification = classification
					}
				}

				// Set title to filename if it is empty
				if title == "" {
					title = "# " + filepath.Base(file.Name())
				}
				// Send blank abstract with title
				r.PlainText("%s", title+"\n")
				return nil
			}
			defer abstract_file.Close()

			abstract_data, _ := io.ReadAll(abstract_file)
			return r.Abstract(mt, string(abstract_data))
		}
	} else {
		// Get Abstract info
		abstract_file, err := os.Open(filePath + ".abstract")
		if err == nil {
			defer abstract_file.Close()
			abstract_data, _ := io.ReadAll(abstract_file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(string(abstract_data))
		} else if mt == "text/gemini" || mt == "text/scroll" || mt == "text/markdown" {
			// Get the info from the gemtext/scrolltext/markdown file itself
			headerData := ScanUpToTitle(file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(headerData)
		}
	}

	// Check if Convert Mode or if proxied. If proxied or prefixed, the called functions will change the links in the document to the proxy links.
	// If in convert mode, the document will fully convert to the default documenttype of the server. This is usually used for directory listings/menus.
	if r.flags.Test(RequestFlag_ConvertMode) || r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: Change this to convert as it streams
		data, err := io.ReadAll(file)
		if err != nil {
			return err
		}
		if mt == "text/gemini" {
			err = r.Gemini(string(data))
		} else if mt == "text/scroll" {
			err = r.Scroll(string(data))
		} else if mt == "text/nex" {
			err = r.NexListing(string(data))
		} else if mt == "text/markdown" {
			err = r.Markdown(string(data))
		} else if mt == "application/gopher-menu" {
			err = r.Gophermap(string(data))
		} else {
			r.successWithMimetype(mt, nil)
			file.Seek(0, io.SeekStart)
			_, err = io.Copy(r.readwriter, file)
		}
		return err
	}

	r.successWithMimetype(mt, nil)
	_, err = io.Copy(r.readwriter, file)
	return err
}

func (r *Request) FileMimetype(mimetype string, filePath string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	stat_times, _ := times.Stat(filePath)
	if stat_times.HasBirthTime() {
		r.scrollMetadata.PublishDate = stat_times.BirthTime().UTC()
	}
	r.scrollMetadata.UpdateDate = stat_times.ModTime().UTC()

	// Security warnings
	if CertFilename(filePath) || CertMimetype(mimetype) {
		if !r.flags.Test(RequestFlag_AllowServeCert) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] An SSL cert/key file ('%s') is being served.\n", filePath)
	} else if PrivacySensitiveFilename(filePath) || PrivacySensitiveMimetype(mimetype) {
		return r.TemporaryFailure("File not allowed.")
	}
	if DotFilename(filePath) {
		if !r.flags.Test(RequestFlag_AllowServeDotFiles) {
			return r.TemporaryFailure("File not allowed.")
		}
		fmt.Printf("[Warning] Serving a dot file ('%s')\n", filePath)
	}

	// Get Audio File tag metadata
	switch mimetype {
	case "audio/mpeg", "audio/mp3", "audio/x-mpeg", "audio/ogg", "audio/x-ogg", "application/ogg", "application/x-ogg", "audio/flac", "audio/x-flac", "audio/wav", "audio/wave", "audio/x-wav", "audio/vnd.wave", "audio/mp4", "video/mp4":
		tagInfo, err := tag.ReadFrom(file)
		if err == nil {
			r.scrollMetadata.Author = tagInfo.AlbumArtist()
			if r.scrollMetadata.Author == "" {
				r.scrollMetadata.Author = tagInfo.Artist()
			}

			track, totalTracks := tagInfo.Track()
			r.scrollMetadata.PublishDate = time.Date(tagInfo.Year(), 0, 0, 0, 0, 0, 0, time.UTC)
			disc, totalDiscs := tagInfo.Disc()
			composer := tagInfo.Composer()
			r.scrollMetadata.Abstract = fmt.Sprintf("# %s\n, Track: %d/%d\nArtist: %s\nAlbum: %s\nAlbum Artist: %s\nComposer: %s\nDisc: %d/%d\n", tagInfo.Title(), track, totalTracks, tagInfo.Artist(), tagInfo.Album(), tagInfo.AlbumArtist(), composer, disc, totalDiscs)
		}
		file.Seek(0, io.SeekStart)
	}

	// If only Metadata requested, via Scroll Protocol, then don't serve the actual file but just its metadata
	if r.flags.Test(RequestFlag_ScrollMetadataRequested) {
		// Abstract
		if r.scrollMetadata.Abstract != "" {
			return r.Abstract(mimetype, r.scrollMetadata.Abstract)
		} else {
			// Get abstract file
			abstract_file, err := os.Open(filePath + ".abstract")
			if err != nil {
				// Abstract file not found
				title := ""

				// If a gemtext/scrolltext/markdown file, get the title from the file
				if mimetype == "text/gemini" || mimetype == "text/markdown" || mimetype == "text/scroll" {
					title_, author, publishdate, updatedate, language, classification := GetInlineMetadataFromGemtext(file)
					title = title_
					if author != "" {
						r.scrollMetadata.Author = author
					}
					if (publishdate != time.Time{}) {
						r.scrollMetadata.PublishDate = publishdate
					}
					if (updatedate != time.Time{}) {
						r.scrollMetadata.UpdateDate = updatedate
					}
					if language != "" {
						r.scrollMetadata.Language = language
					}
					if classification != ScrollResponseUDC_Unclassed {
						r.scrollMetadata.Classification = classification
					}
				}

				// Set title to filename if it is empty
				if title == "" {
					title = "# " + filepath.Base(file.Name())
				}
				// Send blank abstract with title
				r.PlainText("%s", title+"\n")
				return nil
			}
			defer abstract_file.Close()

			abstract_data, _ := io.ReadAll(abstract_file)
			return r.Abstract(mimetype, string(abstract_data))
		}
	} else {
		// Get Abstract info
		abstract_file, err := os.Open(filePath + ".abstract")
		if err == nil {
			defer abstract_file.Close()
			abstract_data, _ := io.ReadAll(abstract_file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(string(abstract_data))
		} else if mimetype == "text/gemini" || mimetype == "text/scroll" || mimetype == "text/markdown" {
			// Get the info from the gemtext/scrolltext/markdown file itself
			headerData := ScanUpToTitle(file)
			r.scrollMetadata.Abstract = r.GetAbstractWithInlineMetadata(headerData)
		}
	}

	// Check if Convert Mode or if proxied. If proxied or prefixed, the called functions will change the links in the document to the proxy links.
	// If in convert mode, the document will fully convert to the default documenttype of the server. This is usually used for directory listings/menus.
	if r.flags.Test(RequestFlag_ConvertMode) || r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		// TODO: Change this to convert as it streams
		data, err := io.ReadAll(file)
		if err != nil {
			return err
		}
		if mimetype == "text/gemini" {
			err = r.Gemini(string(data))
		} else if mimetype == "text/scroll" {
			err = r.Scroll(string(data))
		} else if mimetype == "text/nex" {
			err = r.NexListing(string(data))
		} else if mimetype == "text/markdown" {
			err = r.Markdown(string(data))
		} else if mimetype == "application/gopher-menu" {
			err = r.Gophermap(string(data))
		} else {
			r.successWithMimetype(mimetype, nil)
			file.Seek(0, io.SeekStart)
			_, err = io.Copy(r.readwriter, file)
		}
		return err
	}

	r.successWithMimetype(mimetype, nil)
	_, err = io.Copy(r.readwriter, file)
	return err
}

// TODO: Use a buffer bool and provide a 32KB buffer from the pool.
// Streams a file, using a 32KB buffer by default. If you want to control the buffer size, use StreamBuffer.
func (r *Request) Stream(mimetype string, reader io.Reader) error {
	r.successWithMimetype(mimetype, nil)
	_, err := io.Copy(r.readwriter, reader)
	return err
}

// Provide a buffer to handle the copy. This allows you to control your own buffer size to increase performance. For big files, create a big buffer (at the cost of memory). For small files, create a smaller buffer.
func (r *Request) StreamBuffer(mimetype string, reader io.Reader, buf []byte) error {
	r.successWithMimetype(mimetype, nil)
	_, err := io.CopyBuffer(r.readwriter, reader, buf)
	return err
}

func (r *Request) NotFound(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("51 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("4 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		result := "Error: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		// TODO: What should the selector be? And does it matter?
		result := "3Error: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Host.Port + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

func (r *Request) TemporaryFailure(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("40 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 "+format+"\r\n", elem...) // TODO: Distinguish between temporary server error and client error
	case ProtocolType_Nex:
		result := "Temporary Failure: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		result := "3Temporary Failure: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Host.Port + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

func (r *Request) CGIFailure(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("42 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 "+format+"\r\n", elem...) // TODO: Distinguish between temporary server error and client error
	case ProtocolType_Nex:
		result := "CGI Failure: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		result := "3CGI Failure: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Host.Port + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

func (r *Request) BadRequest(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("59 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("4 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		result := "Bad Request: " + fmt.Sprintf(format, elem...) + "\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	case ProtocolType_Gopher:
		result := "3Bad Request: " + fmt.Sprintf(format, elem...) + "\t/\t" + r.Hostname() + "\t" + r.Host.Port + "\r\n"
		_, err := r.readwriter.Write([]byte(result))
		return err
	}

	return nil
}

// TODO: When proxying gopher to gemini, translate the gopher input requested message/error to a gemini input request.
func (r *Request) RequestInput(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan:
		return r.sendHeader("10 "+format+"\r\n", elem...)
	case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.TemporaryFailure("Misfin doesn't support input.")
	case ProtocolType_Spartan:
		// Since spartan has no input request response, send over gemtext with a prompt linetype link that goes to the same url/path as this current page
		selector := r.Path()
		/*if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		selector = path.Join(r.ProxiedUnder, r.prefix, r.RawPath())
		if strings.HasSuffix(r.Path(), "/") { // Add trailing slash back in, since path.Join strips it
			selector += "/"
		}
		}*/
		return r.PromptLine(selector, fmt.Sprintf(format, elem...))
	case ProtocolType_Nex:
		// Doesn't support input
		return r.TemporaryFailure("Nex doesn't support input.")
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		selector := r.Path()
		/*if r.flags.Test(RequestFlag_Proxied) || r.ProxiedUnder != "" || r.prefix != "" {
		selector = path.Join(r.ProxiedUnder, r.prefix, r.RawPath())
		if strings.HasSuffix(r.Path(), "/") { // Add trailing slash back in, since path.Join strips it
			selector += "/"
		}
		}*/
		return r.GophermapLine("7", fmt.Sprintf(format, elem...), selector, r.Hostname(), r.Host.Port)
		// Does support input // TODO
	}
	return nil
}

func (r *Request) RequestClientCert(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("60 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 Spartan doesn't support client certs.\r\n") // Considered a server error
	case ProtocolType_Nex:
		return r.TemporaryFailure("Nex doesn't support client certs.")
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		// Doesn't support client certs // TODO
	}
	return nil
}

func (r *Request) ClientCertNotAuthorized(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("61 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 Spartan doesn't support client certs.\r\n") // Considered a server error
	case ProtocolType_Nex:
		return r.TemporaryFailure("Nex doesn't support client certs.\n")
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		// Doesn't support client certs // TODO
	}
	return nil
}

func (r *Request) ServerUnavailable(format string, elem ...any) error {
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Scroll, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		return r.sendHeader("41 "+format+"\r\n", elem...)
	case ProtocolType_Spartan:
		return r.sendHeader("5 "+format+"\r\n", elem...)
	case ProtocolType_Nex:
		r.TemporaryFailure("Server Unavailable: "+format+"\n", elem)
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		// TODO: Determine if current route is a gophermap or text (or some other itemtype)
	}
	return nil
}

func (r *Request) success(data []byte) error {
	defaultMimetype := "text/plain"
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Spartan, ProtocolType_Titan, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
		defaultMimetype = "text/gemini"
	case ProtocolType_Nex:
		defaultMimetype = "text/nex"
	case ProtocolType_Gopher, ProtocolType_Gopher_SSL:
		defaultMimetype = "application/gopher-menu"
	case ProtocolType_Scroll:
		defaultMimetype = "text/scroll"
	}

	return r.successWithMimetype(defaultMimetype, data)
}

func (r *Request) successWithMimetype(mimetype string, data []byte) error {
	mimetypeMetadataDocument := strings.HasPrefix(mimetype, "text/gemini") || strings.HasPrefix(mimetype, "text/scroll") || strings.HasPrefix(mimetype, "text/nex")

	// Handle language
	if r.scrollMetadata.Language != "" && r.scrollMetadata.Language != "none" { // Should use BCP47
		mediatype, _, _ := mime.ParseMediaType(mimetype)
		parsedlang, err := language.Parse(r.scrollMetadata.Language)
		if err == nil && MimetypeGetsLangParam(mediatype) {
			mimetype = mimetype + "; lang=" + parsedlang.String()
		}
	} else if r.scrollMetadata.Language != "none" {
		mediatype, params, err := mime.ParseMediaType(mimetype)
		// If mimetype is a textual natural-language type, then add the lang parameter if it doesn't already exist
		if _, ok := params["lang"]; !ok && err == nil && MimetypeGetsLangParam(mediatype) {
			defaultLanguage := r.Server.DefaultLanguage()
			parsedLang, err := language.Parse(defaultLanguage)
			if err == nil && defaultLanguage != "" {
				mimetype = mimetype + "; lang=" + parsedLang.String()
			}
		}
	}

	var err error
	switch r.Type {
	case ProtocolType_Gemini, ProtocolType_Titan:
		// Send heading with metadata if gemtext/nex-listing is being proxied
		if r.flags.Test(RequestFlag_Proxied) && mimetypeMetadataDocument {
			author := ""
			if r.scrollMetadata.Author != "" {
				author = "Author: " + r.scrollMetadata.Author + "\n"
			}
			publishDate := ""
			if r.scrollMetadata.PublishDate != (time.Time{}) {
				publishDate = "Publish Date: " + r.scrollMetadata.PublishDate.UTC().Format(time.RFC3339) + "\n"
			}
			modifiedDate := ""
			if r.scrollMetadata.UpdateDate != (time.Time{}) {
				modifiedDate = "Modification Date: " + r.scrollMetadata.UpdateDate.UTC().Format(time.RFC3339) + "\n"
			}
			err = r.sendHeader("20 %s\r\n%s%s%s", mimetype, author, publishDate, modifiedDate)
		} else {
			err = r.sendHeader("20 %s\r\n", mimetype)
		}
		if err != nil {
			return err
		}
	case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
	// TODO
	case ProtocolType_Scroll:
		statusCode := UDCToSuccessResponse(r.scrollMetadata.Classification)
		err = r.sendHeader("%d %s\r\n%s\r\n%s\r\n%s\r\n", statusCode, mimetype, r.scrollMetadata.Author, r.scrollMetadata.PublishDate.UTC().Format(time.RFC3339), r.scrollMetadata.UpdateDate.UTC().Format(time.RFC3339))
		if err != nil {
			return err
		}
	case ProtocolType_Spartan:
		// Send heading with metadata if gemtext/nex-listing is being proxied
		if r.flags.Test(RequestFlag_Proxied) && mimetypeMetadataDocument {
			author := ""
			if r.scrollMetadata.Author != "" {
				author = "Author: " + r.scrollMetadata.Author + "\n"
			}
			publishDate := ""
			if r.scrollMetadata.PublishDate != (time.Time{}) {
				publishDate = "Publish Date: " + r.scrollMetadata.PublishDate.UTC().Format(time.RFC3339) + "\n"
			}
			modifiedDate := ""
			if r.scrollMetadata.UpdateDate != (time.Time{}) {
				modifiedDate = "Modification Date: " + r.scrollMetadata.UpdateDate.UTC().Format(time.RFC3339) + "\n"
			}
			err = r.sendHeader("2 %s\r\n%s%s%s", mimetype, author, publishDate, modifiedDate)
		} else {
			err = r.sendHeader("2 %s\r\n", mimetype)
		}
		if err != nil {
			return err
		}
	}

	if len(data) > 0 {
		_, err = r.readwriter.Write(data)
	}
	return err
}

func (r *Request) sendHeader(format string, elem ...any) error {
	if r.flags.Test(RequestFlag_HeaderSent) {
		return nil
	}
	header := fmt.Sprintf(format, elem...)
	if header != "" {
		_, err := r.readwriter.Write([]byte(header))
		r.flags.Set(RequestFlag_HeaderSent)
		return err
	}
	return nil
}

// TODO: Ability to return errors (especially NotFound, which redirects to the NotFoundHandler)
type RequestHandler func(request *Request)

// Proxy Handler - handles proxying routes from one server to another, or to a route on the same server
func proxyHandler(request *Request) {
	// TODO: As a security mechanism, make sure we do not get infinite proxying when having two servers that proxy each other.
	// Ideally, if we find that this request is proxying a route that proxies back to the originating server, just redirect to that page.
	// Technically, the request header limit also limites the depth of the proxying, but we should still have this here just in case
	// (and for protocols in the future that might have a much larger header length limit).

	proxyRoute := request.proxyRoute
	var server *VirtualServer = nil
	if strings.HasPrefix(proxyRoute, "$") {
		trimmed := strings.TrimPrefix(proxyRoute, "$")
		if strings.HasPrefix(trimmed, "/") {
			// If starts with $ but there's no server name ("$/"), assume the current server
			proxyRoute = trimmed
		} else {
			parts := strings.SplitN(trimmed, "/", 2)
			destServerName := parts[0]
			proxyRoute = parts[1]
			handle := request.Server.SIS().FindServerByName(destServerName)
			server = handle.GetServer()
		}
	}
	if server == nil {
		server = request.Server.GetServer()
	}
	proxyRoute = strings.TrimPrefix(proxyRoute, "/")

	fmt.Printf("Proxied from server '%s' to server '%s'\n", request.Server.Name(), server.Name)

	resultRouteString := InsertParamsIntoRouteString(proxyRoute, request.params, request.GlobString, false)

	// Find route node from server we are proxying to.
	node, globString, params := server.Router.Search(resultRouteString)
	if node != nil && node.Handler != nil {
		proxyRequest := &Request{}
		*proxyRequest = *request // NOTE: Copies all pointers
		proxyRequest.flags.Set(RequestFlag_ConvertMode)
		proxyRequest.flags.Set(RequestFlag_Proxied)

		// The proxied request will set the requestPath to the route of the server we are proxying to.
		// This excludes ProxiedUnder from the requestPath. The requestString includes the entire
		// request path/URL, including the fragment but excluding the query string.
		proxyRequest.requestPath = resultRouteString
		proxyRequest.requestString = request.requestString

		proxyRequest.GlobString = globString
		proxyRequest.params = params
		proxyRequest.servePath = node.servePath
		proxyRequest.prefix = InsertParamsIntoRouteString(node.prefix, params, globString, true)

		// If gopherItemType of node was assigned, then use that. Otherwise, continue to use the ProxyRoute's assigned gopher type
		if node.gopherItemType != 0 {
			proxyRequest.gopherItemType = node.gopherItemType
		}

		// NOTE: When proxying *to* a Spartan server, since spartan doesn't have proper query strings because
		// they act like normal data uploads, when the query or upload data is requested in the spartan
		// server, it will first check if _data is non-empty, and if so, it will use that as the data upload.
		// NOTE: However, when proxying *from* a Spartan server, do not get the query, since that will start
		// reading the data upload. Instead, we want to delay the data upload to when the handler requests it.
		if request.Type != ProtocolType_Spartan { // Get query when not proxying *from* a spartan server.
			query, _ := request.Query()
			proxyRequest._data = []byte(query)
			proxyRequest.DataMime = "text/plain"
		}

		proxyRequest.proxyRoute = node.proxyRoute
		proxyRequest.fromRoute = node.GetRoutePath()
		proxyRequest.proxiedFromRoute = request.fromRoute
		fmt.Printf("Proxied from route '%s' to route '%s'\n", proxyRequest.proxiedFromRoute, proxyRequest.fromRoute)

		// If the original request was already proxied, include its ProxiedUnder value in our new ProxiedUnder value.
		if request.flags.Test(RequestFlag_Proxied) {
			proxyRequest.ProxiedUnder = path.Join(request.ProxiedUnder, request.prefix, strings.TrimSuffix(request.RawPath(), request.GlobString))
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		} else {
			proxyRequest.ProxiedUnder = path.Join("/", request.prefix, strings.TrimSuffix(request.RawPath(), request.GlobString))
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		}

		// proxyRequest.Server // TODO: For now, the proxyRequest.Server is the source Server, not the one being proxied. Look more into this, or perhaps add another field as proxyRequest.ProxyServer
		node.Handler(proxyRequest)
	} else {
		request.NotFound("Proxy route not found.")
	}
}

// Reverse will insert params into components starting with ":". Otherwise, params will be inserted into components starting with "$"
func InsertParamsIntoRouteString(route string, params map[string]string, globString string, reverse bool) string {
	resultRouteString := "/"
	parts := strings.Split(route, "/")
	for _, component := range parts {
		if !reverse && strings.HasPrefix(component, "$") {
			param := params[strings.TrimPrefix(component, "$")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if reverse && strings.HasPrefix(component, ":") {
			param := params[strings.TrimPrefix(component, ":")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if component == "*" {
			if !strings.HasSuffix(resultRouteString, "/") {
				resultRouteString = resultRouteString + "/" + globString
			} else {
				resultRouteString = resultRouteString + globString
			}
		} else {
			resultRouteString = path.Join(resultRouteString, component)
		}
	}

	return resultRouteString
}
