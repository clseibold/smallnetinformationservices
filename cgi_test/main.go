package main

import (
	"fmt"
	"os"
	"strings"
)

var header_names = [...]string{"GATEWAY_INTERFACE", "SERVER_SOFTWARE", "SERVER_PROTOCOL", "", "SCHEME", "REQUEST_METHOD", "SERVER_NAME", "SERVER_PORT", "REAL_LISTEN_ADDR", "REAL_LISTEN_PORT", "SSL_TLS_SNI", "", "REMOTE_ADDR", "REMOTE_PORT", "REMOTE_HOST", "", "URL", "GEMINI_URL", "SCROLL_URL", "TITAN_URL", "SPARTAN_URL", "NEX_URL", "GOPHER_URL", "REQUEST_URI", "REQUEST_DOMAIN", "REQUEST_PATH", "", "SCRIPT_NAME", "PATH_INFO", "PATH_TRANSLATED", "QUERY_STRING", "", "AUTH_TYPE", "TLS_CIPHER", "TLS_VERSION", "TLS_CLIENT_HASH", "TLS_CLIENT_NOT_BEFORE", "TLS_CLIENT_NOT_AFTER", "TLS_CLIENT_SERIAL_NUMBER", "TLS_CLIENT_SERIAL", "TLS_CLIENT_ISSUER", "REMOTE_IDENT", "REMOTE_USER", "TLS_CLIENT_SUBJECT", "REMOTE_USER_ID", "", "CONTENT_LENGTH"}

// This is a CGI script that can be used to print out all of the CGI environment variables for testing, debugging, and documentation purposes. It can speak the following protocols: gemini, gopher, nex, spartan, scroll, and titan. Misfin support will be added later.
func main() {
	var headers = make(map[string]string, len(header_names))
	for _, n := range header_names {
		if n == "" {
			continue
		}
		if v, exists := os.LookupEnv(n); exists {
			headers[n] = v
		}
	}

	hostname := ""
	if v := headers["SSL_TLS_SNI"]; v != "" { // Get SNI first, and if that isn't given, then go to the other env variables.
		hostname = headers["SSL_TLS_SNI"]
	} else if v := headers["REQUEST_DOMAIN"]; v != "" {
		hostname = headers["REQUEST_DOMAIN"]
		/*} else if v := headers["GEMINI_URL"]; strings.ToUpper(headers["SERVER_PROTOCOL"]) == "GEMINI" && v != "" {
		} else if v := headers["URL"]; v != "" {
		} else if v := headers["REQUEST_URI"]; v != "" {*/
	} else if v := headers["SERVER_NAME"]; v != "" {
		hostname = headers["SERVER_NAME"]
	}

	port := ""
	if v := headers["SERVER_PORT"]; v != "" {
		port = headers["SERVER_PORT"]
		/*} else if v := headers["GEMINI_URL"]; strings.ToUpper(headers["SERVER_PROTOCOL"]) == "GEMINI" && v != "" {
		} else if v := headers["URL"]; v != "" {
		} else if v := headers["REQUEST_URI"]; v != "" {*/
	} else {
		port = headers["REAL_LISTEN_PORT"]
	}

	switch strings.ToUpper(headers["SERVER_PROTOCOL"]) {
	case "GEMINI", "TITAN":
		fmt.Printf("20 text/gemini\r\n# CGI Environment Variables\r\n\r\n")
		// Print out all of the environment variables
		for _, n := range header_names {
			if n == "" {
				fmt.Printf("\r\n")
				continue
			}
			if v, exists := os.LookupEnv(n); exists {
				fmt.Printf("%s: %s\r\n", n, v)
			}
		}
	case "SCROLL":
		fmt.Printf("24 text/scroll\r\n# CGI Environment Variables\r\n\r\n")
		// Print out all of the environment variables
		for _, n := range header_names {
			if n == "" {
				fmt.Printf("\r\n")
				continue
			}
			if v, exists := os.LookupEnv(n); exists {
				fmt.Printf("%s: %s\r\n", n, v)
			}
		}
	case "SPARTAN":
		fmt.Printf("2 text/gemini\r\n# CGI Environment Variables\r\n\r\n")
		// Print out all of the environment variables
		for _, n := range header_names {
			if n == "" {
				fmt.Printf("\r\n")
				continue
			}
			if v, exists := os.LookupEnv(n); exists {
				fmt.Printf("%s: %s\r\n", n, v)
			}
		}
	case "NEX":
		fmt.Printf("CGI Environment Variables\n")
		fmt.Printf("-------------------------\n\n")
		// Print out all of the environment variables
		for _, n := range header_names {
			if n == "" {
				fmt.Printf("\n")
				continue
			}
			if v, exists := os.LookupEnv(n); exists {
				fmt.Printf("%s: %s\n", n, v)
			}
		}
	case "MISFIN", "MISFIN(B)", "MISFIN(C)":
	case "GOPHER":
		fmt.Printf("iCGI Environment Variables\t/\t%s\t%s\r\n", hostname, port)
		fmt.Printf("i-------------------------\t/\t%s\t%s\r\n", hostname, port)
		fmt.Printf("i\t/\t%s\t%s\r\n", hostname, port)
		// Print out all of the environment variables
		for _, n := range header_names {
			if n == "" {
				fmt.Printf("i\t/\t%s\t%s\r\n", hostname, port)
				continue
			}
			if v, exists := os.LookupEnv(n); exists {
				fmt.Printf("i%s: %s\t/\t%s\t%s\r\n", n, v, hostname, port)
			}
		}
	}
}
