package smallnetinformationservices

import (
	"bytes"
	"context"
	"crypto"
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/cretz/bine/tor"
	"golang.org/x/sync/semaphore"
)

type ProtocolType uint

const ProtocolType_Unknown ProtocolType = 0
const ProtocolType_Gemini ProtocolType = 1
const ProtocolType_Nex ProtocolType = 2
const ProtocolType_NPS ProtocolType = 3
const ProtocolType_Gopher ProtocolType = 4
const ProtocolType_Gopher_SSL ProtocolType = 5 // TODO
const ProtocolType_Misfin_A ProtocolType = 6   // Deprecated
const ProtocolType_Misfin_B ProtocolType = 7
const ProtocolType_Misfin_C ProtocolType = 8
const ProtocolType_Titan ProtocolType = 9
const ProtocolType_Spartan ProtocolType = 10
const ProtocolType_Scroll ProtocolType = 11
const ProtocolType_Guppy ProtocolType = 12    // TODO
const ProtocolType_Scorpion ProtocolType = 13 // TODO: Has both TLS and Non-TLS?

func ProtocolIsTLS(protocol ProtocolType) bool {
	switch protocol {
	case ProtocolType_Gemini, ProtocolType_Gopher_SSL, ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Titan, ProtocolType_Scroll:
		return true
	case ProtocolType_Gopher, ProtocolType_Nex, ProtocolType_NPS, ProtocolType_Spartan, ProtocolType_Guppy:
		return false
	}

	return false
}

func ProtocolToName(protocol ProtocolType) string {
	switch protocol {
	case ProtocolType_Gemini:
		return "gemini"
	case ProtocolType_Nex:
		return "nex"
	case ProtocolType_NPS:
		return "nps"
	case ProtocolType_Gopher:
		return "gopher"
	case ProtocolType_Gopher_SSL:
		return "gophers"
	case ProtocolType_Misfin_A:
		return "misfin(A)"
	case ProtocolType_Misfin_B:
		return "misfin(B)"
	case ProtocolType_Misfin_C:
		return "misfin(C)"
	case ProtocolType_Titan:
		return "titan"
	case ProtocolType_Spartan:
		return "spartan"
	case ProtocolType_Scroll:
		return "scroll"
	case ProtocolType_Guppy:
		return "guppy"
	case ProtocolType_Scorpion:
		return "scorpion"
	}

	return "unknown"
}

func NameToProtocol(name string) ProtocolType {
	switch name {
	case "gemini":
		return ProtocolType_Gemini
	case "nex":
		return ProtocolType_Nex
	case "nps":
		return ProtocolType_NPS
	case "gopher":
		return ProtocolType_Gopher
	case "gophers":
		return ProtocolType_Gopher_SSL
	case "misfin(A)":
		return ProtocolType_Misfin_A
	case "misfin(B)":
		return ProtocolType_Misfin_B
	case "misfin(C)":
		return ProtocolType_Misfin_C
	case "titan":
		return ProtocolType_Titan
	case "spartan":
		return ProtocolType_Spartan
	case "scroll":
		return ProtocolType_Scroll
	case "guppy":
		return ProtocolType_Guppy
	case "scorpion":
		return ProtocolType_Scorpion
	}

	return ProtocolType_Unknown
}

func ProtocolHasDownload(protocol ProtocolType) bool {
	switch protocol {
	case ProtocolType_Gemini:
		return true
	case ProtocolType_Nex:
		return true
	case ProtocolType_NPS:
		return false
	case ProtocolType_Gopher:
		return true
	case ProtocolType_Gopher_SSL:
		return true
	case ProtocolType_Misfin_A:
		return false
	case ProtocolType_Misfin_B:
		return false
	case ProtocolType_Misfin_C:
		return false
	case ProtocolType_Titan:
		return false // TODO
	case ProtocolType_Spartan:
		return true
	case ProtocolType_Scroll:
		return true
	case ProtocolType_Guppy:
		return true
	case ProtocolType_Scorpion:
		return true
	}

	return false
}

func ProtocolHasUpload(protocol ProtocolType) bool {
	switch protocol {
	case ProtocolType_Gemini:
		return false
	case ProtocolType_Nex:
		return false
	case ProtocolType_NPS:
		return true
	case ProtocolType_Gopher:
		return false
	case ProtocolType_Gopher_SSL:
		return false
	case ProtocolType_Misfin_A:
		return true
	case ProtocolType_Misfin_B:
		return true
	case ProtocolType_Misfin_C:
		return true
	case ProtocolType_Titan:
		return true
	case ProtocolType_Spartan:
		return true // TODO: Has both upload and download
	case ProtocolType_Scroll:
		return false
	case ProtocolType_Guppy:
		return false
	case ProtocolType_Scorpion:
		return true
	}

	return false
}

type PortListenerID struct {
	BindAddress string
	BindPort    string
}

type PortListener struct {
	SIS                      *SISContext
	BindAddress              string
	BindPort                 string
	MaxConcurrentConnections int
	running                  bool

	TorServer VirtualServerHandle
	TorKey    crypto.PrivateKey
	tor       bool // Port listeners may only have one onion address
	i2p       bool // Port listeners may only have one i2p address

	msg             chan string
	maintenanceMode bool
}

// Create new PortListener
func NewPortListener(SIS *SISContext, BindAddress string, Port string) *PortListener {
	// make(map[string]BindPortHostnameCertificate, 1), make(map[ServerHost]ServerHandle, 2)
	return &PortListener{SIS, BindAddress, Port, 2000, false, VirtualServerHandle{}, nil, false /* tor */, false /* i2p */, nil, false}
}

func (l *PortListener) ID() PortListenerID {
	return PortListenerID{l.BindAddress, l.BindPort}
}

func (l *PortListener) DefaultServer() VirtualServerHandle {
	return l.SIS.certmanager.PortListenerProperties[l.ID()].defaultServer
}

func (l *PortListener) SetTorServerRoute(server VirtualServerHandle) {
	l.TorServer = server
	l.tor = true
}

func (l *PortListener) SetTorKey(key crypto.PrivateKey) {
	if !l.tor {
		panic("Must set a Tor server before adding a key.")
	}
	l.TorKey = key
}

func (l *PortListener) Start(wg *sync.WaitGroup) {
	// Make sure msg channel is set
	l.msg = make(chan string, 30)
	fmt.Printf("[Listener:%s:%s] Starting with %d max concurrent connections.\n", l.BindAddress, l.BindPort, l.MaxConcurrentConnections)
	l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.BindPort, "Starting with %d max concurrent connections.\n", l.MaxConcurrentConnections)
	if l.tor {
		wg.Add(1)
		go l.startListener(wg, true)
	}
	l.startListener(wg, false)
}

func (l *PortListener) startListener(wg *sync.WaitGroup, usingTor bool) {
	defer wg.Done()
	l.running = true

	// TLS Configuration for TLS connections only
	// Setup TLS Config and Listener
	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
		ClientAuth: tls.RequestClientCert,
		NextProtos: []string{"gemini", "misfin", "gophers", "titan", "scroll"},
	}
	//tlsConfig.Certificates = make([]tls.Certificate, 1)

	// SNI Handling
	strictSni := false
	tlsConfig.GetCertificate = func(chi *tls.ClientHelloInfo) (*tls.Certificate, error) {
		if chi.ServerName == "" { // Not using SNI
			// TODO: Try to supported protocols (ALPN) stuff next, then send the default if not available
			defaultCert := l.SIS.certmanager.GetDefaultCertForPortListener(l)
			return defaultCert.GetTLSCertificate(), nil
		} else if c, has := l.SIS.certmanager.GetCertificate(l.BindAddress, l.BindPort, chi.ServerName); has {
			return c.GetTLSCertificate(), nil
		} else if strictSni {
			return nil, fmt.Errorf("strict SNI enabled - No certificate found for domain: %q, closing connection", chi.ServerName)
		} else {
			defaultCert := l.SIS.certmanager.GetDefaultCertForPortListener(l)
			return defaultCert.GetTLSCertificate(), nil
		}
	}

	// Support logging TLS keys for debugging
	keylogfile := os.Getenv("SSLKEYLOGFILE")
	if keylogfile != "" {
		w, err := os.OpenFile(keylogfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0660)
		if err == nil {
			tlsConfig.KeyLogWriter = w
			defer w.Close()
		}
	}

	// Setup Regular Connection Listener (or Tor Listener if using Tor)
	var listener net.Listener
	if usingTor {
		t := l.SIS.GetTor()

		// Wait at most a few minutes to publish the service
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
		defer cancel()

		porti, _ := strconv.Atoi(l.BindPort)
		onion, onionErr := t.Listen(ctx, &tor.ListenConf{Version3: true, Key: l.TorKey, RemotePorts: []int{porti}})
		if onionErr != nil {
			fmt.Printf("Failed to create onion service on 127.0.0.1:%s", l.BindPort)
			return
		}
		listener = onion
	} else {
		// TODO: tcp4 and tcp6 options, also the bind address for ipv6 is different (especially if trying to listen on all interfaces with ipv6; should be something like `[::1]`?)
		var listenErr error
		listener, listenErr = net.Listen("tcp", l.BindAddress+":"+l.BindPort)
		if listenErr != nil {
			panic(listenErr)
		}
	}

	tcpListener := &MaybeTLSListener{&tcpKeepAliveListener{listener.(*net.TCPListener)}, tlsConfig}
	defer tcpListener.Close()

	// Loop to accept incoming connections
	//var bufpool sync.Pool
	//bufio.NewReadWriter(nil, nil)
	/*bufpool.New = func() interface{} {
		return bufio.NewReadWriter(bufio.NewReaderSize(nil, 1024), bufio.NewWriterSize(nil, 1))
	}*/
	var connChan chan net.Conn
	if l.MaxConcurrentConnections == -1 {
		connChan = make(chan net.Conn, 4000)
	} else {
		connChan = make(chan net.Conn, l.MaxConcurrentConnections*2)
	}

	var sem *semaphore.Weighted
	if l.MaxConcurrentConnections == -1 { // No Limit - semaphores released almost as soon as they are acquired
		sem = semaphore.NewWeighted(int64(runtime.GOMAXPROCS(0)))
	} else {
		sem = semaphore.NewWeighted(int64(l.MaxConcurrentConnections))
	}

	acceptLoop := func() {
		for {
			// Wait until there's an open space for the connection
			sem.Acquire(context.Background(), 1)
			conn, err := tcpListener.Accept() // NOTE: Blocking (unless listener is closed)
			if errors.Is(err, net.ErrClosed) {
				sem.Release(1)
				break
			} else if err != nil {
				fmt.Printf("accept error: %v\n", err)
				sem.Release(1)
				continue
			}

			// Check IP Blocking stuff
			ip, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
			if l.SIS.IsIPBlocked(ip) {
				conn.Close()
				sem.Release(1)
				continue
			}

			// If not expecting redirect, do rate limiting here. Otherwise, wait to do rate limiting until we have read the header of the request (which occurs in the goroutine for the connection)
			/*if !s.IPRateLimit_IsExpectingRedirect(ip) {
				if s.IsIPRateLimited(ip) {
					// Rate limited, return a 44 slow down error
					conn.Write([]byte("44 1\r\n"))
					conn.Close()
					sem.Release(1)
					continue
				}
			}*/

			connChan <- conn
		}
	}
	go acceptLoop()

mainloop:
	for {
		var netConn net.Conn
		ok := true
		received := 0
		hasConn := false

		// Try to get both
		var msg string // For messages to the portListener
		select {
		case msg, ok = <-l.msg:
			received++
		default:
		}
		select {
		case netConn = <-connChan:
			received++
			hasConn = true
		default:
		}

		// Haven't received anything, block the loop until one is received
		if received == 0 {
			select {
			case msg, ok = <-l.msg:
			case netConn = <-connChan:
				hasConn = true
			}
		}
		// If need to shut down
		if !ok {
			fmt.Printf("[PortListener:%s:%s] Shutting down.\n", l.BindAddress, l.BindPort)
			l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.BindPort, "Shutting down.")

			if hasConn {
				if tlsConn, isTLS := netConn.(*tls.Conn); isTLS {
					tlsConn.Close()
				} else {
					netConn.Close()
				}
			}
			tcpListener.Close()
			break mainloop
		} else if !hasConn {
			// Handle server messages here
			switch msg {
			case "enter_maintenance":
				l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.BindPort, "Entering maintenance mode.")
				l.maintenanceMode = true
			case "exit_maintenance":
				l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.BindPort, "Exiting maintenance mode.")
				l.maintenanceMode = false
			}
		}

		// If no max number of concurrent connections specified, then release 1 from the semaphor before the goroutine starts
		if l.MaxConcurrentConnections == -1 {
			sem.Release(1)
		}

		if hasConn {
			if _, isTLS := netConn.(*tls.Conn); isTLS {
				go l.multiplex(netConn, sem, true, netConn.(*tls.Conn).NetConn().(*peekedConn).Conn.(*net.TCPConn))
			} else {
				go l.multiplex(netConn, sem, false, netConn.(*peekedConn).Conn.(*net.TCPConn))
			}
		}
	}

	l.running = false
}

// Multiplexes multiple protocols, finds the server for each, and sends the connection off to the proper server
func (l *PortListener) multiplex(conn net.Conn, sem *semaphore.Weighted, isTLS bool, _tcpConn *net.TCPConn) {
	defer func() {
		if isTLS {
			conn.(*tls.Conn).Close()
		} else {
			conn.Close()
		}
	}()
	defer func() {
		if l.MaxConcurrentConnections != -1 {
			sem.Release(1)
		}
	}()

	defer func() {
		if r := recover(); r != nil {
			panicString := ""
			switch r := r.(type) {
			case string:
				panicString = r
			}
			fmt.Printf("PortListener:%s:%s panic: %s\n%s\n", l.BindAddress, l.BindPort, panicString, string(debug.Stack()))
			if !l.SIS.configless {
				l.SIS.Log(ServerType_Unknown, "PortListener:"+l.BindAddress+":"+l.BindPort, "panic: %s\n%s\n", panicString, string(debug.Stack()))
			}
		}
	}()

	// Check if TLS Connection, and get the TLS connection
	if !isTLS {
		// Check Gopher, Nex, NPS, and Spartan
		requestHeader, err := l.readNonTLSHeader(conn)
		if err != nil {
			fmt.Printf("Error reading non-tls header: %s\n", err.Error())
			switch requestHeader.Protocol {
			case ProtocolType_Gopher:
				host := l.DefaultServer().GetServer().GetPreferredHostOfPortListener(ProtocolType_Gopher, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
				conn.Write([]byte(fmt.Sprintf("3%s\t/\t%s\t%s\r\n", err.Error(), host.Hostname, host.Port)))
			case ProtocolType_Spartan:
				conn.Write([]byte("4 " + err.Error() + "\r\n"))
			case ProtocolType_Nex:
				conn.Write([]byte("Error: " + err.Error() + "\n"))
			case ProtocolType_NPS:
				conn.Write([]byte("Error: " + err.Error() + "\n"))
			case ProtocolType_Gemini, ProtocolType_Scroll:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Titan:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Gopher_SSL:
				host := l.DefaultServer().GetServer().GetPreferredHostOfPortListener(ProtocolType_Gopher_SSL, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
				conn.Write([]byte(fmt.Sprintf("3%s.\t/\t%s\t%s\r\n", err.Error(), host.Hostname, host.Port)))
			}
			return
		}

		isUpload := requestHeader.ContentLength > 0 || requestHeader.Protocol == ProtocolType_NPS
		server, hasServer := l.SIS.certmanager.GetServerFromHostMapping(l.BindAddress, l.BindPort, requestHeader.Hostname, requestHeader.Port, isUpload, requestHeader.SCGI)
		if requestHeader.SCGI && !hasServer {
			// If a host for a specific hostname+port isn't found, try blank hostname, blank port, and blank hostname+port for SCGI hosts that don't care about the hostname or port.
			if server, hasServer = l.SIS.certmanager.GetServerFromHostMapping(l.BindAddress, l.BindPort, requestHeader.Hostname, "", isUpload, requestHeader.SCGI); !hasServer {
				if server, hasServer = l.SIS.certmanager.GetServerFromHostMapping(l.BindAddress, l.BindPort, "", requestHeader.Port, isUpload, requestHeader.SCGI); !hasServer {
					server, hasServer = l.SIS.certmanager.GetServerFromHostMapping(l.BindAddress, l.BindPort, "", "", isUpload, requestHeader.SCGI)
				}
			}
		}
		if hasServer {
			serverPtr := server.Handle.GetServer()
			if l.maintenanceMode {
				switch requestHeader.Protocol {
				case ProtocolType_Gopher:
					host := serverPtr.GetPreferredHostOfPortListener(ProtocolType_Gopher, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
					conn.Write([]byte(fmt.Sprintf("3Server in Maintenance Mode\t/\t%s\t%s\r\n", host.Hostname, host.Port)))
				case ProtocolType_Spartan:
					conn.Write([]byte("5 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Nex:
					conn.Write([]byte("Server in Maintenance Mode.\n"))
				case ProtocolType_NPS:
					conn.Write([]byte("Server in Maintenance Mode.\n"))
				case ProtocolType_Gemini, ProtocolType_Scroll:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Titan:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Gopher_SSL:
					host := serverPtr.GetPreferredHostOfPortListener(ProtocolType_Gopher_SSL, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
					conn.Write([]byte(fmt.Sprintf("3Server in Maintenance Mode\t/\t%s\t%s\r\n", host.Hostname, host.Port)))
				}
				return
			}

			host := serverPtr.GetHostOfPortListener(requestHeader.Protocol, isUpload, l.BindAddress, l.BindPort, requestHeader.Hostname, server.ServePort, requestHeader.SCGI)
			host.Port = server.ServePort // Set host's serve port to the serve port specified in the host mapping so that the request knows the serve port for all protocols.

			ip := ""
			remote_port := ""
			if !requestHeader.SCGI {
				ip, remote_port, _ = net.SplitHostPort(conn.RemoteAddr().String())
			} else {
				ip = requestHeader.SCGI_REMOTE_ADDR
				remote_port = requestHeader.SCGI_REMOTE_PORT
			}
			serverPtr.handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
		}
	} else {
		tlsConn, _ := conn.(*tls.Conn)
		// Give 10 seconds max for the TLS handshake
		ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		err := tlsConn.HandshakeContext(ctx)
		cancel()
		if err != nil {
			fmt.Printf("TLS handshake error: %v\n", err)
			return
		}

		// Check Gopher SSL, Gemini, Misfin, or Titan
		requestHeader, err := l.readTLSHeader(conn)
		if err != nil {
			fmt.Printf("Error reading tls header: %s\n", err.Error())
			switch requestHeader.Protocol {
			case ProtocolType_Gemini, ProtocolType_Scroll:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Titan:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
				conn.Write([]byte(fmt.Sprintf("59 %s.\r\n", err.Error())))
			case ProtocolType_Gopher_SSL:
				host := l.DefaultServer().GetServer().GetPreferredHostOfPortListener(ProtocolType_Gopher_SSL, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
				conn.Write([]byte(fmt.Sprintf("3%s.\t/\t%s\t%s\r\n", err.Error(), host.Hostname, host.Port)))
			}
			return
		}

		isUpload := requestHeader.ContentLength > 0 || requestHeader.Protocol == ProtocolType_NPS
		if server, hasServer := l.SIS.certmanager.GetServerFromHostMapping(l.BindAddress, l.BindPort, requestHeader.Hostname, requestHeader.Port, isUpload, requestHeader.SCGI); hasServer {
			serverPtr := server.Handle.GetServer()
			if l.maintenanceMode {
				switch requestHeader.Protocol {
				case ProtocolType_Gemini, ProtocolType_Scroll:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Titan:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Misfin_A, ProtocolType_Misfin_B, ProtocolType_Misfin_C:
					conn.Write([]byte("41 Server in Maintenance Mode.\r\n"))
				case ProtocolType_Gopher_SSL:
					host := serverPtr.GetPreferredHostOfPortListener(ProtocolType_Gopher_SSL, false, l.BindAddress, l.BindPort, requestHeader.SCGI)
					conn.Write([]byte(fmt.Sprintf("3Server in Maintenance Mode\t/\t%s\t%s\r\n", host.Hostname, host.Port)))
				}
				return
			}

			host := serverPtr.GetHostOfPortListener(requestHeader.Protocol, isUpload, l.BindAddress, l.BindPort, requestHeader.Hostname, server.ServePort, requestHeader.SCGI)
			host.Port = server.ServePort // Set host's serve port to the serve port specified in the host mapping so that the request knows the serve port for all protocols.

			ip, remote_port, _ := net.SplitHostPort(conn.RemoteAddr().String())
			serverPtr.handleRequest(conn, requestHeader, host, ip, remote_port, _tcpConn)
		}
	}
}

// Handles headers for Gopher, Nex, NPS, and Spartan
// Gopher: 255 bytes for selector
func (l *PortListener) readNonTLSHeader(conn net.Conn) (RequestHeader, error) {
	// Protocol
	protocol := ProtocolType_Unknown

	// Get expected (or default in the case of spartan) non-TLS protocol based on
	listenerProperties := l.SIS.certmanager.PortListenerProperties[l.ID()]
	if listenerProperties.nex {
		protocol = ProtocolType_Nex
	} else if listenerProperties.gopher {
		protocol = ProtocolType_Gopher
	} else if listenerProperties.nps {
		protocol = ProtocolType_NPS
	} else if listenerProperties.spartan {
		protocol = ProtocolType_Spartan
	}

	// SCGI Variables
	is_scgi := false
	scgi_header_length := 0

	// Read the request
	maxDataSize := 1024 - 2
	if listenerProperties.nps { // TODO
		maxDataSize = 1024 * 1024 * 1 // 1 MB
	} else if listenerProperties.nex || listenerProperties.gopher {
		maxDataSize = 1024 * 8 // 8 KB
	}
	var request []byte = make([]byte, 0, maxDataSize)
	var overflow bool
	delim := []byte("\r\n")
	delim_scgi := []byte(":")
	if listenerProperties.nex { // TODO
		delim = []byte("\n")
	} else if listenerProperties.nps {
		delim = []byte("\n.")
	}
	buf := make([]byte, 1)
	for {
		n, err := conn.Read(buf)
		if err == io.EOF && n <= 0 { // TODO: These responses need to look at the expected protocol
			// Error
			responseBadURL := "59 Request invalid.\r\n"
			conn.Write([]byte(responseBadURL))
			return RequestHeader{Protocol: protocol}, err
		} else if err != nil && err != io.EOF {
			// Error
			fmt.Printf("unknown error reading request header; %v\n", err)
			return RequestHeader{Protocol: protocol}, err
		}

		request = append(request, buf...)
		if bytes.HasSuffix(request, delim) {
			request = request[:len(request)-len(delim)]
			break
		} else if bytes.HasSuffix(request, delim_scgi) {
			// Check that header is just an integer
			var err error
			if scgi_header_length, err = strconv.Atoi(string(request[:len(request)-len(delim_scgi)])); err == nil {
				is_scgi = true
				break
			}
		}

		// Detect HTTP Verbs and instantly reject the request
		if bytes.HasPrefix(request, []byte("GET")) || bytes.HasPrefix(request, []byte("HEAD")) || bytes.HasPrefix(request, []byte("POST")) || bytes.HasPrefix(request, []byte("PUT")) || bytes.HasPrefix(request, []byte("DELETE")) || bytes.HasPrefix(request, []byte("CONNECT")) || bytes.HasPrefix(request, []byte("OPTIONS")) || bytes.HasPrefix(request, []byte("TRACE")) || bytes.HasPrefix(request, []byte("PATCH")) {
			return RequestHeader{Protocol: protocol}, fmt.Errorf("http not supported")
		}

		if len(request) > maxDataSize { // TODO: What is the actual limit for Nex?
			overflow = true
			break
		}
	}

	if overflow {
		fmt.Printf("request length too long\n")
		/*responseLengthTooLong := "59 Request length too long. Total length cannot be more than 2048 bytes.\r\n"
		conn.Write([]byte(responseLengthTooLong))*/
		return RequestHeader{Protocol: protocol}, fmt.Errorf("request length too long")
	}

	// Handle SCGI - read headers, reconstruct the request header based on the protocol being used, and handle TLS User Certificate Stuff
	if is_scgi {
		// Get headers
		headerData := make([]byte, scgi_header_length)
		if _, err := io.ReadFull(conn, headerData); err != nil {
			return RequestHeader{}, fmt.Errorf("Incorrectly formatted SCGI header.")
		}
		conn.Read(buf) // trailing comma

		// Parse headers
		headers := make(map[string]string)
		parts := bytes.Split(headerData, []byte{0})
		for i := 0; i < len(parts)-1; i += 2 {
			headers[string(parts[i])] = string(parts[i+1])
		}

		// Check for supported protocols
		supportedProtocols := map[string]string{"GEMINI": "gemini", "TITAN": "titan", "GOPHER": "gopher", "GOPHERS": "gophers", "MISFIN": "misfin", "MISFIN(A)": "misfin", "MISFIN(B)": "misfin", "MISFIN(C)": "misfin"} // TODO: Switch to new schemeToProtocol map
		serverProtocol, _ := headers["SERVER_PROTOCOL"]
		if _, supported := supportedProtocols[strings.ToUpper(serverProtocol)]; !supported {
			return RequestHeader{}, fmt.Errorf("Protocol does not have SCGI support.")
		}

		// Check that the scheme matches the protocol
		scheme, _ := headers["SCHEME"]
		if strings.ToUpper(scheme) != strings.ToUpper(supportedProtocols[strings.ToUpper(serverProtocol)]) {
			return RequestHeader{}, fmt.Errorf("SCGI: scheme does not match protocol.")
		}

		return scgiResultHeader(conn, headers, false)
	} else {
		// For gopher and nex servers, the defaultServer is guaranteed to be the nex/gopher server, since only one host and server is allowed
		if listenerProperties.nex || listenerProperties.nps || listenerProperties.gopher { // TODO
			resultHeader := RequestHeader{}
			resultHeader.Protocol = protocol
			resultHeader.Request = strings.TrimSuffix(string(request), "\r")

			host := l.DefaultServer().GetServer().GetPreferredHostOfPortListener(protocol, listenerProperties.nps, l.BindAddress, l.BindPort, is_scgi)
			resultHeader.Hostname = host.Hostname
			resultHeader.Port = host.Port
			return resultHeader, nil
		} else if listenerProperties.spartan {
			resultHeader := RequestHeader{}
			resultHeader.Protocol = protocol
			resultHeader.Port = ""

			parts := strings.Split(string(request), " ")
			if len(parts) < 3 {
				return RequestHeader{Protocol: protocol}, fmt.Errorf("bad request string")
			}

			resultHeader.Hostname = parts[0] // Hostname
			resultHeader.Request = parts[1]  // Path

			// Content Length
			var parse_err error
			resultHeader.ContentLength, parse_err = strconv.ParseInt(parts[2], 10, 64)
			if parse_err != nil {
				return resultHeader, parse_err
			}

			return resultHeader, nil
		}
		return RequestHeader{Protocol: protocol}, fmt.Errorf("protocol not supported")
	}
}

// Read header for Gopher SSL, Gemini, Misfin, and Titan // Add Spartan support in the future
// Returns protocol, request header, and error
func (l *PortListener) readTLSHeader(conn net.Conn) (RequestHeader, error) {
	listenerProperties := l.SIS.certmanager.PortListenerProperties[l.ID()]
	tlsState := conn.(*tls.Conn).ConnectionState()

	// SCGI Variables
	is_scgi := false
	// scgi_header_length := 0

	// Read the request header
	maxHeaderSize := 2048 - 2
	var request []byte = make([]byte, 0, maxHeaderSize)
	var requestHeaderOverflow bool

	// Read until we hit a \r\n or a space (or integer + colon for scgi). Gemini and Titan don't use spaces in their request headers, but Misfin(B) and Scroll do.
	delim := []byte("\r\n")
	delim_misfinB := []byte(" ")
	delim_scgi := []byte(":")
	buf := make([]byte, 1)
	for {
		n, err := conn.Read(buf)
		if err == io.EOF && n <= 0 {
			return RequestHeader{}, err
		} else if err != nil && err != io.EOF {
			return RequestHeader{}, err
		}

		request = append(request, buf...)
		// Parse request string until a CRLF (for gemini and titan) or a space (for misfin). In misfin, everything after the space is the metadata and message body, ending with another CRLF
		if bytes.HasSuffix(request, delim) {
			request = request[:len(request)-len(delim)]
			break
		} else if bytes.HasSuffix(request, delim_misfinB) {
			request = request[:len(request)-len(delim_misfinB)]
			break
		} else if bytes.HasSuffix(request, delim_scgi) {
			// Check that header is just an integer
			var err error
			if _, err = strconv.Atoi(string(request[:len(request)-len(delim_scgi)])); err == nil {
				is_scgi = true
				break
			}
		}

		// Detect HTTP Verbs and instantly reject the request
		if bytes.HasPrefix(request, []byte("GET")) || bytes.HasPrefix(request, []byte("HEAD")) || bytes.HasPrefix(request, []byte("POST")) || bytes.HasPrefix(request, []byte("PUT")) || bytes.HasPrefix(request, []byte("DELETE")) || bytes.HasPrefix(request, []byte("CONNECT")) || bytes.HasPrefix(request, []byte("OPTIONS")) || bytes.HasPrefix(request, []byte("TRACE")) || bytes.HasPrefix(request, []byte("PATCH")) {
			return RequestHeader{}, fmt.Errorf("http not supported")
		}

		if len(request) > maxHeaderSize {
			requestHeaderOverflow = true
			break
		}
	}

	if is_scgi {
		return RequestHeader{}, fmt.Errorf("SCGI application servers will not respond to SCGI TLS connections.")
	}

	// Check for a tab, which will determine weather this request header is a Misfin(C) request or a Misfin(B) request.
	// Misfin C Format: misfin://<MAILBOX>@<HOSTNAME><TAB><CONTENT-LENGTH><CRLF><MESSAGE>
	// Misfin B Format: misfin://<MAILBOX>@<HOSTNAME><SPACE><MESSAGE><CRLF>
	// NOTE: This won't mess up Titan or Gemini parsing, as their request headers dont have tabs in them.
	requestHeader, contentLengthStr, isMisfinC := strings.Cut(string(request), "\t")

	// Get the URL
	URL, err := url.Parse(requestHeader)
	if err != nil {
		return RequestHeader{}, fmt.Errorf("invalid URL")
	}

	// gopher requests don't use a scheme or hostname
	if URL.Scheme == "" {
		// Try to get a protocol with ALPN
		if tlsState.NegotiatedProtocol != "" {
			URL.Scheme = tlsState.NegotiatedProtocol
		} else if listenerProperties.gopher { // TODO
			URL.Scheme = "gophers"
		} else {
			// Otherwise default to scheme of default server
			URL.Scheme = strings.TrimSuffix(l.DefaultServer().GetServer().Scheme(), "://")
		}
	}

	var resultHeader RequestHeader
	resultHeader.Protocol = ProtocolType_Gemini
	resultHeader.Port = URL.Port()
	switch URL.Scheme {
	case "gemini":
		resultHeader.Protocol = ProtocolType_Gemini
		resultHeader.Request = requestHeader
		if resultHeader.Port == "" {
			resultHeader.Port = defaultPorts[ProtocolType_Gemini]
		}
	case "gophers", "gopher":
		resultHeader.Protocol = ProtocolType_Gopher_SSL
		resultHeader.Request = requestHeader
		if resultHeader.Port == "" {
			resultHeader.Port = defaultPorts[ProtocolType_Gopher_SSL]
		}
	case "scroll":
		resultHeader.Protocol = ProtocolType_Scroll
		if resultHeader.Port == "" {
			resultHeader.Port = defaultPorts[ProtocolType_Scroll]
		}

		// Since we stopped at a space, get the rest of the header data
		var restOfRequest []byte
		for {
			n, err := conn.Read(buf)
			if err == io.EOF && n <= 0 {
				return RequestHeader{}, err
			} else if err != nil && err != io.EOF {
				return RequestHeader{}, err
			}
			restOfRequest = append(restOfRequest, buf...)
			if bytes.HasSuffix(restOfRequest, []byte("\r\n")) {
				restOfRequest = restOfRequest[:len(restOfRequest)-len("\r\n")]
				break
			}

			if len(restOfRequest) > maxHeaderSize-len(requestHeader) {
				requestHeaderOverflow = true
				break
			}
		}
		resultHeader.Request = requestHeader
		if strings.HasPrefix(string(restOfRequest), "+") {
			resultHeader.MetadataRequest = true
			restOfRequest = restOfRequest[len("+"):]
		}
		parts := strings.Split(string(restOfRequest), ",")
		for _, p := range parts {
			resultHeader.Languages = append(resultHeader.Languages, strings.TrimSpace(p))
		}
	case "misfin": // TODO: Unfinished
		resultHeader.Request = requestHeader
		if isMisfinC {
			resultHeader.Protocol = ProtocolType_Misfin_C
			if resultHeader.Port == "" {
				resultHeader.Port = defaultPorts[ProtocolType_Misfin_C]
			}

			var parseErr error
			resultHeader.ContentLength, parseErr = strconv.ParseInt(contentLengthStr, 10, 64)
			if parseErr != nil {
				fmt.Printf("error reading request\n")
				responseBadURL := "59 Request invalid.\r\n"
				conn.Write([]byte(responseBadURL))
				return resultHeader, parseErr
			}
			if resultHeader.ContentLength > 16384 {
				fmt.Printf("request length too long\n")
				responseLengthTooLong := "59 Request length too long. Total metadata+message length cannot be more than 16KB for Misfin(C), or 2KB for Misfin(AB).\r\n"
				conn.Write([]byte(responseLengthTooLong))
				return resultHeader, fmt.Errorf("content length too long")
			}
		} else {
			resultHeader.Protocol = ProtocolType_Misfin_B
			if resultHeader.Port == "" {
				resultHeader.Port = defaultPorts[ProtocolType_Misfin_B]
			}
		}
	case "titan":
		resultHeader.Protocol = ProtocolType_Titan
		if resultHeader.Port == "" {
			resultHeader.Port = defaultPorts[ProtocolType_Titan]
		}

		parts := strings.Split(requestHeader, ";")
		resultHeader.Request = parts[0]
		header_params := parts[1:]

		// Get header params
		foundSize := false
		for _, p := range header_params {
			parts := strings.SplitN(p, "=", 2)
			key := parts[0]
			value := parts[1]
			if key == "token" {
				resultHeader.Token = value
			} else if key == "mime" {
				resultHeader.Mime = value
			} else if key == "size" {
				foundSize = true
				var err error
				resultHeader.ContentLength, err = strconv.ParseInt(value, 10, 64)
				if err != nil {
					fmt.Printf("Titan Error: Size parameter is not an integer; %s; %#v\n", resultHeader.Request, header_params)
					responseBadRequest_SizeParamNotInt := "59 Size parameter is not an integer.\r\n"
					conn.Write([]byte(responseBadRequest_SizeParamNotInt))
					return resultHeader, err
				}
			}
		}

		// Default mime to "text/gemini"
		if resultHeader.Mime == "" {
			resultHeader.Mime = "text/gemini"
		}

		if !foundSize {
			// Error: Size is required
			fmt.Printf("Titan Error: No Size parameter given; %s; %#v\n", requestHeader, header_params)
			responseBadRequest_NoSizeParam := "59 No Size parameter given.\r\n"
			conn.Write([]byte(responseBadRequest_NoSizeParam))
			return resultHeader, fmt.Errorf("no size parameter given")
		}
	default:
		resultHeader.Protocol = ProtocolType_Unknown
	}

	resultHeader.Hostname = URL.Hostname()
	if resultHeader.Hostname == "" { // If no hostname in URL, get hostname from SNI (if available)
		resultHeader.Hostname = tlsState.ServerName
		// Otherwise, use the defaultServer
		if resultHeader.Hostname == "" {
			resultHeader.Hostname = l.DefaultServer().GetServer().GetPreferredHostOfPortListener(resultHeader.Protocol, resultHeader.ContentLength > 0 || resultHeader.Protocol == ProtocolType_Titan || resultHeader.Protocol == ProtocolType_NPS, l.BindAddress, l.BindPort, is_scgi).Hostname
		}
	}

	// Check overflows
	if requestHeaderOverflow {
		fmt.Printf("request length too long\n")
		responseLengthTooLong := "59 Request length too long. Total length cannot be more than 1KB for Misfin(C)'s request header, 1KB for Gemini, and 2KB for Misfin(AB)'s request+metadata+message.\r\n"
		conn.Write([]byte(responseLengthTooLong))
		return resultHeader, fmt.Errorf("request length too long")
	}

	if len(tlsState.PeerCertificates) > 0 {
		resultHeader.UserCert = ClientCertificateFromCertificate(tlsState.PeerCertificates[0])
		//resultHeader.UserCert = tlsState.PeerCertificates[0]
	}

	return resultHeader, nil
}

func scgiResultHeader(conn net.Conn, headers map[string]string, scgiIsOverTLS bool) (RequestHeader, error) {
	var resultHeader RequestHeader
	resultHeader.SCGI = true
	scheme := ""
	if v, _ := headers["SCHEME"]; v != "" {
		scheme = v
	}
	if v, _ := headers["SSL_TLS_SNI"]; v != "" { // Get SNI first, and if that isn't given, then go to the other env variables.
		resultHeader.Hostname = headers["SSL_TLS_SNI"]
	} else if v, _ := headers["REQUEST_DOMAIN"]; v != "" {
		resultHeader.Hostname = headers["REQUEST_DOMAIN"]
		/*} else if v := headers["GEMINI_URL"]; strings.ToUpper(headers["SERVER_PROTOCOL"]) == "GEMINI" && v != "" {
		} else if v := headers["URL"]; v != "" {
		} else if v := headers["REQUEST_URI"]; v != "" {*/
	} else if v, _ := headers["SERVER_NAME"]; v != "" {
		resultHeader.Hostname = headers["SERVER_NAME"]
	}
	if v, _ := headers["SERVER_PORT"]; v != "" {
		resultHeader.Port = headers["SERVER_PORT"]
		/*} else if v := headers["GEMINI_URL"]; strings.ToUpper(headers["SERVER_PROTOCOL"]) == "GEMINI" && v != "" {
		} else if v := headers["URL"]; v != "" {
		} else if v := headers["REQUEST_URI"]; v != "" {*/
	} else {
		resultHeader.Port = headers["REAL_LISTEN_PORT"]
	}

	// TODO: UserCert stuff
	resultHeader.SCGI_SERVER_NAME = headers["SERVER_NAME"]
	resultHeader.SCGI_REMOTE_ADDR = headers["REMOTE_ADDR"]
	resultHeader.SCGI_REMOTE_PORT = headers["REMOTE_PORT"]
	resultHeader.SCGI_REMOTE_HOST = headers["REMOTE_HOST"]
	resultHeader.SCGI_SCRIPT_NAME = headers["SCRIPT_NAME"] // TODO: Should not be url encoded?

	if strings.ToUpper(headers["AUTH_TYPE"]) == "CERTIFICATE" {
		if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "SHA256:") {
			resultHeader.UserCert.HashType = "SHA256"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "SHA256:")
		} else if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "MD5:") {
			resultHeader.UserCert.HashType = "MD5"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "MD5:")
		} else if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "SHA1:") {
			resultHeader.UserCert.HashType = "SHA1"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "SHA1:")
		} else if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "SHA224:") {
			resultHeader.UserCert.HashType = "SHA224"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "SHA224:")
		} else if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "SHA384:") {
			resultHeader.UserCert.HashType = "SHA384"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "SHA384:")
		} else if strings.HasPrefix(headers["TLS_CLIENT_HASH"], "SHA512:") {
			resultHeader.UserCert.HashType = "SHA512"
			resultHeader.UserCert.Hash = strings.TrimPrefix(headers["TLS_CLIENT_HASH"], "SHA512:")
		} else {
			resultHeader.UserCert.Hash = headers["TLS_CLIENT_HASH"]
		}

		if v, _ := headers["TLS_CLIENT_SERIAL_NUMBER"]; v != "" {
			resultHeader.UserCert.SerialNumber = headers["TLS_CLIENT_SERIAL"]
		} else if v, _ := headers["TLS_CLIENT_SERIAL"]; v != "" {
			resultHeader.UserCert.SerialNumber = headers["TLS_CLIENT"]
		}
		// NotBefore
		// NotAfter
		if v, _ := headers["TLS_CLIENT_SUBJECT"]; v != "" {
			resultHeader.UserCert.Subject = headers["TLS_CLIENT_SUBJECT"]
		} else if v, _ := headers["REMOTE_USER"]; v != "" {
			resultHeader.UserCert.Subject = headers["REMOTE_USER"]
		}

		resultHeader.UserCert.Issuer = headers["TLS_CLIENT_ISSUER"]
		resultHeader.UserCert.SubjectUserID = headers["REMOET_USER_ID"]
	}

	switch strings.ToUpper(headers["SERVER_PROTOCOL"]) {
	case "GEMINI":
		resultHeader.Protocol = ProtocolType_Gemini
		if scheme == "" {
			scheme = "gemini"
		}

		var given_url *url.URL
		var url_parse_err error
		if v, _ := headers["GEMINI_URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["GEMINI_URL"]) // Includes the query string
		} else if v, _ := headers["URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["URL"]) // Includes the query string
		} else if v, _ := headers["REQUEST_URI"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["REQUEST_URI"]) // TODO: Is this the full URL???
		}
		if url_parse_err != nil {
			conn.Write([]byte("42 Could not parse given url."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not parse given url.")
		}

		path := ""
		if v, _ := headers["PATH_INFO"]; v != "" {
			path = headers["PATH_INFO"]
		} else if v, _ := headers["REQUEST_PATH"]; v != "" {
			path = headers["REQUEST_PATH"]
		} else {
			// TODO: take just the path of GEMINI_URL (or URL or REQUEST_URI) and remove the SCRIPT_NAME prefix
			path = strings.TrimPrefix(given_url.Path, headers["SCRIPT_NAME"])
		}
		path = strings.TrimPrefix(path, "/")
		resultHeader.SCGI_PATH_INFO = path

		query, query_err := url.QueryUnescape(given_url.RawQuery)
		if query_err != nil {
			conn.Write([]byte("42 Could not unescape query string."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not unescape query string.")
		}
		if v, _ := headers["QUERY_STRING"]; v != "" {
			query = headers["QUERY_STRING"]
		}
		if query != "" {
			query = "?" + query
		}

		if resultHeader.Port == defaultPorts[ProtocolType_Gemini] {
			resultHeader.Request = fmt.Sprintf("%s://%s/%s%s", scheme, resultHeader.Hostname, path, query)
		} else {
			resultHeader.Request = fmt.Sprintf("%s://%s:%s/%s%s", scheme, resultHeader.Hostname, resultHeader.Port, path, query)
		}
	case "SPARTAN":
		resultHeader.Protocol = ProtocolType_Spartan
		if scheme == "" {
			scheme = "spartan"
		}

		if v, _ := headers["PATH_INFO"]; v != "" {
			resultHeader.Request = headers["PATH_INFO"] // Includes the query string
		} else if v, _ := headers["REQUEST_PATH"]; v != "" {
			resultHeader.Request = headers["REQUEST_PATH"] // Includes the query string
		}
		resultHeader.SCGI_PATH_INFO = resultHeader.Request

		if v, _ := headers["CONTENT_LENGTH"]; v != "" {
			i, err := strconv.Atoi(v)
			if err != nil {
				fmt.Printf("error reading request\n")
				responseBadContentLength := "59 Request invalid: content length not an integer.\r\n"
				conn.Write([]byte(responseBadContentLength))
				return resultHeader, fmt.Errorf("Content length not an integer.")
			}
			resultHeader.ContentLength = int64(i)
		}
	case "TITAN":
		resultHeader.Protocol = ProtocolType_Titan
		if scheme == "" {
			scheme = "titan"
		}

		params := ""
		var given_url *url.URL
		var url_parse_err error
		if v, _ := headers["TITAN_URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["TITAN_URL"]) // Includes the query string
			_, params, _ = strings.Cut(headers["TITAN_URL"], ";")
		} else if v, _ := headers["URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["URL"]) // Includes the query string
			fmt.Printf("%s\n", headers["URL"])
			_, params, _ = strings.Cut(headers["URL"], ";")
		} else if v, _ := headers["REQUEST_URI"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["REQUEST_URI"]) // TODO: Is this the full URL???
			_, params, _ = strings.Cut(headers["REQUEST_URI"], ";")
		}
		if url_parse_err != nil {
			conn.Write([]byte("42 Could not parse given url."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not parse given url.")
		}
		if params != "" {
			params = ";" + params
		}

		path := ""
		if v, _ := headers["PATH_INFO"]; v != "" {
			path = headers["PATH_INFO"]
		} else if v, _ := headers["REQUEST_PATH"]; v != "" {
			path = headers["REQUEST_PATH"]
		} else {
			// TODO: take just the path of GEMINI_URL (or URL or REQUEST_URI) and remove the SCRIPT_NAME prefix
			path = strings.TrimPrefix(given_url.Path, headers["SCRIPT_NAME"])
		}
		path = strings.TrimPrefix(path, "/")
		resultHeader.SCGI_PATH_INFO = path

		query, query_err := url.QueryUnescape(given_url.RawQuery)
		if query_err != nil {
			conn.Write([]byte("42 Could not unescape query string."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not unescape query string.")
		}
		if v, _ := headers["QUERY_STRING"]; v != "" {
			query = headers["QUERY_STRING"]
		}
		if query != "" {
			query = "?" + query
		}

		if resultHeader.Port == defaultPorts[ProtocolType_Titan] {
			resultHeader.Request = fmt.Sprintf("%s://%s/%s%s%s", scheme, resultHeader.Hostname, path, query, params)
		} else {
			resultHeader.Request = fmt.Sprintf("%s://%s:%s/%s%s%s", scheme, resultHeader.Hostname, resultHeader.Port, path, query, params)
		}

		// Get header params, if available
		parts := strings.Split(resultHeader.Request, ";")
		resultHeader.Request = parts[0]
		header_params := parts[1:]

		foundSize := false
		for _, p := range header_params {
			parts := strings.SplitN(p, "=", 2)
			key := parts[0]
			value := parts[1]
			if key == "token" {
				resultHeader.Token = value
			} else if key == "mime" {
				resultHeader.Mime = value
			} else if key == "size" {
				foundSize = true
				var err error
				resultHeader.ContentLength, err = strconv.ParseInt(value, 10, 64)
				if err != nil {
					fmt.Printf("Titan Error: Size parameter is not an integer; %s; %#v\n", resultHeader.Request, header_params)
					responseBadRequest_SizeParamNotInt := "42 Size parameter is not an integer.\r\n"
					conn.Write([]byte(responseBadRequest_SizeParamNotInt))
					return resultHeader, err
				}
			}
		}

		if v, _ := headers["TITAN_TOKEN"]; v != "" {
			resultHeader.Token = headers["TITAN_TOKEN"]
		}
		if v, _ := headers["TITAN_MIME"]; v != "" {
			resultHeader.Mime = headers["TITAN_MIME"]
		} else if v, _ := headers["CONTENT_TYPE"]; v != "" {
			resultHeader.Mime = headers["CONTENT_TYPE"]
		}

		if v, _ := headers["CONTENT_LENGTH"]; v != "" {
			i, err := strconv.Atoi(v)
			if err != nil {
				fmt.Printf("error reading request\n")
				responseBadContentLength := "42 Request invalid: content length not an integer.\r\n"
				conn.Write([]byte(responseBadContentLength))
				return resultHeader, fmt.Errorf("Content length not an integer.")
			}
			resultHeader.ContentLength = int64(i)
			foundSize = true
		}

		// Default mime to "text/gemini"
		if resultHeader.Mime == "" {
			resultHeader.Mime = "text/gemini"
		}

		if !foundSize {
			// Error: Size is required
			fmt.Printf("Titan Error: No Size parameter given; %s; %#v\n", resultHeader.Request, header_params)
			responseBadRequest_NoSizeParam := "59 No Size parameter given.\r\n"
			conn.Write([]byte(responseBadRequest_NoSizeParam))
			return resultHeader, fmt.Errorf("no size parameter given")
		}
	case "SCROLL":
		resultHeader.Protocol = ProtocolType_Scroll
		if scheme == "" {
			scheme = "scroll"
		}

		var given_url *url.URL
		var url_parse_err error
		if v, _ := headers["SCROLL_URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["SCROLL_URL"]) // Includes the query string
		} else if v, _ := headers["URL"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["URL"]) // Includes the query string
		} else if v, _ := headers["REQUEST_URI"]; v != "" {
			given_url, url_parse_err = url.Parse(headers["REQUEST_URI"]) // TODO: Is this the full URL???
		}
		if url_parse_err != nil {
			conn.Write([]byte("42 Could not parse given url."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not parse given url.")
		}

		path := ""
		if v, _ := headers["PATH_INFO"]; v != "" {
			path = headers["PATH_INFO"]
		} else if v, _ := headers["REQUEST_PATH"]; v != "" {
			path = headers["REQUEST_PATH"]
		} else {
			// TODO: take just the path of GEMINI_URL (or URL or REQUEST_URI) and remove the SCRIPT_NAME prefix
			path = strings.TrimPrefix(given_url.Path, headers["SCRIPT_NAME"])
		}
		path = strings.TrimPrefix(path, "/")
		resultHeader.SCGI_PATH_INFO = path

		query, query_err := url.QueryUnescape(given_url.RawQuery)
		if query_err != nil {
			conn.Write([]byte("42 Could not unescape query string."))
			return RequestHeader{}, fmt.Errorf("SCGI Application Server: Could not unescape query string.")
		}
		if v, _ := headers["QUERY_STRING"]; v != "" {
			query = headers["QUERY_STRING"]
		}
		if query != "" {
			query = "?" + query
		}

		if resultHeader.Port == defaultPorts[ProtocolType_Scroll] {
			resultHeader.Request = fmt.Sprintf("%s://%s/%s%s", scheme, resultHeader.Hostname, path, query)
		} else {
			resultHeader.Request = fmt.Sprintf("%s://%s:%s/%s%s", scheme, resultHeader.Hostname, resultHeader.Port, path, query)
		}

		if v, _ := headers["SCROLL_LANGUAGES"]; v != "" {
			parts := strings.Split(v, ",")
			for _, p := range parts {
				resultHeader.Languages = append(resultHeader.Languages, strings.TrimSpace(p))
			}
		}

		if v, _ := headers["REQUEST_METHOD"]; v == "METADATA" {
			resultHeader.MetadataRequest = true
		}
	case "MISFIN_B", "MISFIN/B", "MISFIN(B)": // TODO: Unfinished - where would the message data be sent?
		resultHeader.Protocol = ProtocolType_Misfin_B
		if scheme == "" {
			scheme = "misfin"
		}

		if v, _ := headers["MISFIN_URL"]; v != "" {
			resultHeader.Request = headers["MISFIN_URL"]
		} else if v, _ := headers["URL"]; v != "" {
			resultHeader.Request = headers["URL"] // Includes the query string.
		} else if v, _ := headers["REQUEST_URI"]; v != "" {
			resultHeader.Request = headers["REQUEST_URI"] // TODO: Is this the full URL???
		}
	case "MISFIN_C", "MISFIN/C", "MISFIN(C)":
		resultHeader.Protocol = ProtocolType_Misfin_C
		if scheme == "" {
			scheme = "misfin"
		}

		if v, _ := headers["MISFIN_URL"]; v != "" {
			resultHeader.Request = headers["MISFIN_URL"] // Includes the query string.
		} else if v, _ := headers["URL"]; v != "" {
			resultHeader.Request = headers["URL"] // Includes the query string.
		} else if v, _ := headers["REQUEST_URI"]; v != "" {
			resultHeader.Request = headers["REQUEST_URI"] // TODO: Is this the full URL???
		}

		// Get the content length of the message data (which will be passed in after the SCGI header as upload data)
		if v, _ := headers["CONTENT_LENGTH"]; v != "" {
			i, err := strconv.Atoi(v)
			if err != nil {
				fmt.Printf("error reading request\n")
				responseBadContentLength := "42 Request invalid: content length not an integer.\r\n"
				conn.Write([]byte(responseBadContentLength))
				return resultHeader, fmt.Errorf("Content length not an integer.")
			}
			resultHeader.ContentLength = int64(i)
		}
	case "NEX":
		resultHeader.Protocol = ProtocolType_Nex
		if scheme == "" {
			scheme = "nex"
		}

		panic("Unimplemented")
	case "GOPHER", "GOPHERS": // TODO
		resultHeader.Protocol = ProtocolType_Gopher_SSL
		if scheme == "" {
			scheme = "gopher"
		}

		panic("Unimplemented")
	default:
		panic("Unsupported protocol for SCGI.")
	}
	return resultHeader, nil
}

type RequestHeader struct {
	Protocol        ProtocolType
	Request         string            // The Selector/Path or URL that is requested. Includes the query string.
	Hostname        string            // Used for Gemini, Misfin, and Titan
	Port            string            // aka. ServePort, the public-facing port.
	ContentLength   int64             // Used for Titan, Misfin, and Spartan. If Titan protocol and 0, it's a titan delete request. If Spartan and 0, no upload data is expected.
	Token           string            // Used for Titan
	Mime            string            // Used for Titan
	UserCert        ClientCertificate // For TLS protocols
	MetadataRequest bool              // Used for Scroll protocol
	Languages       []string          // Used for Scroll protocol

	SCGI             bool
	SCGI_SERVER_NAME string
	SCGI_REMOTE_ADDR string
	SCGI_REMOTE_PORT string
	SCGI_REMOTE_HOST string // FQDN of client, or empty.
	SCGI_SCRIPT_NAME string
	SCGI_PATH_INFO   string // TODO

	//SCGI_PORT        string // The port that the SCGI server is being hosted on
	//SCGI_BIND_PORT   string // The port that the SCGI server is bound to
}

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections. It's used by Run so dead TCP connections (e.g.
// closing laptop mid-download) eventually go away.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	if c, err = ln.AcceptTCP(); err != nil {
		return
	} else if err = c.(*net.TCPConn).SetKeepAlive(true); err != nil {
		return
	}
	// Ignore error from setting the KeepAlivePeriod as some systems, such as
	// OpenBSD, do not support setting TCP_USER_TIMEOUT on IPPROTO_TCP
	_ = c.(*net.TCPConn).SetKeepAlivePeriod(3 * time.Minute)

	return
}
