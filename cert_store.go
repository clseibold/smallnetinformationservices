package smallnetinformationservices

import (
	"crypto"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"strconv"
	"strings"
)

type CertID string // Absolute Filepath

// A certificate for a given BindAddr+Port+Hostname Pair
type BindPortHostnameCertificate struct {
	CertFilepath       CertID
	Certificate        *x509.Certificate
	PrivateKey         crypto.PrivateKey
	PrivateKeyPemBytes []byte
}

func (c *BindPortHostnameCertificate) IsEmpty() bool {
	return c.Certificate == nil
}

// TODO: Return empty CertInfo if cannot load cert file and cert is empty.
func (c *BindPortHostnameCertificate) GetInfo() CertInfo {
	if c.IsEmpty() { // If empty, load the cert file
		certificate, privateKey, privateKeyPemBytes := getCertsFromPem(string(c.CertFilepath))
		c.Certificate = certificate
		c.PrivateKey = privateKey
		c.PrivateKeyPemBytes = privateKeyPemBytes
	}
	return getCertInfo(c.Certificate)
}

// TODO: Return nil if cannot load cert file and cert is empty.
func (c *BindPortHostnameCertificate) GetTLSCertificate() *tls.Certificate {
	if c.IsEmpty() { // If empty, load the cert file
		certificate, privateKey, privateKeyPemBytes := getCertsFromPem(string(c.CertFilepath))
		c.Certificate = certificate
		c.PrivateKey = privateKey
		c.PrivateKeyPemBytes = privateKeyPemBytes
	}
	return &tls.Certificate{Certificate: [][]byte{c.Certificate.Raw}, PrivateKey: c.PrivateKey, Leaf: c.Certificate}
}

type PortListenerProperties struct {
	nex           bool
	nps           bool
	gopher        bool
	spartan       bool
	misfin        bool
	defaultServer VirtualServerHandle
}

type CertConnection struct {
	BindAddress string
	BindPort    string
	Hostname    string // TODO: Empty for default/none
}

type ServerHost struct { // TODO: Rename to Host?
	BindAddress string
	BindPort    string
	Hostname    string
	Port        string // aka. ServePort, the public facing port. Should be empty for nex, nps, gopher, and spartan. // TODO
	//Protocol    ProtocolType
	Upload bool
	SCGI   bool
}

func (s *ServerHost) IsEmpty() bool {
	return (*s == ServerHost{})
}

func (s *ServerHost) ToString() string {
	upload := "0"
	scgi := "0"
	if s.Upload {
		upload = "1"
	}
	if s.SCGI {
		scgi = "1"
	}
	return fmt.Sprintf("%s:%s %s:%s %s%s", s.BindAddress, s.BindPort, s.Hostname, s.Port, upload, scgi)
}

func ParseHostString(host string) ServerHost {
	parts := strings.SplitN(host, " ", 3)
	bindAddress, bindPort, _ := strings.Cut(parts[0], ":")
	hostname, port, _ := strings.Cut(parts[1], ":")
	i, _ := strconv.Atoi(parts[2])
	if i == 00 {
		return ServerHost{bindAddress, bindPort, hostname, port, false, false}
	} else if i == 01 {
		return ServerHost{bindAddress, bindPort, hostname, port, false, true}
	} else if i == 10 {
		return ServerHost{bindAddress, bindPort, hostname, port, true, false}
	} else {
		return ServerHost{bindAddress, bindPort, hostname, port, true, true}
	}
}

type ServerHostMapping struct {
	ServePort string // The public-facing port. For nex, nps, gopher, and spartan, should be what was set in the config file.
	Handle    VirtualServerHandle
}

// Manages Certificates, their connections to BindPortHostnames, as well as Server Routes.
type CertificateManager struct {
	CertIDs                map[CertID]*BindPortHostnameCertificate
	CertConnections        map[CertConnection]*BindPortHostnameCertificate
	ServerHostMappings     map[ServerHost]ServerHostMapping
	PortListenerProperties map[PortListenerID]PortListenerProperties // TODO: Deal with this map when port listeners and server handles are deleted.
	//PortListenerDefaultServerMappings map[PortListenerID]ServerHandle           // TODO: Deal with this map when port listeners and server handles are deleted
}

func NewCertManager() *CertificateManager {
	return &CertificateManager{make(map[CertID]*BindPortHostnameCertificate), make(map[CertConnection]*BindPortHostnameCertificate), make(map[ServerHost]ServerHostMapping), make(map[PortListenerID]PortListenerProperties)}
}

func (m *CertificateManager) PortListenerHasServerHostOfProtocol(bindaddress string, bindport string, protocol ProtocolType) bool {
	for key, val := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && val.Handle.Protocols().Test(protocol) {
			return true
		}
	}

	return false
}

func (m *CertificateManager) PortListenerHasServerHostNotOfProtocol(bindaddress string, bindport string, protocol ProtocolType) bool {
	for key, val := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && !val.Handle.Protocols().Test(protocol) {
			return true
		}
	}

	return false
}

func (m *CertificateManager) GetOrAddCertificate(id CertID) *BindPortHostnameCertificate {
	if _, exists := m.CertIDs[id]; exists {
		return m.CertIDs[id]
	} else {
		certificate, privateKey, privateKeyPemBytes := getCertsFromPem(string(id))
		m.CertIDs[id] = &BindPortHostnameCertificate{id, certificate, privateKey, privateKeyPemBytes}
		return m.CertIDs[id]
	}
}

func (m *CertificateManager) AddCertificateConnection(filepath string, bindaddress string, bindport string, hostname string) CertID {
	id := CertID(filepath)
	cert := m.GetOrAddCertificate(id)
	m.CertConnections[CertConnection{bindaddress, bindport, hostname}] = cert
	return id
}

func (m *CertificateManager) RemoveCertificateConnection(bindaddress string, bindport string, hostname string) {
	delete(m.CertConnections, CertConnection{bindaddress, bindport, hostname})
}

func (m *CertificateManager) RemoveCertificateConnections(id CertID) {
	for key, connection := range m.CertConnections {
		if connection.CertFilepath == id {
			delete(m.CertConnections, key)
		}
	}
}

func (m *CertificateManager) RemoveCertificate(id CertID) {
	m.RemoveCertificateConnections(id)
	delete(m.CertIDs, id)
}

// RemoveCertificatesWithNoConnections removes orphaned certificates that have no connections to hostnames.
func (m *CertificateManager) RemoveCertificatesWithNoConnections() {
	certs := make(map[CertID]bool, len(m.CertIDs))
	for id := range m.CertIDs {
		certs[id] = false
	}
	for _, val := range m.CertConnections {
		certs[val.CertFilepath] = true
	}

	for id, val := range certs {
		if !val {
			delete(m.CertIDs, id)
		}
	}
}

// GetCertificateFromID gets the certificate given the certificate ID. Use GetOrAddCertificate() to get a certificate and add it if it hasn't already been added.
func (m *CertificateManager) GetCertificateFromID(id CertID) (*BindPortHostnameCertificate, bool) {
	v, exists := m.CertIDs[id]
	return v, exists
}

func (m *CertificateManager) GetCertificate(bindaddress string, bindport string, hostname string) (*BindPortHostnameCertificate, bool) {
	v, exists := m.CertConnections[CertConnection{bindaddress, bindport, hostname}]
	return v, exists
}

func (m *CertificateManager) GetCertificateFromServerHost(key ServerHost) (*BindPortHostnameCertificate, bool) {
	return m.GetCertificate(key.BindAddress, key.BindPort, key.Hostname)
}

func (m *CertificateManager) GetCertificatesForPortListener(bindaddress string, bindport string) map[string]*BindPortHostnameCertificate {
	result := make(map[string]*BindPortHostnameCertificate)

	for key, val := range m.CertConnections {
		if key.BindAddress == bindaddress && key.BindPort == bindport {
			result[key.Hostname] = val
		}
	}

	return result
}

func (m *CertificateManager) GetDefaultCertForPortListener(l *PortListener) *BindPortHostnameCertificate {
	l_id := PortListenerID{l.BindAddress, l.BindPort}
	if m.PortListenerProperties[l_id].misfin {
		// Find misfin server's hostname, then get it's cert
		hostname := ""
		for k, h := range m.ServerHostMappings {
			if h.Handle.Type() != ServerType_Misfin {
				continue
			}
			hostname = k.Hostname
			break
		}
		cert, exists := m.GetCertificate(l.BindAddress, l.BindPort, hostname)
		if exists {
			return cert
		}
	}

	// Uses the defaultServer's preferred host's hostname as the default
	s := m.PortListenerProperties[l_id].defaultServer
	if (s == VirtualServerHandle{}) {
		panic("Default server is nil.")
	} else if len(m.GetServerHosts(s)) == 0 {
		panic("Default server has no hosts.")
	}

	preferredHost := s.GetPreferredHost(ProtocolType_Unknown, false, false)
	cert, _ := m.GetCertificate(l.BindAddress, l.BindPort, preferredHost.Hostname) // TODO: Panic if it doesn't exist?
	return cert
}

// GetCertificatesWithNoConnections gets orphaned certificates that have no connectins to hostnames.
func (m *CertificateManager) GetCertificatesWithNoConnections() []*BindPortHostnameCertificate {
	certs := make(map[CertID]bool, len(m.CertIDs))
	for id := range m.CertIDs {
		certs[id] = false
	}
	for _, val := range m.CertConnections {
		certs[val.CertFilepath] = true
	}

	var result []*BindPortHostnameCertificate
	for id, val := range certs {
		if !val {
			result = append(result, m.CertIDs[id])
		}
	}

	return result
}

func (m *CertificateManager) AddServerHostMapping(bindaddress string, bindport string, hostname string, port string, upload bool /*protocol ProtocolType*/, scgi bool, handle VirtualServerHandle) {
	serveport := port
	switch handle.Type() {
	case ServerType_Nex, ServerType_Spartan, ServerType_Gopher:
		port = ""
	}

	// Handle Nex, NPS, & Gopher servers and listeners, which cannot be served along with other non-TLS protocols on one portlistener
	portListenerNonTLSHostsLength := len(m.GetNonTLSHostsOnPortListener(bindaddress, bindport))
	if (handle.Protocols().Test(ProtocolType_Nex) && portListenerNonTLSHostsLength > 0) || (m.PortListenerHasServerHostOfProtocol(bindaddress, bindport, ProtocolType_Nex) && handle.HasNonTLSProtocol()) {
		panic("Nex servers cannot be served with other ptotocols or hostnames on the same port. They must have their own port alone.")
	} else if (handle.Protocols().Test(ProtocolType_NPS) && portListenerNonTLSHostsLength > 0) || (m.PortListenerHasServerHostOfProtocol(bindaddress, bindport, ProtocolType_NPS) && handle.HasNonTLSProtocol()) {
		panic("NPS servers cannot be served with other protocols or hostnames on the same port. They must have their own port alone.")
	} else if (handle.Protocols().Test(ProtocolType_Gopher) && portListenerNonTLSHostsLength > 0) || (m.PortListenerHasServerHostOfProtocol(bindaddress, bindport, ProtocolType_Gopher) && handle.HasNonTLSProtocol()) {
		panic("Gopher servers cannot be served with other ptotocols or hostnames on the same port. They must have their own port alone.")
	} else if (handle.Protocols().Test(ProtocolType_Spartan) && m.PortListenerHasServerHostNotOfProtocol(bindaddress, bindport, ProtocolType_Spartan)) || (!handle.Protocols().Test(ProtocolType_Spartan) && handle.HasNonTLSProtocol() && m.PortListenerHasServerHostOfProtocol(bindaddress, bindport, ProtocolType_Spartan)) {
		// TODO: The first if expression is bugged - we should check if port listener has a host that has non-spartan servers *that are also non-TLS*. TLS protocols *can* be ran with spartan servers on same portlistener.

		// Handle Spartan - supports virtual hosting, but doesn't specify a protocol in the request string.
		// Therefore, this port listener will only allow spartan servers.
		panic("You cannot have multiple protocols served with a spartan server on the same port listener. Spartan servers support virtual hosting, but do not support being multiplexed with other protocols.")
	} /*else if l.misfin && protocol != ProtocolType_Misfin_A && protocol != ProtocolType_Misfin_B && protocol != ProtocolType_Misfin_C {
		// Handle misfin listener - all entries of the given hostname must go to the same server
		for k, v := range l.serverRouteMap {
			if k.Hostname != key.Hostname {
				continue
			} else if v != server {
				panic("You cannot have multiple servers served with a misfin server on the same port+hostname pair. Misfin listeners must have all entries of a given port+hostname link to the same server.")
			}
		}
	} else if protocol == ProtocolType_Misfin_A || protocol == ProtocolType_Misfin_B || protocol == ProtocolType_Misfin_C {
		l.misfin = true

		// All entries of a hostname must go to the same server
		for k, v := range l.serverRouteMap {
			if k.Hostname != key.Hostname {
				continue
			} else if v != server {
				panic("You cannot have multiple servers served with a misfinserver on the same port+hostname pair. Misfin listeners must have all entries of a given post+hostname link to the same server.")
			}
		}
	}*/

	// Ensure that there is only one onion hostname per listener
	//if strings.HasSuffix(key.Hostname, ".onion") {
	//panic("Please use SetTorServer() to set a Tor server.")
	/*for k := range l.serverRouteMap {
		if strings.HasSuffix(k.Hostname, ".onion") {
			panic("You may only have one onion address per port listener.")
		}
	}
	l.tor = true*/
	//}

	// Ensure that there is only one onion hostname per listener
	//if strings.HasSuffix(key.Hostname, ".i2p") {
	//panic("Please use SetI2PServer() to set an I2P server.")
	/*for k := range l.serverRouteMap {
		if strings.HasSuffix(k.Hostname, ".i2p") {
			panic("You may only have one i2p address per port listener.")
		}
	}
	l.i2p = true*/
	//}

	// TODO: Error out if a hostname + port + protocol + scgi is being overwritten when a mapping for it already exists
	m.ServerHostMappings[ServerHost{bindaddress, bindport, hostname, port, upload, scgi}] = ServerHostMapping{serveport, handle}

	l_id := PortListenerID{bindaddress, bindport}
	properties := m.PortListenerProperties[l_id]
	if handle.Protocols().Test(ProtocolType_Nex) {
		properties.nex = true
	} else if handle.Protocols().Test(ProtocolType_NPS) {
		properties.nps = true
	} else if handle.Protocols().Test(ProtocolType_Gopher) || handle.Protocols().Test(ProtocolType_Gopher_SSL) {
		properties.gopher = true
	} else if handle.Protocols().Test(ProtocolType_Spartan) {
		properties.spartan = true
	} else if handle.Protocols().Test(ProtocolType_Misfin_A) || handle.Protocols().Test(ProtocolType_Misfin_B) || handle.Protocols().Test(ProtocolType_Misfin_C) {
		properties.misfin = true
	}

	// Set port listener's default server to first server added to it. If adding a misfin server, that should always be the default regardless of when it was added.
	// TODO: This should work differently somehow. Maybe a setting to set a "fallback server"
	// TODO: When the misfin server's certificate doesn't cover all the hostnames on the PortListener, then probably should error out?
	if _, exists := m.PortListenerProperties[l_id]; !exists || handle.Protocols().Test(ProtocolType_Misfin_A) || handle.Protocols().Test(ProtocolType_Misfin_B) || handle.Protocols().Test(ProtocolType_Misfin_C) {
		properties.defaultServer = handle
	}
	m.PortListenerProperties[l_id] = properties
}

func (m *CertificateManager) RemoveServerHostMapping(bindaddress string, bindport string, hostname string, port string, upload bool, scgi bool) {
	/*if protocol == ProtocolType_Nex || protocol == ProtocolType_NPS || protocol == ProtocolType_Spartan || protocol == ProtocolType_Gopher {
	port = ""
	}*/
	delete(m.ServerHostMappings, ServerHost{bindaddress, bindport, hostname, port, upload, scgi})
}

// RemoveServerRoutes removes just the routes of the given server handle. Will not remove any certificates or certificate connections associated with the server's hosts. For that, use RemoveServer().
func (m *CertificateManager) RemoveServerHosts(handle VirtualServerHandle) {
	for key, h := range m.ServerHostMappings {
		if h.Handle == handle {
			delete(m.ServerHostMappings, key)
		}
	}
}

// RemoveServerHostsOfHostnameAndPort removes all Hosts of a given Hostname on a given PortListener.
func (m *CertificateManager) RemoveServerHostsOfHostname(bindaddress string, bindport string, hostname string) {
	for key := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && key.Hostname == hostname {
			delete(m.ServerHostMappings, key)
		}
	}
}

// RemoveServerHostsOfHostnameAndPort removes all Hosts of a given Hostname and Port on a given PortListener.
func (m *CertificateManager) RemoveServerHostsOfHostnameAndPort(bindaddress string, bindport string, hostname string, port string) {
	for key, v := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && key.Hostname == hostname && (key.Port == port || v.ServePort == port) {
			delete(m.ServerHostMappings, key)
		}
	}
}

func (m *CertificateManager) GetServerFromHostMapping(bindaddress string, bindport string, hostname string, port string, upload bool, scgi bool) (ServerHostMapping, bool) {
	/*if protocol == ProtocolType_Nex || protocol == ProtocolType_NPS || protocol == ProtocolType_Spartan || protocol == ProtocolType_Gopher {
	port = ""
	}*/
	v, exists := m.ServerHostMappings[ServerHost{bindaddress, bindport, hostname, port, upload, scgi}]
	return v, exists
}

func (m *CertificateManager) GetHostsOnPortListener(bindaddress string, bindport string) []ServerHost {
	var result []ServerHost

	for key := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport {
			result = append(result, key)
		}
	}

	return result
}

func (m *CertificateManager) GetNonTLSHostsOnPortListener(bindaddress string, bindport string) []ServerHost {
	var result []ServerHost

	for key, val := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && val.Handle.HasNonTLSProtocol() {
			result = append(result, key)
		}
	}

	return result
}

// GetHostsOfHostname gets all Hosts of a hostname attached to any bind address and bind port.
func (m *CertificateManager) GetHostsOfHostname(hostname string) []ServerHost {
	var result []ServerHost

	for key := range m.ServerHostMappings {
		if key.Hostname == hostname {
			result = append(result, key)
		}
	}

	return result
}

// GetHostsOfHostname gets all Hosts of a hostname attached to any bind address and bind port.
func (m *CertificateManager) GetHostsOfHostnameOnPortListener(bindaddress string, bindport string, hostname string) []ServerHost {
	var result []ServerHost

	for key := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && key.Hostname == hostname {
			result = append(result, key)
		}
	}

	return result
}

// GetHostsOfHostnameAndPort gets all Hosts of a hostname and port attached to any bind address and bind port.
func (m *CertificateManager) GetHostsOfHostnameAndPort(hostname string, port string) []ServerHost {
	var result []ServerHost

	for key, v := range m.ServerHostMappings {
		if key.Hostname == hostname && (key.Port == port || v.ServePort == port) {
			result = append(result, key)
		}
	}

	return result
}

// GetServerCertificates gets the certificates of the server's hosts.
func (m *CertificateManager) GetServerCertificates(handle VirtualServerHandle) []*BindPortHostnameCertificate {
	var certs = make(map[CertConnection]*BindPortHostnameCertificate)
	hosts := m.GetServerHosts(handle)
	for _, h := range hosts {
		if cert, exists := m.GetCertificateFromServerHost(h); exists {
			certs[CertConnection{h.BindAddress, h.BindPort, h.Hostname}] = cert
		}
	}

	var result []*BindPortHostnameCertificate
	for _, c := range certs {
		result = append(result, c)
	}

	return result
}

// GetServerHosts gets all Hosts of a particular server.
func (m *CertificateManager) GetServerHosts(handle VirtualServerHandle) []ServerHost {
	var result []ServerHost

	for key, h := range m.ServerHostMappings {
		if h.Handle == handle {
			result = append(result, key)
		}
	}

	return result
}

// GetServerHosts gets all Hosts of a particular server.
func (m *CertificateManager) GetServerHostsOnPortListener(bindaddress string, bindport string, handle VirtualServerHandle) []ServerHost { // TODO: Add way to specify serveport (in key of mapping)
	var result []ServerHost

	for key, h := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && h.Handle == handle {
			result = append(result, key)
		}
	}

	return result
}

// GetServerHosts gets all Hosts of a particular server.
func (m *CertificateManager) GetServerHostsWithServePortOnPortListener(bindaddress string, bindport string, serveport string, handle VirtualServerHandle) []ServerHost { // TODO: Add way to specify serveport (in key of mapping)
	var result []ServerHost

	for key, h := range m.ServerHostMappings {
		if key.BindAddress == bindaddress && key.BindPort == bindport && h.Handle == handle && h.ServePort == serveport {
			result = append(result, key)
		}
	}

	return result
}

// RemoveHostname removes the server routes of the given hostname and its certificate connections.
func (m *CertificateManager) RemoveHostname(bindaddress string, bindport string, hostname string) {
	m.RemoveServerHostsOfHostname(bindaddress, bindport, hostname)
	m.RemoveCertificateConnection(bindaddress, bindport, hostname)
}

// RemoveHostname removes the server routes of the given hostname and port on a given bind address and bind port, as well as the certificate connections if no server routes of the hostname exist with other ports.
func (m *CertificateManager) RemoveHostnameAndPort(bindaddress string, bindport string, hostname string, port string) {
	m.RemoveServerHostsOfHostnameAndPort(bindaddress, bindport, hostname, port)
	if len(m.GetHostsOfHostnameOnPortListener(bindaddress, bindport, hostname)) <= 0 {
		// Only remove certificate connection if there are no server routes of the hostname with other ports.
		m.RemoveCertificateConnection(bindaddress, bindport, hostname)
	}
}

// TODO: RemoveServer removes the certificate connections and server routes of the given server handle. Does not remove the certificates or cert connections of hostnames used on other servers.
func (m *CertificateManager) RemoveServer(handle VirtualServerHandle) {

}
