package smallnetinformationservices

import (
	"time"

	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
)

// Handle value for servers
type VirtualServerHandle struct {
	index int
	sis   *SISContext
}

func (handle VirtualServerHandle) GetData(key string) (interface{}, bool) {
	return handle.GetServer().KeyValStore.Get(key)
}

func (handle VirtualServerHandle) SetData(key string, value interface{}) {
	handle.GetServer().KeyValStore.Set(key, value)
}

func (handle VirtualServerHandle) Protocols() *bitset.BitSet[ProtocolType, ProtocolType] {
	return &handle.GetServer().Protocols
}

func (handle VirtualServerHandle) HasTLSProtocol() bool {
	return handle.GetServer().HasTLSProtocol()
}

func (handle VirtualServerHandle) HasNonTLSProtocol() bool {
	return handle.GetServer().HasNonTLSProtocol()
}

func (handle VirtualServerHandle) HasUploadProtocol() bool {
	return handle.GetServer().HasUploadProtocol()
}

func (handle VirtualServerHandle) GetServer() *VirtualServer {
	if (handle == VirtualServerHandle{}) {
		return nil
	}
	return &handle.sis.servers[handle.index]
}

func (handle VirtualServerHandle) via_config() bool {
	return handle.GetServer().via_config
}

func (handle VirtualServerHandle) AddHosts(hosts ...HostConfig) {
	handle.GetServer().AddHosts(hosts...)
}

func (handle VirtualServerHandle) GetPreferredHost(protocol ProtocolType, upload bool, scgi bool) ServerHost {
	return handle.GetServer().GetPreferredHost(protocol, upload, scgi)
}
func (handle VirtualServerHandle) GetPreferredHostOfPortListener(protocol ProtocolType, upload bool, bind_addr string, bind_port string, scgi bool) ServerHost {
	return handle.GetServer().GetPreferredHostOfPortListener(protocol, upload, bind_addr, bind_port, scgi)
}
func (handle VirtualServerHandle) DefaultLanguage() string {
	return handle.GetServer().DefaultLanguage
}
func (handle VirtualServerHandle) Name() string {
	return handle.GetServer().Name
}
func (handle VirtualServerHandle) SetName(v string) {
	handle.GetServer().Name = v
}
func (handle VirtualServerHandle) Type() ServerType {
	return handle.GetServer().Type
}
func (handle VirtualServerHandle) SetType(v ServerType) {
	handle.GetServer().Type = v
}
func (handle VirtualServerHandle) MaxConcurrentConnections() int {
	return handle.GetServer().MaxConcurrentConnections
}
func (handle VirtualServerHandle) SetMaxConcurrentConnections(v int) {
	handle.GetServer().MaxConcurrentConnections = v
}
func (handle VirtualServerHandle) Directory() string {
	return handle.GetServer().Directory
}
func (handle VirtualServerHandle) FS() ServerFS {
	return handle.GetServer().FS
}
func (handle VirtualServerHandle) ServerHandler() ServerHandlerFunc {
	return handle.GetServer().ServerHandler
}
func (handle VirtualServerHandle) SetServerHandler(v ServerHandlerFunc) {
	handle.GetServer().ServerHandler = v
}
func (handle VirtualServerHandle) Pubnix() bool {
	return handle.GetServer().Pubnix
}
func (handle VirtualServerHandle) SetPubnix(v bool) {
	handle.GetServer().Pubnix = v
}
func (handle VirtualServerHandle) Router() *Router {
	return &handle.GetServer().Router
}

/*
	func (handle ServerHandle) SetRouter(v Router) {
		handle.GetServer().Router = v
	}
*/

func (handle VirtualServerHandle) RateLimitDuration() time.Duration {
	return handle.GetServer().RateLimitDuration
}
func (handle VirtualServerHandle) SetRateLimitDuration(v time.Duration) {
	handle.GetServer().RateLimitDuration = v
}
func (handle VirtualServerHandle) IsIPRateLimited(ip string) bool {
	return handle.GetServer().IsIPRateLimited(ip)
}
func (handle VirtualServerHandle) CleanupRateLimiting() {
	handle.GetServer().CleanupRateLimiting()
}
func (handle VirtualServerHandle) SIS() *SISContext {
	return handle.GetServer().SIS
}
func (handle VirtualServerHandle) SetSIS(v *SISContext) {
	handle.GetServer().SIS = v
}

func (handle VirtualServerHandle) LoadRoutes() error {
	return handle.GetServer().LoadRoutes()
}
func (handle VirtualServerHandle) SaveRoutes() error {
	return handle.GetServer().SaveRoutes()
}

/*
func (handle *ServerHandle) StartServer(wg *sync.WaitGroup) {
	handle.GetServer().StartServer(wg)
}
func (handle *ServerHandle) StopServer() {
	handle.GetServer().StopServer()
}
func (handle *ServerHandle) RestartServer() {
	handle.GetServer().RestartServer()
}
*/

func (handle VirtualServerHandle) AddRoute(p string, handler RequestHandler) {
	handle.GetServer().AddRoute(p, handler)
}
func (handle VirtualServerHandle) AddUploadRoute(p string, handler RequestHandler) {
	handle.GetServer().AddUploadRoute(p, handler)
}
func (handle VirtualServerHandle) AddDirectory(p string, directoryPath string) {
	handle.GetServer().AddDirectory(p, directoryPath)
}
func (handle VirtualServerHandle) AddCGIRoute(p string, filePath string) {
	handle.GetServer().AddCGIRoute(p, filePath)
}
func (handle VirtualServerHandle) AddSCGIRoute(p string, address string) {
	handle.GetServer().AddSCGIRoute(p, address)
}
func (handle VirtualServerHandle) AddProxyRoute(p string, proxyRoute string, gopherItemType rune) {
	handle.GetServer().AddProxyRoute(p, proxyRoute, gopherItemType)
}
func (handle VirtualServerHandle) AddFile(p string, filePath string) {
	handle.GetServer().AddFile(p, filePath)
}

// Group creates a prefix to be automatically prepended to all routes created on the RouteGroup.
// It will also ensure all absolute links in the responses of the attached routes will be
// prepended with the group prefix.
func (handle VirtualServerHandle) Group(p string) RouteGroup {
	return RouteGroup{handle: handle, p: p}
}

// Does nothing, since ServerHandles don't add prefixes to routes. This is here so that ServerHandle
// implements the ServeMuxer interface.
/*func (handle ServerHandle) PrefixLink(link string) string {
return link
}*/

func (handle VirtualServerHandle) Scheme() string {
	return handle.GetServer().Scheme()
}
