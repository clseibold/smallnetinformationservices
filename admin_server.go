package smallnetinformationservices

import (
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"

	"gitlab.com/sis-suite/smallnetinformationservices/bitset"
)

func admin_Init(s ServeMux) {
	s.AddRoute("/", admin_indexHandler)            // SIS Manager Homepage
	s.AddRoute("/log", admin_serverLog)            // Server Log
	s.AddRoute("/settings", admin_SISSettingsPage) // SIS Settings
	//s.AddRoute("/update_restart", admin_updateRestart)
	//s.AddRoute("/full_restart", admin_fullRestart)
	//s.AddRoute("/update_finished", admin_updateFinished)
	//s.AddUploadRoute("/upload_update_restart", admin_uploadUpdateRestart)
	//s.AddRoute("/shutdown", admin_shutdown) // Shutdown SIS

	s.AddRoute("/s/", admin_servers)                 // Servers List
	s.AddRoute("/s/create", admin_createServerStart) // Create Server
	s.AddRoute("/s/create/:type", admin_createServerOfType)
	s.AddRoute("/s/create/:type/:name", admin_createServer)
	s.AddRoute("/s/create_temp/:type", admin_addNexTempServer)       // Create Temp Server
	s.AddRoute("/s/create_temp/:type/:name", admin_addNexTempServer) // Create Temp Server

	s.AddRoute("/s/:server", admin_serverPage) // Server Homepage

	s.AddRoute("/s/:server/h/:host", admin_serverHostPage)   // Route
	s.AddRoute("/s/:server/r/:route", admin_serverRoutePage) // Route

	s.AddRoute("/s/:server/:setting", admin_serverSetting) // Set Server Setting
}

// ----- Route Request Handlers -----

func admin_indexHandler(request *Request) {
	request.Gemini("# SIS Manager\n")
	request.Gemini("\n")
	request.Gemini("=> /settings SIS Manager Settings\n")
	request.Gemini("=> /log Server Log\n")
	request.Gemini("=> /s/create_temp/nex Add Temp Nex Server\n")
	request.Gemini("=> /s/create Create Server\n")
	request.Gemini("\n")
	request.Gemini("## Servers\n")
	for _, s := range request.Server.SIS().servers {
		request.Gemini(fmt.Sprintf("=> /s/%s/ %s\n", url.PathEscape(s.Name), s.Name))
	}
}

func admin_serverLog(request *Request) {
	request.Gemini("# Server Log\n")
	request.Gemini("\n")
	request.Gemini("```\n")

	request.Server.SIS().logBuffer.Do(func(a any) {
		switch a := a.(type) {
		case string:
			request.Gemini(a + "\n")
			return
		}
	})
	request.Gemini("```\n")
}

func admin_SISSettingsPage(request *Request) { // TODO
	request.Gemini("# SIS Manager Settings\n")
	request.Gemini("\n")
	request.Gemini("=> /shutdown Shutdown SIS\n")
	request.Gemini("=> /full_restart SIS Full Restart\n")
	request.Gemini("\n")
	request.Gemini("## Update Options\n")
	request.Gemini("\nIf an update link has been configured for custom server software, use the below link. If no link is configured, then it will do a full restart without an update.\n")
	request.Gemini("=> /update_restart SIS Update and Restart\n")
	request.Gemini("\nYou can also upload the updated executable via Titan and have the server update and restart with that. It will *only* replace the executable file and start the server from this executable file. You must have admin permissions to do this. This option can be enabled/disabled in the configuration file only.\n")
	request.Gemini(fmt.Sprintf("=> titan://%s:%s/upload_update_restart Upload Update via Titan and Restart\n", request.Hostname(), request.Host.Port))
}

func admin_shutdown(request *Request) {
	query, err := request.Query()
	if err != nil {
		request.TemporaryFailure("%s", err.Error())
		return
	}
	if query == "" {
		request.RequestInput("Shutdown SIS? Y/N?")
	} else if query == "Y" || query == "y" || query == "yes" || query == "YES" {
		request.ServerUnavailable("SIS has been shut down.")
		request.Server.SIS().ShutdownSIS()
	}
}

func admin_updateRestart(request *Request) {
	query, err := request.Query()
	if err != nil {
		request.TemporaryFailure("%s", err.Error())
		return
	}
	if query == "" {
		request.RequestInput("Update and restart SIS? Y/N?")
	} else if query == "Y" || query == "y" || query == "yes" || query == "YES" {
		renamedFilepath := request.Server.SIS().UpdateAndRestart(nil)
		request.Redirect("/update_finished?%s", renamedFilepath)
	}
}

func admin_updateFinished(request *Request) {
	// Delete the renamed exe file
	query, err := request.Query()
	if err != nil {
		request.TemporaryFailure("%s", err.Error())
		return
	}
	if query != "" {
		current, err := os.Executable()
		if err != nil {
			request.Redirect("/")
			return
		}

		// Make sure filepath is in same directory as current executable
		if filepath.Dir(current) == filepath.Dir(query) {
			// If so, delete the file
			os.Remove(query)
		}
	}
	request.Redirect("/")
}

// TODO: Check that this is allowed (with config setting) and that the client cert user has permissions to do this.
func admin_uploadUpdateRestart(request *Request) {
	if !request.Upload() {
		request.TemporaryFailure("Use titan to upload an executable to this link.")
		return
	} else {
		if request.DataMime != "application/x-msdownload" && request.DataMime != "application/octet-stream" && request.DataMime != "application/x-executable" && request.DataMime != "application/vnd.microsoft.portable-executable" {
			request.TemporaryFailure("Wrong mimetype.")
			return
		}

		data, err := request.GetUploadData()
		if err != nil {
			request.TemporaryFailure("Failed to get uploaded data.")
			return
		}
		renamedFilepath := request.Server.SIS().UpdateAndRestart(data)
		request.Redirect("gemini://%s:%s/update_finished?%s", request.Hostname(), request.Host.Port, renamedFilepath)
	}
}

/*
func admin_uploadUpdateShutdown(request Request) {
	if !request.Upload {
		request.TemporaryFailure("Use titan to upload an executable to this link.")
		return
	} else {
		data, err := request.GetUploadData()
		if err != nil {
			request.TemporaryFailure("Failed to get uploaded data.")
			return
		}
		request.Server.SIS.UpdateAndShutdown(data)
		request.ServerUnavailable("SIS has been shut down.")
		//request.Redirect("gemini://%s:%s/update_finished?%s", request.Hostname(), request.Server.Port, renamedFilepath)
	}
}
*/

func admin_fullRestart(request *Request) {
	query, err := request.Query()
	if err != nil {
		request.TemporaryFailure("%s", err.Error())
		return
	}
	if query == "" {
		request.RequestInput("Do a full restart of SIS? Y/N?")
	} else if query == "Y" || query == "y" || query == "yes" || query == "YES" {
		request.Server.SIS().FullRestart()
		request.Redirect("/")
	}
}

func admin_addNexTempServer(request *Request) {
	// Add a new Nex Server, then redirect back to homepage
	/*dynServer := request.Server.SIS.AddServer(Server{Name: "NewNexServer", Type: ServerType_Nex, BindAddress: "0.0.0.0", Port: "1996", MaxConcurrentConnections: 2000})
	dynServer.AddRoute("/", func(request Request) {
		request.NexListing("New Nex Server that was Dynamically Loaded!\n")
	})
	go dynServer.StartServer(request.Server.SIS.wg)
	request.Redirect("/s/%s", url.PathEscape(dynServer.Name()))
	*/
}

func admin_servers(request *Request) {
	request.Gemini("# SIS Manager: Servers\n")

	for _, s := range request.Server.SIS().servers {
		request.Gemini(fmt.Sprintf("=> /s/%s/ %s\n", url.PathEscape(s.Name), s.Name))
	}
	request.Gemini("```\n")
}

// -- Create Server Routes --

func admin_createServerStart(request *Request) {
	request.Gemini("# Create Server of Type\n")
	request.Link("/s/create/gemini", "Gemini")
	request.Link("/s/create/gopher", "Gopher")
	request.Link("/s/create/nex", "Nex")
	request.Link("/s/create/spartan", "Spartan")
	request.Link("/s/create/scroll", "Scroll")
	request.Link("/s/create/misfin", "Misfin")
	request.Gemini("\n")
}

func admin_createServerOfType(request *Request) {
	query, err := request.Query()
	if err != nil {
		request.TemporaryFailure("Temp: %s", err.Error())
	}
	if query == "" {
		request.RequestInput("Enter a server name (no spaces allowed):")
		return
	}

	t := request.GetParam("type")

	if strings.ContainsAny(query, " \t\n\r") {
		request.TemporaryFailure("Server names cannot contain whitespace.")
	} else {
		request.Redirect("/s/create/%s/%s", t, url.PathEscape(query))
	}
}

func admin_createServer(request *Request) {
	t := request.GetParam("type")
	name := request.GetParam("name")
	if strings.ContainsAny(name, " \n\r") {
		request.TemporaryFailure("Server names cannot contain whitespace.")
		return
	}

	serverType := ServerType_Gemini
	switch t {
	case "gemini":
		serverType = ServerType_Gemini
	case "nex":
		serverType = ServerType_Nex
	case "gopher":
		serverType = ServerType_Gopher
	case "misfin":
		serverType = ServerType_Misfin
	case "spartan":
		serverType = ServerType_Spartan
	case "scroll":
		serverType = ServerType_Scroll
	}

	// TODO: Check that server did not already exist

	serverhandle, err := request.Server.SIS().AddServer(VirtualServer{Type: serverType, Name: name, via_config: true})
	if err != nil {
		request.TemporaryFailure("Failed to create server: %s", err.Error())
	}
	server := serverhandle.GetServer()
	request.Server.SIS().SaveConfiguration()
	if server == nil {
		request.TemporaryFailure("Failed to create server.")
		return
	} else {
		request.Redirect("/s/%s", server.Name)
	}
}

// --- Server Pages ---

func admin_serverPage(request *Request) {
	serverName := request.GetParam("server")
	serverhandle := request.Server.SIS().FindServerByName(serverName)
	server := serverhandle.GetServer()
	if server == nil {
		request.Server.GetServer().Router.NotFoundHandler(request) // Hacky
		return
	}

	request.Gemini(fmt.Sprintf("# Server: %s\n", server.Name))
	request.Gemini(fmt.Sprintf("=> /s/%s/restart Restart Server (TODO)\n", server.Name))
	request.Gemini("\n")

	t := ""
	switch server.Type {
	case ServerType_Admin:
		t = "Admin (Gemini)"
	case ServerType_Gemini:
		t = "Gemini"
	case ServerType_Misfin:
		t = "Misfin"
	case ServerType_Gopher:
		t = "Gopher"
	case ServerType_Nex:
		t = "Nex"
	case ServerType_Spartan:
		t = "Spartan"
	}
	request.Gemini(fmt.Sprintf("=> /s/%s/type Type: %s\n", server.Name, t))
	request.Gemini(fmt.Sprintf("=> /s/%s/directory Directory: %s\n", server.Name, server.Directory))
	//request.Gemini(fmt.Sprintf("=> /s/%s/max-concurrent-connections Max Concurrent Connections: %d\n", server.Name, server.MaxConcurrentConnections))
	if server.Type != ServerType_Admin { // Note: Pubnix mode not allowed on admin interface
		request.Gemini(fmt.Sprintf("Pubnix Mode: %v", server.Pubnix))
	}
	// TODO: Print cert info for server if it's a Gemini or Misfin server.
	request.Gemini("\n")

	request.Gemini("## Hosts\n")
	request.Gemini(fmt.Sprintf("=> /s/%s/h/create Add Host\n", serverName))

	// List Preferred Host first
	preferred := server.GetPreferredHost(ProtocolType_Unknown, false, false)
	if !preferred.IsEmpty() {
		request.Gemini(fmt.Sprintf("=> /s/%s/h/p/ Download: %s:%s %s:%s\n", serverName, preferred.BindAddress, preferred.BindPort, preferred.Hostname, preferred.Port))
	}
	preferred_upload := server.GetPreferredHost(ProtocolType_Unknown, true, false)
	if !preferred_upload.IsEmpty() && preferred_upload != preferred {
		request.Gemini(fmt.Sprintf("=> /s/%s/h/p/ Upload: %s:%s %s:%s (Upload)\n", serverName, preferred_upload.BindAddress, preferred_upload.BindPort, preferred_upload.Hostname, preferred_upload.Port))
	}

	hosts := server.SIS.certmanager.GetServerHosts(server.handle)
	for _, host := range hosts {
		if host == preferred || host == preferred_upload { // Skip already-listed preferred host
			continue
		}
		/*if host.Upload {
			request.Gemini(fmt.Sprintf("=> /s/%s/h/%d/ %s:%s %s:%s (Upload)\n", serverName, i, host.BindAddress, host.BindPort, host.Hostname, host.Port))
		} else {*/
		t := "Download: "
		if host.Upload {
			t = "Upload: "
		}
		request.Gemini(fmt.Sprintf("=> /s/%s/h/%s/ %s%s:%s %s:%s\n", serverName, url.PathEscape(host.ToString()), t, host.BindAddress, host.BindPort, host.Hostname, host.Port))
		//}
	}
	request.Gemini("\n")

	request.Gemini("## Certificates\n")
	certificates := server.SIS.certmanager.GetServerCertificates(server.handle)
	for _, c := range certificates {
		info := c.GetInfo()
		request.Gemini(fmt.Sprintf("%s: %s\n", info.Domains[0], c.CertFilepath))
	}

	// TODO: Misfin servers should list these as mailboxes, not routes.
	request.Gemini("## Routes\n")
	routes := server.Router.GetRoutesList()
	for _, route := range routes {
		request.Gemini(fmt.Sprintf("=> /s/%s/r/%s/ %s\n", serverName, url.PathEscape(route.route), route.route))
	}
}

// TODO: Mostly unimplemented
func admin_serverSetting(request *Request) {
	serverName := request.GetParam("server")
	serverhandle := request.Server.SIS().FindServerByName(serverName)
	server := serverhandle.GetServer()
	if server == nil {
		request.Server.GetServer().Router.NotFoundHandler(request) // Hacky
		return
	}

	setting := request.GetParam("setting")
	value, err := request.Query()
	if err != nil {
		request.TemporaryFailure("%s", err.Error())
		return
	}
	if value == "" {
		prompt := ""
		switch setting {
		case "type":
			prompt = "Enter a server type (gemini, nex, misfin, or gopher):"
		case "directory":
			prompt = "Enter directory to store server's config files:"
		case "bind-address":
			prompt = "Enter bind-address (ip address of network interface to listen on, or '0.0.0.0' for all interfaces):"
		case "hostname":
			prompt = "Enter hostname:"
		case "port":
			prompt = "Enter Port:"
		case "max-concurrent-connections":
			prompt = "Enter number of max concurrent connections to allow for this particular server:"
		case "certificate":
			prompt = "Filepath of certificate:"
		case "pubnix":
			prompt = "Enable pubnix mode? (yes/no) "
		}
		request.RequestInput("%s", prompt)
		return
	} else {
		switch setting {
		case "type":
			value = strings.ToLower(value)
			switch value {
			case "gemini":
				server.Type = ServerType_Gemini
			case "gopher":
				server.Type = ServerType_Gopher
			case "misfin":
				server.Type = ServerType_Misfin
			case "nex":
				server.Type = ServerType_Nex
				// TODO: case "admin":
			case "spartan":
				server.Type = ServerType_Spartan
			}
		case "directory":
			server.Directory = strings.TrimSpace(value)
		// case "bind-address":
		// server.BindAddress = strings.TrimSpace(value)
		// case "hostname":
		// server.Hostname = strings.TrimSpace(value)
		/*case "port":
		// TODO: Parse Int to make sure it's a valid integer
		server.Port = strings.TrimSpace(value)
		// TODO: Check that server of same port doesn't already exist.
		*/
		case "max-concurrent-connections":
			// TODO: Parse Int
		// case "certificate":
		// server.ServerTLSCertFilename = strings.TrimSpace(value)
		case "pubnix":
			value = strings.ToLower(value)
			if value == "true" || value == "yes" || value == "y" {
				server.Pubnix = true
			} else if value == "false" || value == "no" || value == "n" {
				server.Pubnix = false
			} else {
				request.TemporaryFailure("Invalid input.")
				return
			}
		}

		// TODO: Restart Server for changes that require this (like port and server type)
		server.SIS.SaveConfiguration() // TODO: Mutex Lock
		request.Redirect("/s/%s", server.Name)
		return
	}
}

// --- Server Host Pages ---

func admin_serverHostPage(request *Request) {
	name := request.GetParam("server")
	serverhandle := request.Server.SIS().FindServerByName(name)
	server := serverhandle.GetServer()
	if server == nil {
		request.Server.GetServer().Router.NotFoundHandler(request) // Hacky
		return
	}

	host_escaped := request.GetParam("host")
	host_str, err := url.PathUnescape(host_escaped)
	if err != nil {
		request.TemporaryFailure("Could not parse host")
		return
	}
	host := ParseHostString(host_str)

	title := "# "
	if host.SCGI {
		title += "SCGI "
	}
	if host.Upload {
		title += "Upload "
	} else {
		title += "Download "
	}
	title += "Host: " + fmt.Sprintf("%s:%s %s:%s", host.BindAddress, host.BindPort, host.Hostname, host.Port) + "\n"
	request.Gemini(title)

	cert, has_cert := request.Server.SIS().certmanager.GetCertificateFromServerHost(host)
	if has_cert {
		request.Gemini(fmt.Sprintf("Certificate: %s\n", string(cert.CertFilepath)))
	}
	request.Gemini(fmt.Sprintf("\n=> /s/%s/h/%s/bind_address Change Bind Address\n", server.Name, host_escaped))
	request.Gemini(fmt.Sprintf("=> /s/%s/h/%s/bind_port Change Bind Port\n", server.Name, host_escaped))
	request.Gemini(fmt.Sprintf("=> /s/%s/h/%s/hostname Change Hostname\n", server.Name, host_escaped))
	request.Gemini(fmt.Sprintf("\n=> /s/%s/h/%s/delete Delete Host\n", server.Name, host_escaped))
	request.Gemini(fmt.Sprintf("=> /s/%s/ Server Home\n", server.Name))
	//request.Gemini("\n=> Change Certificate\n")
}

// --- Server Route Pages ---

func admin_serverRoutePage(request *Request) {
	name := request.GetParam("server")
	serverhandle := request.Server.SIS().FindServerByName(name)
	server := serverhandle.GetServer()
	if server == nil {
		request.Server.GetServer().Router.NotFoundHandler(request) // Hacky
		return
	}

	preferredHost := server.GetPreferredHost(ProtocolType_Unknown, false, false) // TODO: SCGI?

	route_escaped := request.GetParam("route")
	route, err := url.PathUnescape(route_escaped)
	if err != nil {
		request.Gemini("Error.... (TODO)\n")
		return
	}
	routeName := path.Join("$"+server.Name, route)
	request.Gemini(fmt.Sprintf("# Route: %s\n", routeName))

	request.Gemini("\n")
	routeNode, globString, params := server.Router.Search(route)

	request.Gemini(fmt.Sprintf("Visits: %d\n", routeNode.visits.Load()))
	request.Gemini(fmt.Sprintf("In-progress Client Connections: %d\n", routeNode.currentConnections.Load()))
	// If a serve path, show the directory tree of the route (by proxying to gemini_handleDirectory)
	if routeNode.servePath != "" {
		request.Gemini(fmt.Sprintf("Serving Path: %s\n", routeNode.servePath))
		request.Gemini("File Contents: \n```\n")

		beforeGlob, _, _ := strings.Cut(route, "*")
		newRequestString := server.Scheme() + preferredHost.Hostname + ":" + preferredHost.Port + beforeGlob
		rawQuery, _ := request.RawQuery()

		// TODO: Do we put in request.Host or preferredHost for Proxy Requests?
		flags := bitset.BitSet[RequestFlag, RequestFlag]{}
		requestRoute := strings.TrimSuffix(routeNode.GetRoutePath(), "*")
		requestPath := beforeGlob
		prefix := InsertParamsIntoRouteString(routeNode.prefix, params, globString, true)
		proxyRequest := &Request{request.Type, flags, request.Host, requestRoute, prefix, requestPath, newRequestString, rawQuery, 0, globString, request.readwriter, request._tcpConn, params, 0, []byte{}, "", request.UserCert, request.IP, request.RemotePort, routeNode.servePath, routeNode.proxyRoute, "", request.fromRoute, routeNode.gopherItemType, request.Server, []string{}, ScrollMetadata{}}
		gemini_handleDirectory(proxyRequest)
		request.Gemini("\n```\n")
	}
}
